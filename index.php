<?php // phpcs:disable PSR1.Files.SideEffects
/**
 *
 * @package     Frontline WebApp Base
 *
 * @subpackage  <Application Main Index>
 *
 * @author      Sorin Pohontu <sorin@frontline.ro>
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @category    controllers
 * @see         https://www.yiiframework.com/doc/guide/2.0/en/structure-controllers
 *
 * @since       2020.06.29
 *
 */

/**
 * Initial loading of the config file
 * to get development IP addresses
 */
if (file_exists(__DIR__ . '/config.php')) {
    @include(__DIR__ . '/config.php');
}

if (in_array(filter_input(INPUT_SERVER, 'REMOTE_ADDR'), defined('DEVIPS') ? DEVIPS : ['127.0.0.1'])) {
    // Development
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
    defined('YII_DEBUG') or define('YII_DEBUG', true);
    defined('YII_ENV') or define('YII_ENV', 'dev');
} else {
    // Production
    error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_WARNING & ~E_STRICT);
    defined('YII_DEBUG') or define('YII_DEBUG', false);
    defined('YII_ENV') or define('YII_ENV', 'prod');
}

// Base files
require(__DIR__ . '/vendor/autoload.php');
require(__DIR__ . '/vendor/yiisoft/yii2/Yii.php');

// Config
$config = require(__DIR__ . '/application/config/web.php');

// Start application
(new yii\web\Application($config))->run();
