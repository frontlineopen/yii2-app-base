/**
 * @package     Frontline WebApp Base
 *
 * @subpackage  <JavaScript file>
 *
 * @author      Sebastian Costiug <sebastian@frontline.ro>
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @description The purpose of this file is to instantiate custom jQuery scripts.
 *
 * @since       2020.08.05
 *
 */

jQuery(function (e) {
    /* Gridview row click to view entry */
    $('.pjax-grid-container').on(
        'click',
        'td.grid-view:not(.action-column-buttons)',
        function (e) {
            var link = $(this).closest('tr').find('.btn-view').attr('href');
            $.ajax({
                url: link,
                type: 'GET',
                dataType: 'JSON',
                success: function (result) {
                    $('#activity-modal')
                        .find('.modal-title')
                        .text(result.title)
                        .end()
                        .find('.modal-body')
                        .removeData()
                        .html(result.body);
                    $('#activity-modal').modal('show');
                },
                error: function () {
                    var growlContent = {
                        icon: 'fas fa-radiation-alt',
                        message: 'Something went wrong',
                    };
                    var growlSettings = {
                        placement: {
                            from: 'top',
                            align: 'right',
                        },
                        type: 'danger',
                        animate: {
                            enter: 'animated fadeInDown',
                            exit: 'animated fadeOutUp',
                        },
                    };
                    $.notify(growlContent, growlSettings);
                },
            });
        }
    );

    /* Gridview row click to edit entry */
    $('.pjax-grid-container').on('click', '.btn-update', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var link = $(this).attr('href');
        $.ajax({
            url: link,
            type: 'GET',
            dataType: 'JSON',
            success: function (result) {
                $('#activity-modal')
                    .find('.modal-title')
                    .text(result.title)
                    .end()
                    .find('.modal-body')
                    .removeData()
                    .html(result.body);
                $('#activity-modal').modal('show');
                setTimeout(function() { $('form input:text, form textarea').first().focus(); }, 250);
            },
            error: function () {
                var growlContent = {
                    icon: 'fas fa-radiation-alt',
                    message: 'Something went wrong',
                };
                var growlSettings = {
                    placement: {
                        from: 'top',
                        align: 'right',
                    },
                    type: 'danger',
                    animate: {
                        enter: 'animated fadeInDown',
                        exit: 'animated fadeOutUp',
                    },
                };
                $.notify(growlContent, growlSettings);
            },
        });
    });

    /* Model view click to edit entry */
    $('.modal-body').on('click', '.btn-update', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var link = $(this).attr('href');
        $.ajax({
            url: link,
            type: 'GET',
            dataType: 'JSON',
            success: function (result) {
                $('#activity-modal')
                    .find('.modal-title')
                    .text(result.title)
                    .end()
                    .find('.modal-body')
                    .removeData()
                    .html(result.body);
                $('#activity-modal').modal('show');
                setTimeout(function() { $('form input:text, form textarea').first().focus(); }, 250);
            },
            error: function () {
                var growlContent = {
                    icon: 'fas fa-radiation-alt',
                    message: 'Something went wrong',
                };
                var growlSettings = {
                    placement: {
                        from: 'top',
                        align: 'right',
                    },
                    type: 'danger',
                    animate: {
                        enter: 'animated fadeInDown',
                        exit: 'animated fadeOutUp',
                    },
                };
                $.notify(growlContent, growlSettings);
            },
        });
    });

    /* GridView actionColumn deleteButton AJAX Request */
    $('.pjax-grid-container').on('click', '.pjax-delete-link', function (e) {
        e.preventDefault();
        var deleteUrl = $(this).data('delete-url');
        var cancelLabel = $(this).data('cancel-label');
        var okLabel = $(this).data('ok-label');
        var dialogTitle = $(this).data('dialog-title');
        var dialogMessage = $(this).data('dialog-message');
        BootstrapDialog.confirm({
            title: dialogTitle,
            message: dialogMessage,
            type: BootstrapDialog.TYPE_DEFAULT,
            closable: false,
            draggable: false,
            btnCancelLabel: cancelLabel,
            btnOKLabel: okLabel,
            btnOKClass: 'btn-warning',
            callback: function (result) {
                if (result) {
                    $.ajax({
                        url: deleteUrl,
                        type: 'POST',
                        dataType: 'JSON',
                        error: function (xhr, status, error) {
                            alert(
                                'There was an error handling your request.' +
                                    xhr.responseText
                            );
                        },
                    }).done(function (data) {
                        $.pjax.reload('#grid-view-pjax', { timeout: 3000 });
                        status =
                            data.status === 'error' ? 'danger' : data.status;
                        var growlContent = {
                            icon: 'fas fa-check-circle',
                            message: data.message,
                        };
                        var growlSettings = {
                            type: status,
                            placement: {
                                from: 'top',
                                align: 'right',
                            },
                            animate: {
                                enter: 'animated fadeInDown',
                                exit: 'animated fadeOutUp',
                            },
                        };
                        $.notify(growlContent, growlSettings);
                    });
                }
            },
        });
    });

    /* Gridview add new model entry */
    $('.pjax-grid-container').on('click', '.add-entry', function (e) {
        var link = $('#grid-view').data('addurl');
        $.ajax({
            url: link,
            type: 'GET',
            dataType: 'JSON',
            success: function (result) {
                $('#activity-modal')
                    .find('.modal-title')
                    .text(result.title)
                    .end()
                    .find('.modal-body')
                    .removeData()
                    .html(result.body);
                $('#activity-modal').modal('show');
                setTimeout(function() { $('form input:text, form textarea').first().focus(); }, 250);
            },
            error: function () {
                var growlContent = {
                    icon: 'fas fa-radiation-alt',
                    message: 'Something went wrong',
                };
                var growlSettings = {
                    placement: {
                        from: 'top',
                        align: 'right',
                    },
                    type: 'danger',
                    animate: {
                        enter: 'animated fadeInDown',
                        exit: 'animated fadeOutUp',
                    },
                };
                $.notify(growlContent, growlSettings);
            },
        });
    });

    /* Save and continue buttons on forms */
    $('#activity-modal').on('click', '.btn-save-and-continue', function (e) {
        e.preventDefault();
        var form = $('form');
        $.ajax({
            url: form.attr('action'),
            type: 'POST',
            dataType: 'JSON',
            data: form.serialize(),
            success: function (result) {
                growlStatus =
                    result.status === 'error' ? 'danger' : result.status;
                if (result['status'] == 'success') {
                    $.pjax.reload('#grid-view-pjax', { timeout: 3000 });
                    modal = $('#activity-modal');
                    if (modal) {
                        modal.modal('hide');
                    }
                } else {
                    $.each(result['errors'], function (error) {
                        inputContainer = $('div.field-' + error);
                        inputContainer.addClass('has-error');
                        inputContainer
                            .find('.help-block')
                            .text(result['errors'][error]);
                    });
                }
                var growlContent = {
                    icon: 'fas fa-check-circle',
                    message: result['message'],
                };
                var growlSettings = {
                    type: growlStatus,
                    placement: {
                        from: 'top',
                        align: 'right',
                    },
                    animate: {
                        enter: 'animated fadeInDown',
                        exit: 'animated fadeOutUp',
                    },
                };
                $.notify(growlContent, growlSettings);
            },
            error: function () {
                var growlContent = {
                    icon: 'fas fa-radiation-alt',
                    message: 'Something went wrong',
                };
                var growlSettings = {
                    placement: {
                        from: 'top',
                        align: 'right',
                    },
                    type: 'danger',
                    animate: {
                        enter: 'animated fadeInDown',
                        exit: 'animated fadeOutUp',
                    },
                };
                $.notify(growlContent, growlSettings);
            },
        });
    });

    /* GridView table header resize */
    if ($('.kv-grid-table').length > 0) {
        $(window).on('scroll', function (e) {
            $('.kv-grid-table').floatThead().trigger('reflow');
        });
    }

    /* Forms: Focus on first input */
    setTimeout(function() { $('form input:text, form textarea').first().focus(); }, 250);
});
