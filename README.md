Frontline Yii2 Base Application Template
================================

Frontline Yii2 Base Application is a skeleton Yii 2 application best for rapidly creating projects using LAMP stack.
It includes all commonly used configurations that would allow you to focus on adding new features to your application.

DIRECTORY STRUCTURE
-------------------
    application/assets/         Assets definition
    application/commands/       Console commands (controllers)
    application/config/         Application configurations
    application/controllers/    Web controller classes
    application/mail/           Emails views
    application/models/         Model classes
    application/runtime/        Runtime files
    application/tests/          Various tests for the application
    application/views/          View files for the Web application

    vendor/                     3rd-party packages
    index.php                   Entry script and Web resources

CODE STYLE
------------
We're using [Yii 2 Core Framework Code Style](https://github.com/yiisoft/yii2/blob/master/docs/internals/core-code-style.md).
You're advise to use the same style and conventions.

REQUIREMENTS
------------

The minimum requirement by this application template that your Web server supports PHP 5.4.0.

You can also check if your server configuration meets the requirements for running Yii application by executing the following command after installation:

~~~
php application/requirements.php
~~~

INSTALLATION
------------

### Install from an Archive File

Extract the archive file downloaded from [bitbucket.org](https://bitbucket.org/frontlineopen/yii2-app-base).

### Install via Composer

If you do not have [Composer](http://getcomposer.org/), you may install it by following the instructions
at [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

You can then install this application template using the following command:

~~~
php composer.phar global require "fxp/composer-asset-plugin:1.0.0-beta3"
php composer.phar create-project --prefer-dist --no-dev --prefer-dist frontlineopen/yii2-app-base test
~~~

CONFIGURATION
-------------

### Database

Edit the file `application/config/db.php` with real data, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=yii2_simple',
    'username' => 'root',
    'password' => '1234',
    'charset' => 'utf8',
];
```

**NOTE:** Yii won't create the database for you, this has to be done manually before you can access it.

Also check and edit the other files in the `application/config/` directory to customize your application.
