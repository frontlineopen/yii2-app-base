<?php
/**
 * @package     Frontline WebApp Base
 *
 * @subpackage  <Container file>
 *
 * @author      Sebastian Costiug <sebastian@frontline.ro>
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @category    container
 * @see         https://www.yiiframework.com/doc/api/2.0/yii-di-container
 *
 * @since       2020.07.24
 */

use yii\helpers\Html;

/*
 * Default settings for the Kartik GridView widget
 * more info : https://demos.krajee.com/grid#gridview
 */

\Yii::$container->set(
    'kartik\grid\GridView',
    [
        'bootstrap'          => true,
        'responsive'         => true,
        'bordered'           => false,
        'striped'            => false,
        'condensed'          => false,
        'panel'              => [
        ],
        'layout'             => '<span class="d-flex justify-content-end">{toolbar}</span>{items}<hr /><div class="d-flex justify-content-between">{summary}{pager}</div>',
        'toolbar'            => [
            [
                'content' =>
                Html::button('<span><i class="fas fa-plus"></i> Add</span>', [
                    'class' => 'btn btn-success add-entry',
                    'title' => Yii::t('app', 'Add'),
                ]),
            ],
            '{export}',
        ],
        'export'             => [
            'icon'        => 'fas fa-download',
            'label'       => Yii::t('app', 'Export'),
            'fontAwesome' => true,
        ],
        'responsiveWrap'     => false,
        'hover'              => true,
        'resizableColumns'   => true,
        'floatHeader'        => true,
        'floatHeaderOptions' => [
            'autoReflow' => true,
            'position'   => 'absolute',
        ],
        'pjax'               => true,
        'pjaxSettings'       => [
            'neverTimeout'       => true,
            'enablePushState'    => false,
            'enableReplaceState' => false,
        ],
        'rowOptions'         => function ($model, $key, $index, $grid) {
            return ['data-id' => $model->id];
        },
    ]
);

/*
 * Default settings for the Kartik GridView ActionsColumn
 * more info : https://demos.krajee.com/grid#action-column
 */

\Yii::$container->set('kartik\grid\ActionColumn', [
    'mergeHeader'    => false,
    'contentOptions' => ['class' => 'action-column-buttons'],
    'headerOptions'  => ['class' => 'action-column-buttons'],
    'filterOptions'  => ['class' => 'action-column-buttons'],
    'template'       => '{view}{update}{delete}',
    'buttons'        => [
        'view'   => function ($url, $model, $key) {
            return Html::a('<i class="fas fa-eye"></i>', $url, [
                'title'       => Yii::t('app', 'View'),
                'class'       => 'btn btn-outline-primary btn-view d-none pjax-view-link',
                'data-toggle' => 'modal',
                'data-target' => '#activity-modal',
                'data-id'     => $key,
                'data-pjax'   => '1',
            ]);
        },
        'update' => function ($url, $model, $key) {
            return Html::a('<i class="fas fa-pen"></i>', $url, [
                'title'       => Yii::t('app', 'Update'),
                'class'       => 'btn btn-outline-info btn-sm btn-update pjax-update-link',
                'data-toggle' => 'modal',
                'data-target' => '#activity-modal',
                'data-id'     => $key,
                'data-pjax'   => '1',
            ]);
        },
        'delete' => function ($url, $model) {
            return Html::a('<i class="fas fa-trash-alt"></i>', false, [
                'title'               => Yii::t('app', 'Delete'),
                'class'               => 'btn btn-outline-danger btn-sm btn-delete pjax-delete-link',
                'data-delete-url'     => $url,
                'data-cancel-label'   => Yii::t('app', 'Cancel'),
                'data-ok-label'       => Yii::t('app', 'Delete'),
                'data-dialog-title'   => Yii::t('app', 'Warning'),
                'data-dialog-message' => Yii::t('app', 'You are about to delete <b>{name}</b>! Are you sure?', ['name' => $model->name]),
            ]);
        },
    ],
]);

/*
 *
 * Default settings for the Kartik ActiveForm
 * more info : https://demos.krajee.com/widget-details/active-form
 */

\Yii::$container->set('kartik\form\ActiveForm', [
    'successCssClass' => false,
]);
