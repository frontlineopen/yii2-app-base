<?php
/**
 * @package     Frontline WebApp Base
 *
 * @subpackage  <Console Application configuration>
 *
 * @author      Sorin Pohontu <sorin@frontline.ro> & Sebastian Costiug <sebastian@frontline.ro>
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @category    config
 * @see         http://www.yiiframework.com/doc-2.0/guide-concept-configurations.html
 *
 * @since       2020.06.03
 */

/**
 * Aliases
 * @see         http://www.yiiframework.com/doc-2.0/guide-concept-aliases.html
 */
Yii::setAlias('@uploads', dirname(dirname(__DIR__)) . DIRECTORY_SEPARATOR . 'uploads');

/**
 * Container for default options
 */
require(__DIR__ . DIRECTORY_SEPARATOR . 'container.php');

/**
 * Database credentials
 * and development IP addresses
 */
if (file_exists(dirname(dirname(__DIR__)) . DIRECTORY_SEPARATOR . 'config.php')) {
    @include(dirname(dirname(__DIR__)) . DIRECTORY_SEPARATOR . 'config.php');
} else {
    define('DBHOST', '');
    define('DBNAME', '');
    define('DBUSER', '');
    define('DBPASS', '');
    define('DBPREFIX', '');
    define('APPID', '');
}

$config = [
    // Application definition
    'id'      => APPID . '-console',
    'name'    => 'Frontline WebApp Base Console',
    'version' => '1.00',

    // Paths
    'basePath'   => dirname(__DIR__),
    'vendorPath' => dirname(dirname(__DIR__)) . DIRECTORY_SEPARATOR . 'vendor',

    // Bootstrap
    'bootstrap' => [
        'log',
        'gii',
    ],

    // Controller namespace: commands
    'controllerNamespace' => 'app\commands',

    // Modules
    'modules' => [
        'gii' => 'yii\gii\Module',
    ],

    // Components
    'components' => [
        // Caching
        'cache' => [
            'class' => extension_loaded('apc') ? 'yii\caching\ApcCache' : 'yii\caching\FileCache',
        ],

        // App logging
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning', 'trace'],
                ],
            ],
        ],

        // Database config
        'db' => [
            'class'      => 'yii\db\Connection',
            'dsn'        => 'mysql:host=localhost;dbname=' . DBNAME,
            'username'   => DBUSER,
            'password'   => DBPASS,
            'charset'    => 'utf8',
            'attributes' => [
                PDO:: ATTR_AUTOCOMMIT => true,
            ],
            'on afterOpen' => function ($event) {
                $event->sender
                ->createCommand("SET default_storage_engine=InnoDB; SET NAMES 'utf8mb4' COLLATE 'utf8mb4_general_ci'; SET time_zone='+00:00';")
                ->execute();
            }
        ],
    ],

    // controllerMap
    'controllerMap' => [
        'migrate' => [
            'class'         => 'yii\console\controllers\MigrateController',
            'migrationPath' => [
                '@app/migrations',
            ],
        ],
        'clean-vendors' => [
            'class'         => 'app\commands\CleanVendorsController',
        ],
    ],

    // Params
    'params' => require(__DIR__ . DIRECTORY_SEPARATOR . 'params.php'),
];

return $config;
