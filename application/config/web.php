<?php
/**
 * @package     Frontline WebApp Base
 *
 * @subpackage  <Web Application configuration>
 *
 * @author      Sorin Pohontu <sorin@frontline.ro> & Sebastian Costiug <sebastian@frontline.ro>
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @category    config
 * @see         http://www.yiiframework.com/doc-2.0/guide-concept-configurations.html
 *
 * @since       2020.06.03
 */

/**
 * Aliases
 * @see         http://www.yiiframework.com/doc-2.0/guide-concept-aliases.html
 */
Yii::setAlias('@uploads', dirname(dirname(__DIR__)) . DIRECTORY_SEPARATOR . 'uploads');

/**
 * Container for default options
 */
require(__DIR__ . DIRECTORY_SEPARATOR . 'container.php');

/**
 * Database credentials
 */
if (file_exists(dirname(dirname(__DIR__)) . DIRECTORY_SEPARATOR . 'config.php')) {
    @include(dirname(dirname(__DIR__)) . DIRECTORY_SEPARATOR . 'config.php');
} else {
    define('DBHOST', '');
    define('DBNAME', '');
    define('DBUSER', '');
    define('DBPASS', '');
    define('DBPREFIX', '');
    define('APPID', '');
}

$config = [
    // Application definition
    'id'             => APPID,
    'name'           => 'Frontline WebApp Base',
    'version'        => '1.00',

    // Language
    'sourceLanguage' => 'en',
    'language'       => 'en',

    // Paths
    'basePath'       => dirname(__DIR__),
    'vendorPath'     => dirname(dirname(__DIR__)) . DIRECTORY_SEPARATOR . 'vendor',

    // Bootstrap
    'bootstrap'      => [
        'log',
    ],

    // Modules
    'modules'        => [
        // kartik GridView widget, replacement for the default Yii2 GridView widget
        'gridview' => [
            'class' => '\kartik\grid\Module',
        ],
    ],

    // Components
    'components'     => [
        // Request
        'request'      => [
            'cookieValidationKey' => 'RANDOM',
        ],

        // URL Manager
        'urlManager'   => [
            'class'               => 'yii\web\UrlManager',
            'enablePrettyUrl'     => true,
            'showScriptName'      => false,
            'enableStrictParsing' => false,
            'rules'               => [
                // Site actions
                ''           => 'site/index',
                'about'      => 'site/about',
                'migrate-up' => 'site/migrate-up',
                'dashboard'  => 'dashboard/index',
            ],
        ],

        // Database config
        'db'           => [
            'class'        => 'yii\db\Connection',
            'dsn'          => 'mysql:host=' . DBHOST . ';dbname=' . DBNAME,
            'username'     => DBUSER,
            'password'     => DBPASS,
            'tablePrefix'  => DBPREFIX,
            'charset'      => 'utf8',
            'attributes'   => [
                PDO::ATTR_AUTOCOMMIT => true,
            ],
            'on afterOpen' => function ($event) {
                $event->sender
                    ->createCommand("SET default_storage_engine=InnoDB; SET NAMES 'utf8mb4' COLLATE 'utf8mb4_general_ci'; SET time_zone='+00:00';")
                    ->execute();
            },
        ],

        // Caching
        'cache'        => [
            'class'           => extension_loaded('apc') ? 'yii\caching\ApcCache' : 'yii\caching\FileCache',
            'keyPrefix'       => APPID,
            'defaultDuration' => 86400,
        ],

        // Default error handler
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'i18n'         => [
            'translations' => [
                'app' => [
                    'class'          => 'yii\i18n\PhpMessageSource',
                    'basePath'       => '@app/messages',
                    'sourceLanguage' => 'en',
                    'fileMap'        => [
                        'app'       => 'app.php',
                        'app/error' => 'error.php',
                    ],
                ],
            ],
        ],

        'formatter'    => [
            'class'          => 'yii\i18n\Formatter',
            'dateFormat'     => 'php:d-M-Y',
            'datetimeFormat' => 'php:d-M-Y H:i:s',
            'timeFormat'     => 'php:H:i:s',
        ],

        // Mailer
        'mailer'       => [
            'class'            => 'yii\swiftmailer\Mailer',
            'useFileTransport' => false,
            'viewPath'         => '@app/mail',
            'transport'        => [
                'class'         => 'Swift_SmtpTransport',
                'constructArgs' => ['localhost', 25],
            ],
        ],

        // App logging
        'log'          => [
            'traceLevel' => YII_DEBUG ? 0 : 0,
            'targets'    => [
                [
                    'class'       => 'yii\log\FileTarget',
                    'levels'      => ['error', 'warning'],
                    'maxLogFiles' => 5,
                    'maxFileSize' => 160,
                ],
            ],
        ],

        // Metadata gatherer
        'metadata'     => [
            'class' => 'app\components\Metadata',
        ],

    ],

    // Params
    'params'         => require(__DIR__ . DIRECTORY_SEPARATOR . 'params.php'),
];

/**
 * Search for module configuration
 * to add to the main $config array variable
 */
$modulesDir = dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR;
if (file_exists($modulesDir)) {
    $handle = opendir($modulesDir);
    while (false !== ($file = readdir($handle))) {
        if ($file != '.' && $file != '..' && is_dir($modulesDir . $file)) {
            $configFile = $modulesDir . $file . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'main.php';
            if (file_exists($configFile)) {
                $config = yii\helpers\ArrayHelper::merge($config, require($configFile));
            }
        }
    }
    closedir($handle);
}

// Setup development environment
if (YII_ENV_DEV) {
    // Debug module
    // $config['bootstrap'][]      = 'debug';
    // $config['modules']['debug'] = [
    //     'class'      => 'yii\debug\Module',
    //     'allowedIPs' => DEVIPS,
    // ];
    // Gii module
    $config['bootstrap'][]    = 'gii';
    $config['modules']['gii'] = [
        'class'      => 'yii\gii\Module',
        'allowedIPs' => defined('DEVIPS') ? DEVIPS : ['127.0.0.1'],
        'generators' => [
            'model' => [
                'class'     => 'yii\gii\generators\model\Generator',
                'templates' => [
                    'FrontlineModel' => '@app/templates/model/default',
                ],
            ],
            'crud'  => [
                'class'     => 'yii\gii\generators\crud\Generator',
                'templates' => [
                    'FrontlineCrud' => '@app/templates/crud/default',
                ],
            ],
            'module'  => [
                'class'     => 'yii\gii\generators\module\Generator',
                'templates' => [
                    'FrontlineCrud' => '@app/templates/module/default',
                ],
            ],
        ],
    ];
}

return $config;
