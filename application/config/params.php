<?php
/**
 *
 * @package     Frontline WebApp Base
 *
 * @subpackage  <Application parameters>
 *
 * @author      Sebastian Costiug  <sebastian@frontline.ro>
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @category    config
 * @see         http://www.yiiframework.com/doc-2.0/guide-concept-configurations.html
 *
 * @since       2017.06.21
 *
 */

return [
    'bsVersion' => '4.x',
    'allowedLanguages' => [
        'en' => 'English',
        'ro' => 'Română',
        'de' => 'Deutsch',
        'fr' => 'Français',
        'es' => 'Español',
        'it' => 'Italiano',
    ]
];
