<?php
/**
 * @package     Frontline WebApp Base
 *
 * @subpackage  <Site Controller>
 *
 * @author      Sorin Pohontu <sorin@frontline.ro> & Sebastian Costiug <sebastian@frontline.ro>
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @category    controllers
 * @see         https://www.yiiframework.com/doc/guide/2.0/en/structure-controllers
 *
 * @since       2020.09.07
 */

namespace app\controllers;

use app\components\BaseController;
use app\helpers\DbOps;
use Yii;
use yii\helpers\Html;

/**
 * Site controller
 */
class SiteController extends BaseController
{
    /**
     * actionIndex
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->redirect(['/dashboard/index']);
    }

    /**
     * actionError
     * @see http://www.yiiframework.com/doc-2.0/guide-runtime-handling-errors.html
     * @return mixed
     */
    public function actionError()
    {
        $exception = Yii::$app->errorHandler->exception;
        // "SQLSTATE[HY000] [1045] Access denied for user ''@'localhost' (using password: NO)"
        if ($exception->getCode() === 1045) {
            return $this->redirect(['/installer/install/install']);
        } elseif ($exception !== null) {
            return $this->render('error', [
                'message' => nl2br(Html::encode((YII_DEBUG ? $exception : $exception->getMessage()))),
            ]);
        }
    }

    /**
     * actionMigrateUp
     * @see http://www.yiiframework.com/doc-2.0/guide-runtime-handling-errors.html
     * @return mixed
     */
    public function actionMigrateUp()
    {
        DbOps::migrateUp();

        Yii::$app->session->setFlash('success', Yii::t('app', 'Application was updated succesfully.'));

        return $this->redirect(['/site/index']);
    }
}
