<?php
/**
 *
 * @package     Frontline WebApp Base
 *
 * @subpackage  Dashboard Controller
 *
 * @author      Sebastian Costiug, sebastian@frontline.ro
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @category    controllers
 * @see         https://www.yiiframework.com/doc/guide/2.0/en/structure-controller
 *
 * @since       2020.07.16
 *
 */

namespace app\controllers;

use app\components\BaseController;
use app\modules\user\components\AccessRule;
use app\modules\user\models\User;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

/**
 * DashboardController implements the CRUD actions for Dashboard model.
 */
class DashboardController extends BaseController
{
    /**
     * RBAC behaviours
     * @return mixed
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class'      => AccessControl::className(),
                // We will override the default rule config with the new AccessRule class
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'only'       => ['index'],
                'rules'      => [
                    [
                        'actions' => ['index'],
                        'allow'   => true,
                        'roles'   => [
                            User::ROLE_USER,
                            User::ROLE_ADMIN,
                        ],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'index' => ['GET'],
                ],
            ],
        ];
    }

    /**
     * Lists all Dashboard models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = [];
        $dataProvider['inactive'] = count(User::find()->where(['active' => 0])->all());
        $dataProvider['totalUsers'] = count(User::find()->all());

        foreach (User::find()->orderBy('register_at DESC')->limit(3)->all() as $entry) {
            $dataProvider['lastRegistered'][] = $entry;
        }

        foreach (User::find()->where(['active' => 0])->limit(3)->all() as $entry) {
            $dataProvider['lastInactive'][] = $entry;
        }

        foreach (Yii::$app->audit->recentEntries()->limit(10)->all() as $entry) {
            $dataProvider['audit'][] = $entry;
        }

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
}
