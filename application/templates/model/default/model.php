<?php
/**
 * This is the template for generating the model class of a specified table.
 */

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\model\Generator */
/* @var $tableName string full table name */
/* @var $className string class name */
/* @var $queryClassName string query class name */
/* @var $tableSchema yii\db\TableSchema */
/* @var $properties array list of properties (property => [type, name. comment]) */
/* @var $labels string[] list of attribute labels (name => label) */
/* @var $rules string[] list of validation rules */
/* @var $relations array list of relations (name => relation declaration) */

echo "<?php\n";
?>
/**
 *
 * @package     <?= Yii::$app->name . "\n" ?>
 *
 * @subpackage  <?= $className ?> Model
 *
 * @author      <AUTHOR_NAME>, <AUTHOR_EMAIL>@frontline.ro
 * @copyright   2017-<?=date('Y')?> Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @category    models
 * @see         https://www.yiiframework.com/doc/guide/2.0/en/structure-models
 *
 * @since       <?= date('Y.m.d') . "\n" ?>
 *
 */

namespace <?=$generator->ns?>;

use Yii;
use yii\behaviors\AttributesBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use app\helpers\VarHelper;

/**
 * This is the model class for table "{{%<?= $tableName ?>}}".
 *
<?php foreach ($properties as $property => $data): ?>
 * @property <?="{$data['type']} \${$property}" . ($data['comment'] ? ' ' . strtr($data['comment'], ["\n" => ' ']) : '') . "\n"?>
<?php endforeach;?>
<?php if (!empty($relations)): ?>
<?php foreach ($relations as $name => $relation): ?>
 * @property <?=$relation[1] . ($relation[2] ? '[]' : '') . ' $' . lcfirst($name) . "\n"?>
<?php endforeach;?>
<?php endif;?>
 */
class <?=$className?> extends <?='\\' . ltrim($generator->baseClass, '\\') . "\n"?>
{
    /**
     * {@inheritdoc}
     * @return string
     */
    public static function tableName()
    {
        return '{{%<?= $tableName ?>}}';
    }
<?php if ($generator->db !== 'db'): ?>

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     * @return string
     */
    public static function getDb()
    {
        return Yii::$app->get('<?=$generator->db?>');
    }
<?php endif;?>

    /**
     * {@inheritdoc}
     * @return array
     */
    public function behaviors()
    {
        return [
            'AuditBehavior' => [
                'class' => 'app\modules\log\components\AuditBehavior',
                // 'ignored' => ['model_field'], // optional, not needed if you use allowed
                // 'allowed' => ['model_field'], // optional, not needed if you use ignore
            ],
            [
                'class' => AttributesBehavior::className(),
                'attributes' => [
                <?php foreach ($properties as $property => $label) {
    switch ($property) {
        case 'created':
            echo str_repeat(' ', 4) . "'$property' => [\n";
            echo str_repeat(' ', 24) . "ActiveRecord::EVENT_BEFORE_INSERT => new Expression('NOW()'),\n";
            echo str_repeat(' ', 20) . "],\n";
            break;
        case 'created_by':
            echo str_repeat(' ', 20) . "'$property' => [\n";
            echo str_repeat(' ', 24) . "ActiveRecord::EVENT_BEFORE_INSERT => VarHelper::getUserId(),\n";
            echo str_repeat(' ', 20) . "],\n";
            break;
        case 'updated':
            echo str_repeat(' ', 20) . "'$property' => [\n";
            echo str_repeat(' ', 24) . "ActiveRecord::EVENT_BEFORE_UPDATE => new Expression('NOW()'),\n";
            echo str_repeat(' ', 20) . "],\n";
            break;
        case 'updated_by':
            echo str_repeat(' ', 20) . "'$property' => [\n";
            echo str_repeat(' ', 24) . "ActiveRecord::EVENT_BEFORE_UPDATE => VarHelper::getUserId(),\n";
            echo str_repeat(' ', 20) . "],\n";
            break;
    }
}?>
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     * @return array
     */
    public function rules()
    {
        return [<?=empty($rules) ? '' : ("\n            " . implode(",\n            ", $rules) . ",\n        ")?>];
    }

    /**
     * {@inheritdoc}
     * @return array
     */
    public function attributeLabels()
    {
        return [
<?php foreach ($labels as $name => $label): ?>
            <?="'$name' => " . $generator->generateString($label) . ",\n"?>
<?php endforeach;?>
            'name' => Yii::t('app', 'Name'),
        ];
    }
<?php foreach ($relations as $name => $relation): ?>

    /**
     * Gets query for [[<?=$name?>]].
     * @return <?=$relationsClassHints[$name] . "\n"?>
     */
    public function get<?=$name?>()
    {
        <?=$relation[0] . "\n"?>
    }
<?php endforeach;?>

    /**
     * Gets the full name of the selected model
     * @return string
     */
    public function getName()
    {
        if ($this->hasAttribute('first_name') || $this->hasAttribute('last_name')) {
            $name = trim(($this->first_name ? $this->first_name : '') . ($this->last_name ? ' ' . $this->last_name : ''));
        } else {
            $name = '';
        }

        return $name;
    }
<?php if ($queryClassName): ?>
<?php
$queryClassFullName = ($generator->ns === $generator->queryNs) ? $queryClassName : '\\' . $generator->queryNs . '\\' . $queryClassName;
echo "\n";
?>

    /**
     * {@inheritdoc}
     * @return <?=$queryClassFullName?> the active query used by this AR class.
     */
    public static function find()
    {
        return new <?=$queryClassFullName?>(get_called_class());
    }
<?php endif;?>
}
