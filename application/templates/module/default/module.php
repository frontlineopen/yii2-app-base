<?php
/**
 * This is the template for generating a module class file.
 */

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\module\Generator */

$className = $generator->moduleClass;
$pos = strrpos($className, '\\');
$ns = ltrim(substr($className, 0, $pos), '\\');
$className = substr($className, $pos + 1);

echo "<?php\n";
?>
/**
 *
 * @package     <?= Yii::$app->name . "\n" ?>
 *
 * @subpackage  <?= ucfirst($generator->moduleID) ?> Module
 *
 * @author      <AUTHOR_NAME>, <AUTHOR_EMAIL>@frontline.ro
 * @copyright   2017-<?= date('Y') ?> Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @category    modules
 * @see         https://www.yiiframework.com/doc/guide/2.0/en/structure-modules
 *
 * @since       <?= date('Y.m.d') . "\n" ?>
 *
 */

namespace <?= $ns ?>;

/**
 * <?= $generator->moduleID ?> module definition class
 */
class <?= $className ?> extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = '<?= $generator->getControllerNamespace() ?>';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

    }
}
