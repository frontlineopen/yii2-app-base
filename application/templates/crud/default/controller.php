<?php
/**
 * This is the template for generating a CRUD controller class file.
 */

use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$controllerClass  = StringHelper::basename($generator->controllerClass);
$modelClass       = StringHelper::basename($generator->modelClass);
$searchModelClass = StringHelper::basename($generator->searchModelClass);
if ($modelClass === $searchModelClass) {
    $searchModelAlias = $searchModelClass . 'Search';
}

/* @var $class ActiveRecordInterface */
$class               = $generator->modelClass;
$pks                 = $class::primaryKey();
$urlParams           = $generator->generateUrlParams();
$actionParams        = $generator->generateActionParams();
$actionParamComments = $generator->generateActionParamComments();

echo "<?php\n";
?>
/**
 *
 * @package     <?= Yii::$app->name . "\n" ?>
 *
 * @subpackage  <?= $controllerClass . "\n" ?>
 *
 * @author      <AUTHOR_NAME>, <AUTHOR_EMAIL>@frontline.ro
 * @copyright   2017-<?= date('Y') ?> Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @category    controllers
 * @see         https://www.yiiframework.com/doc/guide/2.0/en/structure-controller
 *
 * @since       <?= date('Y.m.d') . "\n" ?>
 *
 */

namespace <?= StringHelper::dirname(ltrim($generator->controllerClass, '\\')) ?>;

use Yii;
use <?= ltrim($generator->modelClass, '\\') ?>;
<?php if (!empty($generator->searchModelClass)): ?>
use <?= ltrim($generator->searchModelClass, '\\') . (isset($searchModelAlias) ? " as $searchModelAlias" : '') ?>;
<?php else: ?>
use yii\data\ActiveDataProvider;
<?php endif; ?>
use app\components\BaseController;
use app\modules\user\components\AccessRule;
use app\modules\user\models\User;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\BaseJson;

/**
 * <?= $controllerClass ?> implements the CRUD actions for <?= $modelClass ?> model.
 */
class <?= $controllerClass ?> extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class'      => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'only'       => ['index', 'view', 'add', 'update', 'delete'],
                'rules'      => [
                    [
                        'actions' => ['index', 'view', 'add', 'update', 'delete'],
                        'allow'   => true,
                        'roles'   => [
                            User::ROLE_USER,
                            User::ROLE_ADMIN,
                        ],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'index'  => ['GET', 'POST'],
                    'add'    => ['GET', 'POST'],
                    'view'   => ['GET'],
                    'update' => ['GET', 'POST'],
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all <?= $modelClass ?>s.
     * @return mixed
     */
    public function actionIndex()
    {
<?php if (!empty($generator->searchModelClass)): ?>
        $searchModel = new <?= isset($searchModelAlias) ? $searchModelAlias : $searchModelClass ?>();

        $params = $this->request->queryParams;

        if (count($params) <= 1) {
            $params = Yii::$app->session['<?= lcfirst($modelClass) . 'params' ?>'];
            if (isset(Yii::$app->session['<?= lcfirst($modelClass) . 'params' ?>']['page'])) {
                $this->request->setQueryParams(['page' => Yii::$app->session['<?= lcfirst($modelClass) . 'params' ?>']['page']]);
            }
        } else {
            Yii::$app->session['<?= lcfirst($modelClass) . 'params' ?>'] = $params;
        }

        $dataProvider = $searchModel->search($params);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
<?php else: ?>
        $dataProvider = new ActiveDataProvider([
            'query' => <?= $modelClass ?>::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
<?php endif; ?>
    }

    /**
     * Displays a single <?= $modelClass ?>.
     * <?= implode("\n     * ", $actionParamComments) . "\n" ?>
     * @return mixed
     */
    public function actionView(<?= $actionParams ?>)
    {
        $model = $this->findModel(<?= $actionParams ?>);
        Yii::$app->audit->logModelView($model);
        if ($model) {
            if ($this->request->isAjax) {
                return BaseJson::encode([
                    'title' => Yii::t('app', 'View <?= lcfirst($modelClass) ?>: {name}', ['name' => $model->name]),
                    'body'  => $this->renderAjax('view', ['model' => $model]),
                ]);
            } else {
                return $this->render('view', ['model' => $model]);
            }
        } else {
            Yii::$app->session->setFlash('error', Yii::t('app', '<?= $modelClass ?> not found!'));
        }
    }

    /**
     * Creates a new <?= $modelClass ?>.
     * @return mixed
     */
    public function actionAdd()
    {
        $model = new <?= $modelClass ?>();

        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
                if ($this->request->isAjax) {
                    $result = [];

                    if ($model->validate()) {
                        try {
                            $model->save();
                            $result['status']  = 'success';
                            $result['message'] = Yii::t('app', '<?= $modelClass ?> <b>{name}</b> has been added.', ['name' => $model->name]);
                        } catch (CDbException $e) {
                            $result['status']  = 'error';
                            $result['message'] = Yii::t('app', $e->errorInfo);
                        } catch (yii\db\IntegrityException $e) {
                            $result['status']  = 'error';
                            $result['message'] = Yii::t('app', $e->errorInfo);
                        }
                    } else {
                        $result['status'] = 'error';
                        $result['message'] = Yii::t('app', 'Please check input !');
                        foreach ($model->getErrors() as $field => $error) {
                            $result['errors']['<?= lcfirst($modelClass) ?>-' . $field] = $error;
                        }
                    }

                    return BaseJson::encode($result);
                } else {
                    if ($model->validate()) {
                        try {
                            $model->save();
                            Yii::$app->session->setFlash('success', Yii::t('app', '<?= $modelClass ?> <b>{name}</b> has been added.', ['name' => $model->name]));

                            return $this->redirect(['index']);
                        } catch (CDbException $e) {
                            Yii::$app->session->setFlash('error', Yii::t('app', $e->errorInfo));
                        } catch (yii\db\IntegrityException $e) {
                            Yii::$app->session->setFlash('error', Yii::t('app', $e->errorInfo));
                        }
                    } else {
                        Yii::$app->session->setFlash('error', Yii::t('app', 'Please check input!'));
                    }
                }
            }
        }

        if ($this->request->isAjax) {
            return BaseJson::encode([
                'title' => Yii::t('app', 'Add <?= lcfirst($modelClass) ?>'),
                'body'  => $this->renderAjax('add', ['model' => $model]),
            ]);
        } else {
            return $this->render('add', ['model' => $model,]);
        }
    }

    /**
     * Updates an existing <?= $modelClass ?>.
     * <?= implode("\n     * ", $actionParamComments) . "\n" ?>
     * @return mixed
     */
    public function actionUpdate(<?= $actionParams ?>)
    {
        $model = $this->findModel(<?= $actionParams ?>);

        if ($model) {
            if ($this->request->isPost) {
                if ($model->load($this->request->post())) {
                    if ($this->request->isAjax) {
                        $result = [];

                        if ($model->validate()) {
                            try {
                                $model->save();
                                $result['status']  = 'success';
                                $result['message'] = Yii::t('app', '<?= $modelClass ?> <b>{name}</b> has been updated.', ['name' => $model->name]);
                            } catch (CDbException $e) {
                                $result['status']  = 'error';
                                $result['message'] = Yii::t('app', $e->errorInfo);
                            } catch (yii\db\IntegrityException $e) {
                                $result['status']  = 'error';
                                $result['message'] = Yii::t('app', $e->errorInfo);
                            }
                        } else {
                            $result['status'] = 'error';
                            $result['message'] = Yii::t('app', 'Please check input!');
                            foreach ($model->getErrors() as $field => $error) {
                                $result['errors']['<?= lcfirst($modelClass) ?>-' . $field] = $error;
                            }
                        }

                        return BaseJson::encode($result);
                    } else {
                        if ($model->validate()) {
                            try {
                                $model->save();
                                Yii::$app->session->setFlash('success', Yii::t('app', '<?= $modelClass ?> <b>{name}</b> has been updated.', ['name' => $model->name]));

                                return $this->redirect(['index']);
                            } catch (CDbException $e) {
                                Yii::$app->session->setFlash('error', Yii::t('app', $e->errorInfo));
                            } catch (yii\db\IntegrityException $e) {
                                Yii::$app->session->setFlash('error', Yii::t('app', $e->errorInfo));
                            }
                        } else {
                            Yii::$app->session->setFlash('error', Yii::t('app', 'Please check input!'));
                        }
                    }
                }
            }

            if ($this->request->isAjax) {
                return BaseJson::encode([
                    'title' => Yii::t('app', 'Update <?= lcfirst($modelClass) ?>: {name}', ['name' => $model->name]),
                    'body'  => $this->renderAjax('update', ['model' => $model]),
                ]);
            } else {
                return $this->render('update', ['model' => $model]);
            }
        } else {
            Yii::$app->session->setFlash('error', Yii::t('app', '<?= $modelClass ?> not found!'));
        }
    }

    /**
     * Deletes an existing <?= $modelClass ?>.
     * <?= implode("\n     * ", $actionParamComments) . "\n" ?>
     * @return mixed
     */
    public function actionDelete(<?= $actionParams ?>)
    {
        if ($this->request->isPost) {
            $model = $this->findModel(<?= $actionParams ?>);

            if ($model) {
                if ($this->request->isAjax) {
                    $result = [];
                    try {
                        $name = $model->name;
                        $model->delete();
                        $result['status']  = 'success';
                        $result['message'] = Yii::t('app', '<?= $modelClass ?> <b>{name}</b> has been deleted', ['name' => $name]);
                    } catch (CDbException $e) {
                        $result['status']  = 'error';
                        $result['message'] = Yii::t('app', $e->errorInfo);
                    } catch (yii\db\IntegrityException $e) {
                        $result['status']  = 'error';
                        $result['message'] = Yii::t('app', $e->errorInfo);
                    }

                    return BaseJson::encode($result);
                } else {
                    try {
                        $model->delete();
                        Yii::$app->session->setFlash('success', Yii::t('app', '<?= $modelClass ?> <b>{name}</b> has been deleted', ['name' => $model->name]));

                        return $this->redirect(['index']);
                    } catch (CDbException $e) {
                        Yii::$app->session->setFlash('error', Yii::t('app', $e->errorInfo));
                    } catch (yii\db\IntegrityException $e) {
                        Yii::$app->session->setFlash('error', Yii::t('app', $e->errorInfo));
                    }
                }
            } else {
                Yii::$app->session->setFlash('error', Yii::t('app', '<?= $modelClass ?> not found!'));
            }
        } else {
            throw new \yii\web\HttpException(400, 'Invalid request!');
        }
    }

    /**
     * Finds the <?= $modelClass ?> model based on its primary key value.
     * <?= implode("\n     * ", $actionParamComments) . "\n" ?>
     * @return <?= $modelClass ?> the loaded model
     */
    protected function findModel(<?= $actionParams ?>)
    {
<?php
if (count($pks) === 1) {
    $condition = '$id';
} else {
    $condition = [];
    foreach ($pks as $pk) {
        $condition[] = "'$pk' => \$$pk";
    }
    $condition = '[' . implode(', ', $condition) . ']';
}
?>
        if (($model = <?= $modelClass ?>::findOne(<?= $condition ?>)) !== null) {
            return $model;
        } else {
            return false;
        }
    }
}
