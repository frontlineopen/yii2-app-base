<?php
/**
 * This is the template for generating CRUD search class of the specified model.
 */

use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$modelClass       = StringHelper::basename($generator->modelClass);
$searchModelClass = StringHelper::basename($generator->searchModelClass);
if ($modelClass === $searchModelClass) {
    $modelAlias = $modelClass . 'Model';
}
$rules            = $generator->generateSearchRules();
$labels           = $generator->generateSearchLabels();
$searchAttributes = $generator->getSearchAttributes();
$searchConditions = $generator->generateSearchConditions();

echo "<?php\n";
?>
/**
 *
 * @package     <?= Yii::$app->name . "\n"?>
 *
 * @subpackage  <?= $searchModelClass ?> Models
 *
 * @author      <AUTHOR_NAME>, <AUTHOR_EMAIL>@frontline.ro
 * @copyright   2017-<?= date('Y') ?> Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @category    models
 * @see         https://www.yiiframework.com/doc/guide/2.0/en/structure-models
 *
 * @since       <?= date('Y.m.d') . "\n"?>
 *
 */

namespace <?= StringHelper::dirname(ltrim($generator->searchModelClass, '\\')) ?>;

use yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use <?= ltrim($generator->modelClass, '\\') . (isset($modelAlias) ? " as $modelAlias" : "") ?>;

/**
 * <?= $searchModelClass ?> represents the model behind the search form of `<?= $generator->modelClass ?>`.
 */
class <?= $searchModelClass ?> extends <?= isset($modelAlias) ? $modelAlias : $modelClass ?>

{
    /* Model name */
    public $name;

    /**
     * {@inheritdoc}
     * @return array
     */
    public function rules()
    {
        return [
            <?= implode(",\n            ", $rules) ?>,
        ];
    }

    /**
     * {@inheritdoc}
     * @return mixed
     */
    public function scenarios()
    {
        // Bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = <?= isset($modelAlias) ? $modelAlias : $modelClass ?>::find();

        // Add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => Yii::$app->user->getPageSize(),
            ],
        ]);

        // Sort by name
        if ($this->hasAttribute('first_name') || $this->hasAttribute('last_name')) {
            $dataProvider->setSort([
                'attributes' => [
                    'id',
                    'name' => [
                        'asc'     => ['first_name' => SORT_ASC, 'last_name' => SORT_ASC],
                        'desc'    => ['first_name' => SORT_DESC, 'last_name' => SORT_DESC],
                        'label'   => Yii::t('app', 'Name'),
                        'default' => SORT_ASC,
                    ],
                ]
            ]);
        }

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // Grid filtering conditions
        <?= implode("\n        ", $searchConditions) ?>

        // Search by name
        if ($this->hasAttribute('first_name') || $this->hasAttribute('last_name')) {
            $query->andWhere('first_name LIKE "%' . $this->name . '%" ' . 'OR last_name LIKE "%' . $this->name . '%" ' . 'OR CONCAT(first_name, " ", last_name) LIKE "%' . $this->name . '%"');
        }

        return $dataProvider;
    }

    /**
     * Cleaner index URL with filter params
     */
    public function formName()
    {
        return '';
    }
}
