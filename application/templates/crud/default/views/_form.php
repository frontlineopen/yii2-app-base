<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

$model = new $generator->modelClass();
$safeAttributes = $model->safeAttributes();
if (empty($safeAttributes)) {
    $safeAttributes = $model->attributes();
}

echo "<?php\n";
?>

use app\assets\AppAsset;
use yii\helpers\Html;
use kartik\form\ActiveForm;

AppAsset::register($this);
?>

<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-form">

    <?= "<?php " ?>$form = ActiveForm::begin(); ?>

<?php foreach ($generator->getColumnNames() as $attribute) {
    if (in_array($attribute, $safeAttributes)) {
        echo "    <?= " . $generator->generateActiveField($attribute) . " ?>\n";
    }
} ?>

    <div class="form-actions text-right">
        <?= "<?= " ?>Html::a(<?= $generator->generateString('Close') ?>, null, ['class' => 'btn btn-info float-left', 'data-dismiss' => 'modal']) ?>
        <?= "<?= " ?>Html::submitButton(<?= $generator->generateString('Save') ?>, ['class' => 'btn btn-primary btn-save-and-continue']) ?>
    </div>

    <?= "<?php " ?>ActiveForm::end(); ?>

</div>
