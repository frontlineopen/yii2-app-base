<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

$urlParams = $generator->generateUrlParams();
$nameAttribute = $generator->getNameAttribute();

echo "<?php\n";
?>

use yii\helpers\Html;
use yii\helpers\Url;
use <?= $generator->indexWidgetType === 'grid' ? "kartik\\grid\\GridView" : "yii\\widgets\\ListView" ?>;
<?= $generator->enablePjax ? 'use yii\widgets\Pjax;' : '' ?>

$this->title = <?= $generator->generateString(Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass)))) ?>;
?>

<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-index pjax-grid-container card m-auto">
<?= $generator->enablePjax ? "    <?php Pjax::begin(); ?>\n" : '' ?>
<?php if(!empty($generator->searchModelClass)): ?>
    <?php if($generator->indexWidgetType !== 'grid'): ?>
        <?= "    <?= " . $this->render('_search', ['model' => $searchModel]); ?>
    <?php endif; ?>
<?php endif; ?>
<?php if ($generator->indexWidgetType === 'grid'): ?>
<div class="card-body">

        <?= "<?= " ?>GridView::widget([
            'id'           => 'grid-view',
            'dataProvider' => $dataProvider,
            <?= !empty($generator->searchModelClass) ? "'filterModel'  => \$searchModel,\n" . str_repeat(' ', 12) . "'columns'      => [\n" : str_repeat(' ', 12) . "'columns'      => [\n"; ?>
                // ['class' => 'yii\grid\SerialColumn'],
<?php
            $count = 0;
            if (($tableSchema = $generator->getTableSchema()) === false) {
                foreach ($generator->getColumnNames() as $name) {
                    if (++$count < 6) {
                        echo str_repeat(' ', 16) . "[\n";
                        echo str_repeat(' ', 20) . "'attribute' => '" . $name . "',\n";
                        echo str_repeat(' ', 16) . "],\n";
                    } else {
                        echo str_repeat(' ', 16) . "// [\n";
                        echo str_repeat(' ', 20) . "//" . str_repeat(' ', 4) . "'attribute' => '" . $name . "',\n";
                        echo str_repeat(' ', 16) . "// ],\n";
                    }
                }
            } else {
                foreach ($tableSchema->columns as $column) {
                    $format = $generator->generateColumnFormat($column);
                    if (++$count < 6) {
                        echo str_repeat(' ', 16) . "[\n";
                        echo str_repeat(' ', 20) . "'attribute' => '" . $column->name . "',\n";
                        echo str_repeat(' ', 20) . "'format'    => '" . ($format === 'text' ? "text" : $format) . "',\n";
                        echo str_repeat(' ', 16) . "],\n";
                    } else {
                        echo str_repeat(' ', 16) . "// [\n";
                        echo str_repeat(' ', 20) . "//" . str_repeat(' ', 4) . "'attribute' => '" . $column->name . "',\n";
                        echo str_repeat(' ', 20) . "//" . str_repeat(' ', 4) . "'format'    => '" . ($format === 'text' ? "text" : $format) . "',\n";
                        echo str_repeat(' ', 16) . "// ],\n";
                    }
                }
            }
            ?>
                [
                    'class'          => 'kartik\grid\ActionColumn',
                    'visibleButtons' => [
                        'update' => !Yii::$app->user->isGuest,
                        'delete' => !Yii::$app->user->isGuest,
                    ],
                ],
            ],
            'options'      => [
                'data-addurl' => Url::to(['/' . Yii::$app->controller->module->id . '/' . Yii::$app->controller->id . '/add'])
            ],
        ]) ?>
<?php else: ?>
    <?= "<?= " ?>ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item'],
        'itemView' => function ($model, $key, $index, $widget) {
            return Html::a(Html::encode($model-><?= $nameAttribute ?>), ['view', <?= $urlParams ?>]);
        },
    ]) ?>
<?php endif; ?>

<?= $generator->enablePjax ? "    <?php Pjax::end(); ?>\n" : '' ?>
    </div>
</div>
