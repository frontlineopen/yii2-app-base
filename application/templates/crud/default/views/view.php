<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

$title = "Yii::t('app', 'View contact: {name}', ['name' => \$model->name])";

echo "<?php\n";
?>

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

\yii\web\YiiAsset::register($this);

$this->title = <?=$title?>;
?>
<div class="<?=Inflector::camel2id(StringHelper::basename($generator->modelClass))?>-view">

    <?="<?= "?>DetailView::widget([
        'model'      => $model,
        'options'    => [
            'class' => 'table table-responsive-sm table-sm',
        ],
        'attributes' => [
<?php
if (($tableSchema = $generator->getTableSchema()) === false) {
    foreach ($generator->getColumnNames() as $name) {
        echo str_repeat(' ', 12) . "[\n";
        echo str_repeat(' ', 16) . "'attribute' => '" . $name . "',\n";
        echo str_repeat(' ', 16) . "'visible'   => !empty(\$model->" . $name . "),\n";
        echo str_repeat(' ', 12) . "],\n";
    }
} else {
    foreach ($generator->getTableSchema()->columns as $column) {
        $format = $generator->generateColumnFormat($column);
        echo str_repeat(' ', 12) . "[\n";
        echo str_repeat(' ', 16) . "'attribute' => '" . $column->name . "',\n";
        echo str_repeat(' ', 16) . "'format'    => '" . ($format === 'text' ? "text" : $format) . "',\n";
        echo str_repeat(' ', 16) . "'visible'   => !empty(\$model->" . $column->name . "),\n";
        echo str_repeat(' ', 12) . "],\n";
    }
}
?>
        ],
    ]) ?>

    <div class="text-right">
        <?="<?= "?>Html::a(Yii::t('app', <?=$generator->generateString('Close')?>), null, ['class' => 'btn btn-info float-left', 'data-dismiss' => 'modal']) ?>
        <?="<?= "?>Html::a(Yii::t('app', <?=$generator->generateString('Update')?>), Url::to(['update', 'id' => $model->id]), ['class' => 'btn btn-primary btn-update']) ?>
    </div>

</div>
