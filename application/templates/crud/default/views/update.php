<?php
use yii\helpers\Inflector;
use yii\helpers\StringHelper;

$title = "Yii::t('app', 'Update contact: {name}', ['name' => \$model->name])";

echo "<?php\n";
?>
$this->title = <?=$title?>;
?>
<div class="<?=Inflector::camel2id(StringHelper::basename($generator->modelClass))?>-update">

    <?='<?= '?>$this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
