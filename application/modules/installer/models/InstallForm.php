<?php
/**
 * @package     Frontline WebApp Base
 *
 * @subpackage  <Installer form model>
 *
 * @author      Sebastian Costiug <sebastian@frontline.ro>
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @category    models
 * @see         http://www.yiiframework.com/doc-2.0/guide-structure-models.html
 *
 * @since       2020.07.14
 *
 */

namespace app\modules\installer\models;

use app\modules\installer\helpers\InstallHelper;
use yii;
use yii\base\Model;

/**
 * Installer form model
 */
class InstallForm extends Model
{
    /** Application Settings */
    public $site_name;

    /** Company Settings */
    public $company_name;

    /** Mail */
    public $mail_fromName = 'Site Notifications';
    public $mail_fromEmail;
    public $mail_toName = 'Site Owner';
    public $mail_toEmail;

    /** // Database Information */
    public $dbHost = 'localhost';
    public $dbName;
    public $dbUser;
    public $dbPassword;
    public $dbPrefix;

    // User Model Scenarios
    const SCENARIO_DBINFO  = 'dbInfo';
    const SCENARIO_APPINFO = 'appInfo';

    /**
     * @inheritdoc
     * @return array
     */
    public function rules()
    {
        return [
            // Required
            [['site_name', 'mail_fromEmail', 'mail_fromName'], 'required', 'on' => self::SCENARIO_APPINFO],
            [['dbHost', 'dbName', 'dbUser', 'dbPassword'], 'required', 'on' => self::SCENARIO_DBINFO],
            // Sizes
            [['site_name', 'mail_toName', 'mail_fromName'], 'string', 'max' => 64],
            // All fields are safe
            [['site_name', 'mail_fromEmail', 'mail_toEmail', 'mail_fromName', 'mail_toName', 'company_name', 'dbHost', 'dbName', 'dbUser', 'dbPassword', 'dbPrefix'], 'safe'],
            // EMails
            [['mail_fromEmail', 'mail_toEmail'], 'email'],
            // Filter - convert Email to lowercase
            [['mail_fromEmail', 'mail_toEmail'], 'filter', 'filter' => 'strtolower'],
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'site_name'      => Yii::t('app', 'Application name'),
            'company_name'   => Yii::t('app', 'Owner company'),
            'mail_fromEmail' => Yii::t('app', 'From email'),
            'mail_fromName'  => Yii::t('app', 'From name'),
            'mail_toEmail'   => Yii::t('app', 'To email'),
            'mail_toName'    => Yii::t('app', 'To name'),
            'dbHost'         => Yii::t('app', 'Hostname'),
            'dbName'         => Yii::t('app', 'Database name'),
            'dbUser'         => Yii::t('app', 'Database username'),
            'dbPassword'     => Yii::t('app', 'Database password'),
            'dbPrefix'       => Yii::t('app', 'Tables prefix'),
        ];
    }

    /**
     * Method to save database connection info and run migration
     * @return boolean
     */
    public function saveDbInfo()
    {
        if ($this->validate()) {
            return InstallHelper::saveDbInfo($this);
        }
        Yii::$app->session->setFlash('error', Yii::t('app', 'Please check database credentials!'));

        return false;
    }

    /**
     * Method to save Application basic info to the settings table in database
     * @return boolean
     */
    public function saveAppInfo()
    {
        if ($this->validate()) {
            return InstallHelper::writeToDB($this);
        }

        return false;
    }
}
