<?php
/**
 * @package     Frontline WebApp Base
 *
 * @subpackage  <Installer Controller>
 *
 * @author      Sebastian Costiug <sebastian@frontline.ro>
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @category    controllers
 * @see         http://www.yiiframework.com/doc-2.0/guide-structure-controllers.html
 *
 * @since       2020.07.14
 *
 */

namespace app\modules\installer\controllers;

use app\components\BaseController;
use app\modules\installer\helpers\InstallHelper;
use app\modules\installer\models\InstallForm;
use app\modules\user\models\User;
use yii;
use yii\helpers\FileHelper;
use app\helpers\DbOps;
use app\helpers\VarHelper;

/**
 * Install controller class
 */
class InstallController extends BaseController
{
    /**
     * {@inheritdoc}
     * @return void
     */
    public function init()
    {
        parent::init();
    }

    /**
     * Installer main action
     * starts the instalation process by gathering database information
     * @return mixed
     */
    public function actionInstall()
    {
        if (!Yii::$app->settings->get('system', 'installed')) {
            $model = new InstallForm(['scenario' => InstallForm::SCENARIO_DBINFO]);

            if (Yii::$app->request->isPost) {
                if ($model->load(Yii::$app->request->post())) {
                    if ($model->validate()) {
                        try {
                            if ($model->saveDbInfo()) {
                                Yii::$app->session->setFlash('success', Yii::t('app', 'Database information saved successfully!'));

                                return $this->redirect(['/installer/install/app-settings']);
                            }
                        } catch (yii\db\CDbException $e) {
                            Yii::$app->session->setFlash('error', Yii::t('app', $e->errorInfo));
                        } catch (yii\db\IntegrityException $e) {
                            Yii::$app->session->setFlash('error', Yii::t('app', $e->errorInfo));
                        }
                    } else {
                        Yii::$app->session->setFlash('error', Yii::t('app', 'Please check input data'));
                    }
                } else {
                    throw new \yii\web\HttpException(204, Yii::t('app', 'Invalid request.'));
                }
            }

            return $this->render('install', [
                'model' => $model,
            ]);
        } else {
            return $this->redirect(['/site/index']);
        }
    }

    /**
     * Action to gather application basic information
     * and saves it into the newly created database
     * @return mixed
     */
    public function actionAppSettings()
    {
        if (!Yii::$app->settings->get('system', 'installed')) {
            DbOps::migrateUp();
            $model = new InstallForm(['scenario' => InstallForm::SCENARIO_APPINFO]);

            if (VarHelper::getCurrentSubDomain()) {
                $model->mail_fromEmail = VarHelper::getCurrentSubDomain() . '@' . VarHelper::getCurrentDomain();
            } else {
                $model->mail_fromEmail = 'notifications@' . VarHelper::getCurrentDomain();
            }
            if (Yii::$app->request->isPost) {
                if ($model->load(Yii::$app->request->post())) {
                    if ($model->validate()) {
                        try {
                            if ($model->saveAppInfo()) {
                                Yii::$app->session->setFlash('success', Yii::t('app', 'Application intial settings saved successfully!'));
                                if (Yii::$app->getModule('user') !== null) {
                                    return $this->redirect(['/installer/install/create-admin']);
                                } else {
                                    return $this->redirect(['/site/index']);
                                }
                            } else {
                                Yii::$app->session->setFlash('error', Yii::t('app', 'Application intial settings did not save!'));
                            }
                        } catch (yii\db\CDbException $e) {
                            Yii::$app->session->setFlash('error', Yii::t('app', $e->errorInfo));
                        } catch (yii\db\IntegrityException $e) {
                            Yii::$app->session->setFlash('error', Yii::t('app', $e->errorInfo));
                        }
                    } else {
                        Yii::$app->session->setFlash('error', Yii::t('app', 'Please fill all required fields'));
                    }
                } else {
                    throw new \yii\web\HttpException(204, Yii::t('app', 'Invalid request.'));
                }
            }

            return $this->render('appSettings', [
                'model' => $model,
            ]);
        } else {
            return $this->render('/site/index');
        }
    }

    /**
     * If application uses a database and addmin account is created after DB data is received
     * @return mixed
     */
    public function actionCreateAdmin()
    {
        if (!Yii::$app->settings->get('system', 'installed')) {
            $model = new User(['scenario' => User::SCENARIO_REGISTER_ADMIN]);

            if ($model->load(Yii::$app->request->post()) && $model->password === $model->repeatPassword) {
                $model->setPassword($model->password);
                if ($model->validate()) {
                    $model->register_from = Yii::$app->request->getUserIP();
                    $model->active        = User::STATUS_ACTIVE;
                    $model->role          = User::ROLE_ADMIN;
                    try {
                        if ($model->save()) {
                            $params['password'] = $model->password;
                            if (VarHelper::sendEmail($model->first_name, $model->email, Yii::t('app', 'Welcome to your new application'), 'welcomeEmail', $params)) {
                                Yii::$app->session->setFlash('success', Yii::t('app', 'Thank you for registration. Please check your inbox for an email containing your credentials.'));
                            } else {
                                Yii::$app->session->setFlash('error', Yii::t('app', 'There was an error sending the registration email.'));
                            }
                        } else {
                            Yii::$app->session->setFlash('error', Yii::t('app', 'There was an error saving the administrator account.'));
                        }
                    } catch (CDbException $e) {
                        Yii::$app->session->setFlash('error', Yii::t('app', 'Record did not save!'));
                    } catch (yii\db\IntegrityException $e) {
                        Yii::$app->session->setFlash('error', Yii::t('app', $e->errorInfo));
                    }
                } else {
                    Yii::$app->session->setFlash('error', Yii::t('app', 'Please check input data.'));
                }
                Yii::$app->settings->set('system', 'installed', true);

                return $this->redirect(['/user/user/login']);
                // return $this->redirect(['/installer/install/delete-installer']);
            }

            return $this->render('createAdmin', [
                'model' => $model,
            ]);
        } else {
            return $this->render('/site/index');
        }
    }

    /**
     * Delete installer module
     * @return mixed
     */
    public function actionDeleteInstaller()
    {
        if (FileHelper::removeDirectory(dirname(__DIR__))) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Installer folder deleted.'));

            return $this->redirect(['/user/user/login']);
        } else {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Installer folder not deleted.'));
        }
    }
}
