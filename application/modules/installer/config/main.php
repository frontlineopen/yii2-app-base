<?php
/**
 *
 * @package     Frontline WebApp Base
 *
 * @subpackage  <Installer Module config segment>
 *
 * @author      Sebastian Costiug <sebastian@frontline.ro>
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @category    config
 * @see         https://www.yiiframework.com/doc/guide/2.0/en/concept-configurations
 *
 * @since       2020.06.15
 *
 */

return [
    // Module
    'modules'    => [
        'installer' => [
            'class' => 'app\modules\installer\Module',
        ],
    ],
];
