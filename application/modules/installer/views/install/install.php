<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;

$this->title = Yii::t('app', 'Installer');
$this->context->layout = '//main-full';
?>
<div class="row justify-content-center pt-5">
    <div class="col-md-5">
        <div class="card-group">
            <div class="card">
                <div class="text-center bg-primary text-white p-5 rounded-top">
                    <h1><?= Html::encode($this->title) ?></h1>
                </div>
                <div class="p-4">
                    <p class="text-muted"> <?= Yii::t('app', 'Please fill in your database information') ?></p>
                    <?php $form = ActiveForm::begin(); ?>
                        <?= $form->field($model, 'dbHost', [
                            'addon' => [
                                'prepend' => [
                                    'content' => '<i class="fas fa-server"></i>'
                                ]
                            ]
                        ])->textInput(['class' => 'text-body']) ?>

                        <?= $form->field($model, 'dbName', [
                            'addon' => [
                                'prepend' => [
                                    'content' => '<i class="fas fa-database"></i>'
                                ]
                            ]
                        ])->textInput(['class' => 'text-black-50',
                            'placeholder' => 'name',
                            'autofocus'   => 'autofocus',
                        ]) ?>

                        <?= $form->field($model, 'dbUser', [
                                'addon' => [
                                    'prepend' => [
                                        'content' => '<i class="fas fa-user"></i>'
                                    ]
                                ]
                            ])->textInput(['class' => 'text-black-50',
                            'placeholder' => 'username',
                        ]) ?>

                        <?= $form->field($model, 'dbPassword', [
                                'addon' => [
                                    'prepend' => [
                                        'content' => '<i class="fas fa-lock"></i>'
                                    ]
                                ]
                            ])->textInput(['class' => 'text-black-50',
                            'placeholder' => 'password',
                        ]) ?>

                        <?= $form->field($model, 'dbPrefix', [
                                'addon' => [
                                    'prepend' => [
                                        'content' => '<i class="fas fa-window-minimize"></i>'
                                    ]
                                ]
                            ])->textInput(['class' => 'text-black-50',
                            'placeholder' => 'prefix (if any)',
                        ]) ?>

                        <div id="submit-buttons" class="form-group">
                            <div class="text-right">
                                <?= Html::submitButton(Yii::t('app', 'Check database connection'), ['class' => 'btn btn-success mt-3']) ?>
                            </div>
                        </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
