<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;

$this->title = Yii::t('app', 'Administrator account');
$this->context->layout = '//main-full';
?>
<div class="row justify-content-center pt-5">
    <div class="col-md-5">
        <div class="card-group">
            <div class="card">
                <div class="text-center bg-primary text-white p-5 rounded-top">
                    <h1><?= Html::encode($this->title) ?></h1>
                </div>
                <div class="p-4">
                    <p class="text-muted"><?= Yii::t('app', 'Please fill out the administrator info') ?></p>
                    <div class="mb-1">
                        <?php $form = ActiveForm::begin(['id' => 'form-register']); ?>
                        <?= $form->field($model, 'first_name', [
                        'addon' => [
                            'prepend' => [
                                'content' => '<i class="fas fa-user"></i>'
                                ]
                            ]
                        ])->textInput(['autofocus' => 'autofocus', 'placeholder' => 'Jack']); ?>

                        <?= $form->field($model, 'last_name', [
                            'addon' => [
                                'prepend' => [
                                    'content' => '<i class="fas fa-user"></i>'
                                ]
                            ]
                        ])->textInput(['placeholder' => 'Smith']); ?>

                        <?= $form->field($model, 'email', [
                            'addon' => [
                                'prepend' => [
                                    'content' => '<i class="fas fa-envelope"></i>'
                                ]
                            ]
                        ])->textInput(['placeholder' => 'email@example.com']); ?>

                        <?= $form->field($model, 'phone', [
                        'addon' => [
                            'prepend' => [
                                'content' => '<i class="fas fa-phone"></i>'
                                ]
                            ]
                        ])->textInput(['placeholder' => '1234-567-890']); ?>

                        <?= $form->field($model, 'password', [
                            'addon' => [
                                'prepend' => [
                                    'content' => '<i class="fas fa-lock"></i>'
                                ]
                            ]
                        ])->passwordInput(['placeholder' => '*********']); ?>

                        <?= $form->field($model, 'repeatPassword', [
                            'addon' => [
                                'prepend' => [
                                    'content' => '<i class="fas fa-lock"></i>'
                                ]
                            ]
                        ])->passwordInput(['placeholder' => '*********']); ?>

                        <div class="form-group">
                            <div class="text-right">
                                <?= Html::submitButton(Yii::t('app', 'Create account'), ['class' => 'btn bg-primary text-white', 'name' => 'register-button']) ?>
                            </div>
                        </div>
                        <?php $form->end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
