<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;

$this->title = Yii::t('app', 'Installer');
$this->context->layout = '//main-full';
?>
<div class="row justify-content-center pt-5">
    <div class="col-md-5">
        <div class="card-group">
            <div class="card">
                <div class="text-center bg-primary text-white p-5 rounded-top">
                    <h1><?= Html::encode($this->title) ?></h1>
                </div>
                <div class="p-4">
                    <p class="text-muted"> <?= Yii::t('app', 'Please fill the basic application information') ?></p>
                    <?php $form = ActiveForm::begin(); ?>
                        <?= $form->field($model, 'site_name', [
                            'addon' => [
                                'prepend' => [
                                    'content' => '<i class="fas fa-laptop"></i>'
                                ]
                            ]
                        ])->textInput([
                            'placeholder' => Yii::t('app', 'Application name'),
                            'class'       => 'text-body',
                            'autofocus'   => 'autofocus',
                        ]) ?>

                        <?= $form->field($model, 'company_name', [
                            'addon' => [
                                'prepend' => [
                                    'content' => '<i class="fas fa-building"></i>'
                                ]
                            ]
                        ])->textInput(['class' => 'text-black-50',
                            'placeholder' => Yii::t('app', 'Company name'),
                        ]) ?>

                        <hr>
                        <legend><?= Yii::t('app', 'System emails') ?></legend>

                        <?= $form->field($model, 'mail_fromName', [
                                'addon' => [
                                    'prepend' => [
                                        'content' => '<i class="fas fa-bell"></i>'
                                    ]
                                ]
                            ])->textInput(['class' => 'text-body',
                        ]) ?>

                        <?= $form->field($model, 'mail_fromEmail', [
                            'addon' => [
                                'prepend' => [
                                    'content' => '<i class="fas fa-envelope-open-text"></i>'
                                ]
                            ]
                        ])->textInput(['class' => 'text-black-50',
                            'placeholder' => Yii::t('app', 'Notifications email address'),
                        ]) ?>

                        <div id="submit-buttons" class="form-group">
                            <div class="text-right">
                                <?= Html::submitButton(Yii::t('app', 'Save settings'), ['class' => 'btn btn-success mt-3']) ?>
                            </div>
                        </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
