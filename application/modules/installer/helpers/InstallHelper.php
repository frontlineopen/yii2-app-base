<?php
/**
 *
 * @package     Frontline WebApp Base
 *
 * @subpackage  <Instalation helpers>
 *
 * @author      Sebastian Costiug <sebastian@frontline.ro>
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @category    helpers
 *
 * @since       2020.07.15
 *
 */

namespace app\modules\installer\helpers;

use app\helpers\VarHelper;
use Yii;

/**
 * Instalation type checker class
 */
class InstallHelper
{

    /**
     * Database instalation type variant
     * @param string $formData User trasmited data during install
     * @return boolean
     */
    public static function saveDbInfo($formData)
    {
        if (self::checkDbConnection($formData->dbHost, $formData->dbName, $formData->dbUser, $formData->dbPassword)) {
            return self::saveConfig($formData->dbHost, $formData->dbName, $formData->dbUser, $formData->dbPassword, $formData->dbPrefix);
        }
        return false;
    }


    /**
     * Writes the gathered info into the newly created tables in the database
     * @param string $formData User trasmited data during install
     * @return boolean
     */
    public static function writeToDB($formData)
    {
        foreach ($formData->getAttributes() as $name => $value) {
            if (substr($name, 0, 2) !== 'db') {
                $tmp = explode('_', $name);
                $cat = $tmp[0];
                $key = $tmp[1];
                Yii::$app->settings->set($cat, $key, $value);
            }
        }
        if (VarHelper::getCurrentSubDomain()) {
            $appNotifications = VarHelper::getCurrentSubDomain() . '@' . VarHelper::getCurrentDomain();
        } else {
            $appNotifications = 'notifications@' . VarHelper::getCurrentDomain();
        }

        if (!Yii::$app->settings->set('mail', 'fromEmail', $appNotifications)) {
            return false;
        }
        if (!Yii::$app->settings->set('site', 'copyrightYearFrom', date('Y'))) {
            return false;
        }
        if (!Yii::$app->settings->set('pagination', 'defaultSize', 25)) {
            return false;
        }
        if (!Yii::$app->settings->set('pagination', 'allowedSize', '5,10,15,20,25,50,100')) {
            return false;
        }
        if (!Yii::$app->settings->set('system', 'defaultLanguage', 'en')) {
            return false;
        }
        return true;
    }

    /**
     * Check Database connection
     * @param string $host     Database host name
     * @param string $database Database name
     * @param string $user     Database user name
     * @param string $password Database password
     * @return boolean
     */
    public static function checkDbConnection($host, $database, $user, $password)
    {
        $dbLink = new yii\db\Connection([
        'dsn'      => "mysql:host=$host;dbname=$database",
        'username' => $user,
        'password' => $password,
        'charset'  => 'utf8',
        ]);

        try {
            $dbLink->open();
            return true;
        } catch (yii\db\Exception $e) {
            return false;
        }
    }

    /**
     * Check Database connection
     * @param string $host     Database host name
     * @param string $database Database name
     * @param string $user     Database user name
     * @param string $password Database password
     * @param string $prefix   Table prefix
     * @return integer|false
     */
    public static function saveConfig($host, $database, $user, $password, $prefix = null)
    {
        $appId = VarHelper::generateRandomString(8);
        $dbConfigFileContent = "<?php
define('DBHOST', '" . $host . "');
define('DBNAME', '" . $database . "');
define('DBUSER', '" . $user . "');
define('DBPASS', '" . $password . "');
define('DBPREFIX', '" . $prefix . "');
define('APPID', '" . $appId . "');
const DEVIPS = [
    '127.0.0.1',
];
";
        $dbConfigFile = Yii::getAlias('@webroot') . DIRECTORY_SEPARATOR . 'config.php';

        return file_put_contents($dbConfigFile, $dbConfigFileContent);
    }
}
