<?php
/**
 *
 * @package     Frontline WebApp Base
 *
 * @subpackage  <Log View helper>
 *
 * @author      Sebastian Costiug <sebastian@frontline.ro>
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @category    helpers
 *
 * @since       2020.08.05
 *
 */

namespace app\modules\log\helpers;

use app\modules\log\components\AuditBehavior;
use app\modules\log\models\Audit;
use app\modules\user\models\User;
use Yii;

/**
 * Class to help translate the audit data fields into readable values
 */
class AuditHelper
{
    /**
     * Get a list of all user names that operated changes in the log history that is currently saved in the DB
     * @return array
     */
    public static function getAllUsers()
    {
        $modelsAudited = Audit::find()->select(['created_by', 'created_by_name'])->from('{{%audit}}')->distinct()->createCommand()->queryAll();
        $users         = [];
        foreach ($modelsAudited as $auditModel) {
            $users[$auditModel['created_by']] = isset($auditModel['created_by_name']) ? $auditModel['created_by_name'] : $auditModel['created_by'];
        }
        asort($users);

        return $users;
    }

    /**
     * Get a list of all the unique model names that were affected in the log history that is currently saved in the DB
     * @return array
     */
    public static function getAllModels()
    {
        $modelsAudited = Audit::find()->select(['model'])->from('{{%audit}}')->distinct()->createCommand()->queryAll();
        $models        = [];
        foreach ($modelsAudited as $auditModel) {
            $model                        = self::getModelClassName($auditModel['model']);
            $models[$auditModel['model']] = $model;
        }
        asort($models);

        return $models;
    }

    /**
     * Returns the audited model class name
     * @param string $modelPath The model class path saved in the log entry
     * @return string
     */
    public static function getModelClassName($modelPath)
    {
        $modelClassPathArr = explode('\\', $modelPath);
        $modelClassName    = end($modelClassPathArr);

        return $modelClassName;
    }

    /**
     * Formats Audit values into a preformatted db message entry
     * @param string $action     Behavior action
     * @param string $modelClass Model class
     * @param string $modelName  Model Name
     * @param string $field      Modified model field
     * @param string $value      The new field value
     * @param string $oldValue   The old field value
     * @param string $userId     The id of user that operated the action
     * @return mixed
     */
    public static function formatMessage($action, $modelClass, $modelName, $field = null, $value = null, $oldValue = null, $userId = null)
    {
        switch ($action) {
            case AuditBehavior::ACTION_CREATE:
                if (empty($userId) && $modelClass == 'User') {
                    return '<code>' . $modelName . '</code> registered for a new account';
                } else {
                    return 'Added the new ' . $modelClass . ' <code>' . $modelName . '</code>';
                }
                break;
            case AuditBehavior::ACTION_SET:
                return 'Set a new value of <strong>' . $value . '</strong> to <code>' . $field . '</code> field of the ' . $modelClass . ' <code>' . $modelName . '</code>';
                break;
            case AuditBehavior::ACTION_CHANGE:
                return 'Updated <code>' . $field . '</code> of ' . $modelClass . ' <code>' . $modelName . ($oldValue ? ('</code> from  <strong>' . $oldValue . '</strong>') : '</code>') . ' to <strong>' . $value . '</strong>';
                break;
            case AuditBehavior::ACTION_DELETE:
                return 'Deleted record' . ' <code>' . $modelName . '</code> from ' . $modelClass;
                break;
        }
    }
}
