<?php
/**
 *
 * @package     Frontline WebApp Base
 *
 * @subpackage  Application log controller
 *
 * @author      Sebastian Costiug, sebastian@frontline.ro
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @category    controllers
 * @see         https://www.yiiframework.com/doc/guide/2.0/en/structure-controller
 *
 * @since       2020.08.05
 *
 */

namespace app\modules\log\controllers;

use app\components\BaseController;
use app\modules\user\components\AccessRule;
use app\modules\user\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;

/**
 * LogController implements the read/delete actions for Application log file.
 */
class LogController extends BaseController
{
    /**
     * {@inheritdoc}
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class'      => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'only'  => ['view-application-log', 'view-migration-log', 'clear-application-log', 'clear-migration-log'],
                'rules' => [
                    [
                        'actions' => ['view-application-log', 'view-migration-log', 'clear-application-log', 'clear-migration-log'],
                        'allow'   => true,
                        'roles'   => [
                            User::ROLE_ADMIN,
                        ],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'view-application-log'  => ['GET'],
                    'view-migration-log'    => ['GET'],
                    'clear-application-log' => ['POST'],
                    'clear-migration-log'   => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Renders the application log file.
     * @return mixed
     */
    public function actionViewApplicationLog()
    {
        $applicationLog      = Yii::$app->log->targets[0]->logFile;
        $migrationLog = Yii::getAlias('@runtime') . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'migration.log';

        if (file_exists($applicationLog) && (filesize($applicationLog) > 0)) {
            return $this->render('application', [
                'content'   => $this->renderFile($applicationLog),
                'migration' => (file_exists($migrationLog) && (filesize($migrationLog) > 0)),
            ]);
        } else {
            return $this->render('application', [
                'content'   => Yii::t('app', 'Log is empty'),
                'migration' => (file_exists($migrationLog) && (filesize($migrationLog) > 0)),
            ]);
        }
    }

    /**
     * Renders the migration log file.
     * @return mixed
     */
    public function actionViewMigrationLog()
    {
        $migrationLog = Yii::getAlias('@runtime') . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'migration.log';
        if (file_exists($migrationLog) && (filesize($migrationLog) > 0)) {
            return $this->render('migration', ['content' => $this->renderFile($migrationLog)]);
        }
    }

    /**
     *  Clears the content of the application log file.
     * @return mixed
     */
    public function actionClearApplicationLog()
    {
        $applicationLog = Yii::$app->log->targets[0]->logFile;

        if ($filePointer = @fopen($applicationLog, 'a')) {
            @ftruncate($filePointer, 0);
            @fclose($filePointer);
        }

        return $this->redirect(Url::to(['/log/log/view-application-log']));
    }

    /**
     *  Clears the content of the Migration log file.
     * @return mixed
     */
    public function actionClearMigrationLog()
    {
        $migrationLog = Yii::getAlias('@runtime') . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'migration.log';

        if ($filePointer = @fopen($migrationLog, 'a')) {
            @ftruncate($filePointer, 0);
            @fclose($filePointer);
        }

        return $this->redirect(Url::to(['/log/log/view-application-log']));
    }
}
