<?php
/**
 *
 * @package     Frontline WebApp Base
 *
 * @subpackage  Audit log controller
 *
 * @author      Sebastian Costiug, sebastian@frontline.ro
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @category    controllers
 * @see         https://www.yiiframework.com/doc/guide/2.0/en/structure-controller
 *
 * @since       2020.08.05
 *
 */

namespace app\modules\log\controllers;

use app\components\BaseController;
use app\modules\log\models\Audit;
use app\modules\log\models\AuditSearch;
use app\modules\user\components\AccessRule;
use app\modules\user\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\BaseJson;

/**
 * LogController implements the CRUD actions for Audit model.
 */
class AuditController extends BaseController
{
    /**
     * {@inheritdoc}
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class'      => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'only'       => ['index'],
                'rules'      => [
                    [
                        'actions' => ['index'],
                        'allow'   => true,
                        'roles'   => [
                            User::ROLE_ADMIN,
                        ],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'index'  => ['GET'],
                ],
            ],
        ];
    }

    /**
     * Lists all Audits.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AuditSearch();
        $params = Yii::$app->request->queryParams;
        $dataProvider = $searchModel->search($params);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * View an existing User.
     * @param integer $id Id of the selected user.
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        if ($model) {
            if ($this->request->isAjax) {
                return BaseJson::encode([
                    'title' => Yii::t('app', 'Audit entry details') . ': ' . $model->id,
                    'body'  => $this->renderAjax('view', ['model' => $model]),
                ]);
            } else {
                return $this->render('view', ['model' => $model]);
            }
        } else {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Entry not found!'));
        }
    }

    /**
     * Deletes log entries older then an year
     * @return integer
     */
    protected function pruneDb()
    {
        return Yii::$app->db
            ->createCommand('DELETE FROM {{%audit}} WHERE timestamp < GETDATE()-365')
            ->execute();
    }

    /**
     * Finds the Audit model based on its primary key value.
     * @param integer $id The ID of the model to be found
     * @return Audit the loaded model
     */
    protected function findModel($id)
    {
        if (($model = Audit::findOne($id)) !== null) {
            return $model;
        }
    }
}
