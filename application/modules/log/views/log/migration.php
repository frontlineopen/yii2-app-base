<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Migration log');
?>

<div class="card h-100">
    <div class="card-body h-100">
        <?= Html::textArea('logs', $content, ['rows' => 25,  'class' => 'w-100 h-100']) ?>
    </div>
    <div class="card-footer">
        <div class="text-right">
                <?= Html::a(Yii::t('app', 'Back'), !empty(Yii::$app->request->referrer) ? Yii::$app->request->referrer : Url::to(['/site/index']), ['class' => 'btn btn-info float-left']) ?>
                <?= Html::a(Yii::t('app', 'Application log'), Url::to(['/log/log/view-application-log']), ['class' => 'btn btn-warning']) ?>
                <?= Html::a(Yii::t('app', 'Clear log'), Url::to(['/log/log/clear-migration-log']), ['class' => 'btn btn-danger', 'data-method' => 'POST']) ?>
        </div>
    </div>
</div>
