<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

\yii\web\YiiAsset::register($this);

$this->title = Yii::t('app', 'View audit entry details') . ': ' . $model->id;
?>
<div class="audit-view">

    <?= DetailView::widget([
        'model'      => $model,
        'options'    => [
            'class' => 'table table-responsive-sm table-sm',
        ],
        'attributes' => [
            [
                'attribute' => 'model',
                'visible'   => !empty($model->model),
            ],
            [
                'attribute' => 'model_id',
                'visible'   => !empty($model->model_id),
            ],
            [
                'attribute' => 'model_name',
                'visible'   => !empty($model->model_name),
            ],
            [
                'attribute' => 'message',
                'format'    => 'raw',
                'visible'   => !empty($model->message),
            ],
            [
                'attribute' => 'field',
                'visible'   => !empty($model->field),
            ],
            [
                'attribute' => 'action',
                'visible'   => !empty($model->action),
            ],
            [
                'attribute' => 'from',
                'visible'   => !empty($model->from),
            ],
            [
                'attribute' => 'to',
                'visible'   => !empty($model->to),
            ],
            [
                'attribute' => 'created_by_name',
                'visible'   => !empty($model->created_by_name),
            ],
            [
                'attribute' => 'created_by',
                'visible'   => !empty($model->created_by),
            ],
            [
                'attribute' => 'created_from',
                'visible'   => !empty($model->created_from),
            ],
            [
                'attribute' => 'created',
                'visible'   => !empty($model->created),
            ],
        ],
    ]) ?>

    <div class="text-right">
        <?= Html::a(Yii::t('app', Yii::t('app', 'Close')), null, ['class' => 'btn btn-info float-left', 'data-dismiss' => 'modal']) ?>
    </div>
</div>
