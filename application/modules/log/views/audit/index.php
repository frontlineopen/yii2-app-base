<?php

use app\modules\log\helpers\AuditHelper;
use kartik\grid\GridView;

$this->title = Yii::t('app', 'Audit');
?>

<div class="pjax-grid-container m-auto card">
    <div class="card-body">
        <?=GridView::widget([
            'id'           => 'grid-view',
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'condensed'    => true,
            'toolbar'      => [
                '{export}',
            ],
            'columns'      => [
                [
                    'attribute'           => 'created_by_name',
                    'filter'              => AuditHelper::getAllUsers(),
                    'filterType'          => GridView::FILTER_SELECT2,
                    'filterWidgetOptions' => [
                        'options'       => ['prompt' => ''],
                        'pluginOptions' => ['allowClear' => true],
                    ],
                    'headerOptions'       => ['style' => 'width:200px; white-space: nowrap;'],
                ],
                [
                    'attribute'           => 'model',
                    'value'               => function ($model) {
                        return AuditHelper::getModelClassName($model->model);
                    },
                    'filter'              => AuditHelper::getAllModels(),
                    'filterType'          => GridView::FILTER_SELECT2,
                    'filterWidgetOptions' => [
                        'options'       => ['prompt' => ''],
                        'pluginOptions' => ['allowClear' => true],
                    ],
                    'headerOptions'       => ['style' => 'width:100px; white-space: nowrap;'],
                ],
                [
                    'attribute' => 'message',
                    'format'    => 'raw',
                ],
                [
                    'attribute'     => 'created',
                    'headerOptions' => ['style' => 'width:200px; white-space: nowrap;'],
                ],
                [
                    'attribute'     => 'created_from',
                    'headerOptions' => ['style' => 'width:200px; white-space: nowrap;'],
                ],
                [
                    'class'          => 'kartik\grid\ActionColumn',
                    'hidden'         => true,
                    'template'       => '{view}',
                ],

            ],
        ])?>
    </div>
</div>
