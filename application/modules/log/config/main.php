<?php
/**
 *
 * @package     Frontline WebApp Base
 *
 * @subpackage  <Log module config segment>
 *
 * @author      Sebastian Costiug <sebastian@frontline.ro>
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @category    config
 * @see         https://www.yiiframework.com/doc/guide/2.0/en/concept-configurations
 *
 * @since       2020.08.05
 *
 */

return [
    // Application modules
    'modules'    => [
        'log' => [
            'class' => 'app\modules\log\Module',
        ],
    ],

    // Components
    'components' => [
        // Log URLs
        'urlManager' => [
            'rules' => [
                // Audit actions
                'audit/<page:\w+>'      => '/log/audit/index',
                'audit'                 => '/log/audit/index',
                'audit/view/<id:\d+>'   => '/log/audit/view',
                'audit/delete/<id:\d+>' => '/log/audit/delete',

                // Log actions
                'log'                   => '/log/log/view-application-log',
                'log/migration'         => '/log/log/view-migration-log',
                'log/clear-application' => '/log/log/clear-application-log',
                'log/clear-migration'   => '/log/log/clear-migration-log',
            ],
        ],

        // Log component
        'audit'      => [
            'class' => 'app\modules\log\components\AuditBehavior',
        ],
    ],
];
