<?php
/**
 *
 * @package     Frontline WebApp Base
 *
 * @subpackage  <Audit Model>
 *
 * @author      Sam Millman (Sammaye) modified by Sebastian Costiug, sebastian@frontline.ro
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @category    models
 * @see         https://github.com/Sammaye/yii2-audittrail
 *
 * @since       2020.08.04
 *
 */

namespace app\modules\log\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * Audit model class
 */
class Audit extends ActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public static function tableName()
    {
        return '{{%audit}}';
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id'              => Yii::t('app', 'ID'),
            'model'           => Yii::t('app', 'Module'),
            'model_id'        => Yii::t('app', 'Affected model'),
            'model_name'      => Yii::t('app', 'Model name'),
            'field'           => Yii::t('app', 'Model field'),
            'action'          => Yii::t('app', 'Action'),
            'from'            => Yii::t('app', 'From'),
            'to'              => Yii::t('app', 'To'),
            'message'         => Yii::t('app', 'Message'),
            'created'         => Yii::t('app', 'Timestamp'),
            'created_by'      => Yii::t('app', 'User ID'),
            'created_by_name' => Yii::t('app', 'User name'),
            'created_from'    => Yii::t('app', 'User IP'),
        ];
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            [['action', 'created'], 'required'],
            [['created_by'], 'default', 'value' => 1],
            [['created_by_name'], 'default', 'value' => 'Administrator'],
            [['model', 'model_name', 'field', 'action', 'created_from', 'created_by_name'], 'string', 'max' => 255],
            [['created_by', 'model_id'], 'integer'],
            [['from', 'to', 'message', 'created_from', 'model_name', 'created_by_name'], 'safe'],
        ];
    }

    /**
     * Get log message
     * @param integer $id The log entry ID to find in the DB
     * @return static
     */
    public static function getMessage($id)
    {
        try {
            $logEntry = static::findOne([
                'id'     => $id,
                'action' => 'MESSAGE',
            ]);
        } catch (yii\db\Exception $e) {
            return null;
        } catch (yii\base\InvalidConfigException $e) {
            return null;
        }

        return [
            'user'       => $logEntry->created_by,
            'userIP'     => $logEntry->created_from,
            'message'    => $logEntry->message,
            'modelClass' => $logEntry->model ? $logEntry->model : null,
            'modelID'    => $logEntry->model_id ? $logEntry->model_id : null,
            'timestamp'  => $logEntry->created,
        ];
    }

    /**
     * Get all messages
     * @return static
     */
    public static function getAllMessages()
    {
        try {
            return $messages = static::findAll(['action' => 'MESSAGE']);
        } catch (yii\db\Exception $e) {
            return null;
        } catch (yii\base\InvalidConfigException $e) {
            return null;
        }
    }
}
