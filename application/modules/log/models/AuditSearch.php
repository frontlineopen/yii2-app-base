<?php
/**
 *
 * @package     Frontline WebApp Base
 *
 * @subpackage  AuditSearch Model
 *
 * @author      Seabstian Costiug, sebastian@frontline.ro
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @category    models
 * @see         https://www.yiiframework.com/doc/guide/2.0/en/structure-models
 *
 * @since       2020.08.05
 *
 */

namespace app\modules\log\models;

use app\modules\log\models\Audit;
use yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * AuditSearch represents the model behind the search form of `app\modules\log\models\Audit`.
 */
class AuditSearch extends Audit
{
    /**
     * {@inheritdoc}
     * @return array
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['from', 'to', 'action', 'model', 'field', 'created', 'created_by', 'created_by_name', 'created_from', 'model_id', 'model_name', 'message'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     * @return array
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params The search parameters
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Audit::find()->orderBy(['[[id]]' => SORT_DESC]);

        // add conditions that should always apply here
        $pagesize = Yii::$app->user->getPageSize() ? Yii::$app->user->getPageSize() : Yii::$app->settings->get('system', 'defaultPageSize');

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'defaultPageSize' => $pagesize,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'model', $this->model])
            ->andFilterWhere(['like', 'created', $this->created])
            ->andFilterWhere(['like', 'created_from', $this->created_from])
            ->andFilterWhere(['like', 'created_by_name', $this->created_by_name])
            ->andFilterWhere(['like', 'message', $this->message]);

        return $dataProvider;
    }
}
