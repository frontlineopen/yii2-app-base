<?php
/**
 *
 * @package     Frontline WebApp Base
 *
 * @subpackage  <Audit Component>
 *
 * @author      Sam Millman (Sammaye) modified by Sebastian Costiug, sebastian@frontline.ro
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @category    components
 * @see         https://github.com/Sammaye/yii2-audittrail
 *
 * @since       2020.08.04
 *
 */

namespace app\modules\log\components;

use app\modules\log\helpers\AuditHelper;
use app\modules\log\models\Audit;
use Exception;
use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;

/**
 * AuditBehavior class
 */
class AuditBehavior extends Behavior
{
    /** Actions */
    const ACTION_DELETE  = 'DELETE';
    const ACTION_CREATE  = 'CREATE';
    const ACTION_SET     = 'SET';
    const ACTION_CHANGE  = 'CHANGE';
    const ACTION_MESSAGE = 'MESSAGE';

    /** Class parameters */
    private $_oldattributes = [];
    public $allowed         = [];
    public $ignored         = [];
    public $ignoredClasses  = [];
    public $dateFormat      = 'Y-m-d H:i:s';
    public $userAttribute   = null;
    public $storeTimestamp  = false;
    public $skipNulls       = true;
    public $active          = true;
    public $fqnPrefix       = null;
    public static $actions  = [
        'DELETE'  => 'Delete',
        'CREATE'  => 'Add',
        'SET'     => 'Set',
        'CHANGE'  => 'Update',
        'MESSAGE' => 'Message',
    ];

    /**
     * {@inheritdoc}
     * @return array
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_FIND   => 'afterFind',
            ActiveRecord::EVENT_AFTER_INSERT => 'afterInsert',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterUpdate',
            ActiveRecord::EVENT_AFTER_DELETE => 'afterDelete',
        ];
    }

    /**
     * {@inheritdoc}
     * @param object $event The Event to be logged.
     * @return void
     */
    public function afterDelete($event)
    {
        $this->leaveTrail(self::ACTION_DELETE);
    }

    /**
     * {@inheritdoc}
     * @param object $event The Event to be logged.
     * @return void
     */
    public function afterFind($event)
    {
        $this->setOldAttributes($this->owner->getAttributes());
    }

    /**
     * {@inheritdoc}
     * @param object $event The Event to be logged.
     * @return void
     */
    public function afterInsert($event)
    {
        $this->audit(true);
    }

    /**
     * {@inheritdoc}
     * @param object $event The Event to be logged
     * @return void
     */
    public function afterUpdate($event)
    {
        $this->audit(false);
    }

    /**
     * Audit function
     * @param boolean $insert Parameter used to decide if the audited model
     * is a new record or an update of an already existing one
     * @return mixed
     */
    public function audit($insert)
    {
        $allowedFields  = $this->allowed;
        $ignoredFields  = $this->ignored;
        $ignoredClasses = $this->ignoredClasses;

        $newattributes = $this->owner->getAttributes();
        $oldattributes = $this->getOldAttributes();

        // Lets check if the whole class should be ignored
        if (sizeof($ignoredClasses) > 0) {
            if (array_search(get_class($this->owner), $ignoredClasses) !== false) {
                return;
            }
        }

        // Lets unset fields which are not allowed
        if (sizeof($allowedFields) > 0) {
            foreach ($newattributes as $f => $v) {
                if (array_search($f, $allowedFields) === false) {
                    unset($newattributes[$f]);
                }
            }

            foreach ($oldattributes as $f => $v) {
                if (array_search($f, $allowedFields) === false) {
                    unset($oldattributes[$f]);
                }
            }
        }

        // Lets unset fields which are ignored
        if (sizeof($ignoredFields) > 0) {
            foreach ($newattributes as $f => $v) {
                if (array_search($f, $ignoredFields) !== false) {
                    unset($newattributes[$f]);
                }
            }

            foreach ($oldattributes as $f => $v) {
                if (array_search($f, $ignoredFields) !== false) {
                    unset($oldattributes[$f]);
                }
            }
        }

        // If no difference then WHY?
        // There is some kind of problem here that means "0" and 1 do not diff for array_diff so beware: stackoverflow.com/questions/12004231/php-array-diff-weirdness :S
        if (count(array_diff_assoc($newattributes, $oldattributes)) <= 0) {
            return;
        }

        // If this is a new record lets add a CREATE notification
        if ($insert) {
            $this->leaveTrail(self::ACTION_CREATE);
        }

        // Now lets actually write the attributes
        $this->auditAttributes($insert, $newattributes, $oldattributes);

        // Reset old attributes to handle the case with the same model instance updated multiple times
        $this->setOldAttributes($this->owner->getAttributes());
    }

    /**
     * Audit attributes
     * @param boolean $insert        Decides if the attribute to be saved to db is for a new model and is
     * a new attribute that is being set or is for an old model and neeeds to be changed to the new attribute
     * @param array   $newattributes The array of new attributes that will be saved to the db
     * @param array   $oldattributes The array of the already existing attributes that will be changed to the new ones
     * @return void
     */
    public function auditAttributes($insert, $newattributes, $oldattributes = [])
    {
        foreach ($newattributes as $name => $value) {
            $old = isset($oldattributes[$name]) ? $oldattributes[$name] : '';

            // If we are skipping nulls then lets see if both sides are null
            if ($this->skipNulls && empty($old) && empty($value)) {
                continue;
            }

            // If they are not the same lets write an audit log
            if ($value != $old) {
                $this->leaveTrail($insert ? self::ACTION_SET : self::ACTION_CHANGE, $name, $value, $old);
            }
        }
    }

    /**
     * Saves a log entry into the database
     * @param string $action    The type of action that will be saved in the db for this entry
     * @param string $field     The model field that was affected in the change
     * @param string $value     The new value that is to be saved
     * @param string $old_value The old value that was changed
     * @return boolean
     */
    public function leaveTrail($action, $field = null, $value = null, $old_value = null)
    {
        if ($this->active) {
            $auditEntry     = new Audit();
            $className      = $this->owner->className();
            $classNameParts = explode('\\', $className);

            if ($this->fqnPrefix) {
                $auditEntry->model = end($classNameParts);
            } else {
                $auditEntry->model = $className;
            }

            $auditEntry->from            = $old_value;
            $auditEntry->to              = $value;
            $auditEntry->action          = $action;
            $auditEntry->model_id        = $this->owner->getPrimaryKey();
            $auditEntry->model_name      = $this->owner->getName();
            $auditEntry->field           = $field;
            $auditEntry->created         = $this->storeTimestamp ? time() : date($this->dateFormat);
            $auditEntry->created_by      = $this->getUserId();
            $auditEntry->created_from    = Yii::$app->request->getUserIP();
            $auditEntry->created_by_name = Yii::$app->user->identity->name;
            $auditEntry->message         = AuditHelper::formatMessage($action, end($classNameParts), $auditEntry->model_name, $field, $value, $old_value, $auditEntry->created_by);

            return $auditEntry->save();
        } else {
            return true;
        }
    }

    /**
     * Gets the $_oldattributes array for the current audit model
     * @return array
     */
    public function getOldAttributes()
    {
        return $this->_oldattributes;
    }

    /**
     * Sets the $_oldattributes array for the current audit model
     * @param string $value The old value to be saved
     * @return void
     */
    public function setOldAttributes($value)
    {
        $this->_oldattributes = $value;
    }

    /**
     * Gets the ID for the user that created the current Audit change
     * @return mixed
     */
    public function getUserId()
    {
        if (isset($this->userAttribute)) {
            $data = $this->owner->getAttributes();

            return isset($data[$this->userAttribute]) ? $data[$this->userAttribute] : null;
        } else {
            try {
                $userid = Yii::$app->user->id;

                return empty($userid) ? null : $userid;
            } catch (Exception $e) {
                //If we have no user object, this must be a command line program
                return null;
            }
        }
    }

    /**
     * Saves a log entry into the database
     * @param string $message Message to be saved in the db
     * @param string $model   Model to which the message saved reffers to
     * @return boolean
     */
    public static function logMessage($message, $model = null)
    {
        $auditEntry = new Audit();

        $auditEntry->created_by      = Yii::$app->user->id;
        $auditEntry->created_by_name = strval(Yii::$app->user->identity->name);
        $auditEntry->created_from    = Yii::$app->request->getUserIP();
        $auditEntry->created         = date('Y-m-d H:i:s');
        $auditEntry->action          = self::ACTION_MESSAGE;
        if ($model) {
            $className              = get_class($model);
            $classNameParts         = explode('\\', $className);
            $auditEntry->model_id   = $model->id;
            $auditEntry->model_name = $model->name;
            $auditEntry->model      = $className;
        }
        if ($message === 'Logged in' || $message === 'Logged out') {
            $auditEntry->message = $message;
        } else {
            $auditEntry->message = $message . ($model ? (' ' . end($classNameParts) . ' <code>' . $model->name) . '</code>' : '');
        }
        return $auditEntry->save();
    }

    /**
     * Saves a log entry into the database
     * @param string $model Model to which the message saved reffers to
     */
    public static function logModelView($model)
    {
        self::logMessage(Yii::t('app', 'Viewed entry'), $model);
    }

    /**
     * Get recent entries
     * @return mixed
     */
    public static function recentEntries()
    {
        return Audit::find()->orderBy(['[[created]]' => SORT_DESC]);
    }
}
