<?php
/**
 * @package     Frontline WebApp Base
 *
 * @subpackage  <Log Module Class file>
 *
 * @author      Sebastian Costiug <sebastian@frontline.ro>
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @category    modules
 * @see         https://www.yiiframework.com/doc/guide/2.0/en/structure-modules
 *
 * @since       2020.08.05
 *
 */

namespace app\modules\log;

/**
 * User Module class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
    }
}
