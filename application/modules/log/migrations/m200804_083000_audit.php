<?php
/**
 *
 * @package     Frontline WebApp Base
 *
 * @subpackage  <Audit Database Migration>
 *
 * @author      Sam Millman (Sammaye) modified by Sebastian Costiug, sebastian@frontline.ro
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @category    migrations
 * @see         https://github.com/Sammaye/yii2-audittrail
 *
 * @since       2020.08.04
 *
 */

use yii\db\Migration;

/**
 * Handles the creation of table `{{%audit}}`.
 */
class m200804_083000_audit extends Migration
{
    /**
     * {@inheritdoc}
     * @return void
     */
    public function safeUp()
    {
        // Creates table: audit
        $this->createTable('{{%audit}}', [
            'id'              => $this->primaryKey(10)->unsigned(),
            'model'           => $this->string(),
            'model_id'        => $this->integer(10)->unsigned(),
            'model_name'      => $this->string(),
            'field'           => $this->string(),
            'action'          => $this->string()->notNull(),
            'from'            => $this->string(),
            'to'              => $this->string(),
            'message'         => $this->string(),
            'created'         => $this->timestamp()->notNull(),
            'created_by'      => $this->integer(10)->unsigned()->notNull()->defaultValue(1),
            'created_by_name' => $this->string()->defaultValue('Administrator'),
            'created_from'    => $this->string(),
        ]);

        // Column Names
        $this->addCommentOnColumn('{{%audit}}', 'id', 'ID');
        $this->addCommentOnColumn('{{%audit}}', 'model', 'Module');
        $this->addCommentOnColumn('{{%audit}}', 'model_id', 'Model ID');
        $this->addCommentOnColumn('{{%audit}}', 'model_name', 'Model name');
        $this->addCommentOnColumn('{{%audit}}', 'field', 'Model field');
        $this->addCommentOnColumn('{{%audit}}', 'action', 'Action');
        $this->addCommentOnColumn('{{%audit}}', 'from', 'From');
        $this->addCommentOnColumn('{{%audit}}', 'to', 'To');
        $this->addCommentOnColumn('{{%audit}}', 'message', 'Message');
        $this->addCommentOnColumn('{{%audit}}', 'created', 'Timestamp');
        $this->addCommentOnColumn('{{%audit}}', 'created_by', 'User ID');
        $this->addCommentOnColumn('{{%audit}}', 'created_by_name', 'User name');
        $this->addCommentOnColumn('{{%audit}}', 'created_from', 'User IP');

        // Indexes for speedy lookups
        $this->createIndex('idx_audit_model', '{{%audit}}', 'model');
        $this->createIndex('idx_audit_model_name', '{{%audit}}', 'model_name');
        $this->createIndex('idx_audit_model_id', '{{%audit}}', 'model_id');
        $this->createIndex('idx_audit_field', '{{%audit}}', 'field');
        $this->createIndex('idx_audit_action', '{{%audit}}', 'action');

        // User ID index - foreign key from the user table
        $this->addForeignKey(
            'fk_created_by',
            '{{%audit}}',
            'created_by',
            '{{%user}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     * @return void
     */
    public function safeDown()
    {
        $this->dropTable('{{%audit}}');
    }
}
