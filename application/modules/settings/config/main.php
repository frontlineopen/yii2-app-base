<?php
/**
 *
 * @package     Frontline WebApp Base
 *
 * @subpackage  <Settings Module config segment>
 *
 * @author      Sebastian Costiug <sebastian@frontline.ro>
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @category    config
 * @see         https://www.yiiframework.com/doc/guide/2.0/en/concept-configurations
 *
 * @since       2020.09.09
 *
 */

return [
    'bootstrap'  => [
        'settings',
    ],
    // Module
    'modules'    => [
        'settings' => [
            'class' => 'app\modules\settings\Module',
        ],
    ],
    // Components
    'components' => [
        // URL Manager
        'urlManager' => [
            'rules' => [
                // Actions
                'settings'        => '/settings/settings/index',
            ],
        ],

        // Settings
        'settings'   => [
            'class'     => 'app\modules\settings\components\Settings',
            'modelsMap' => [
                'settings' => 'app\modules\settings\models\SettingsForm',
            ],
        ],
    ],
];
