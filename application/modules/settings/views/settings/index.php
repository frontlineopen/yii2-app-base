<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use app\modules\settings\models\SettingsForm;
use yii\bootstrap4\Tabs;

$this->title = Yii::t('app', 'Settings');
?>
<div class="m-auto col-xl-5 col-lg-8">
    <div class="card">
    <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL, 'formConfig' => ['labelSpan' => 4]]); ?>
        <div class="card-body">
            <div class="col-12 m-auto">
                <div class="nav-tabs-boxed">
                    <?php $tabSite       = $form->field($model, 'site_name')->textInput(); ?>
                    <?php $tabSite      .= $form->field($model, 'site_headline')->textInput(); ?>
                    <?php $tabSite      .= $form->field($model, 'site_copyright')->textInput(); ?>
                    <?php $tabCompany    = $form->field($model, 'company_name')->textInput(); ?>
                    <?php $tabCompany   .= $form->field($model, 'company_address')->textInput(); ?>
                    <?php $tabCompany   .= $form->field($model, 'company_addressCity')->textInput(); ?>
                    <?php $tabCompany   .= $form->field($model, 'company_addressRegion')->textInput(); ?>
                    <?php $tabCompany   .= $form->field($model, 'company_addressCountry')->textInput(); ?>
                    <?php $tabCompany   .= $form->field($model, 'company_addressZip')->textInput(); ?>
                    <?php $tabCompany   .= $form->field($model, 'company_phone')->textInput(); ?>
                    <?php $tabCompany   .= $form->field($model, 'company_fax')->textInput(); ?>
                    <?php $tabCompany   .= $form->field($model, 'company_email')->textInput(); ?>
                    <?php $tabCompany   .= $form->field($model, 'company_registrationId')->textInput(); ?>
                    <?php $tabCompany   .= $form->field($model, 'company_taxId')->textInput(); ?>
                    <?php $tabSocial     = $form->field($model, 'social_linkedin')->textInput(); ?>
                    <?php $tabSocial    .= $form->field($model, 'social_facebook')->textInput(); ?>
                    <?php $tabSocial    .= $form->field($model, 'social_twitter')->textInput(); ?>
                    <?php $tabSocial    .= $form->field($model, 'social_google')->textInput(); ?>
                    <?php $tabMail       = $form->field($model, 'mail_fromName')->textInput(); ?>
                    <?php $tabMail      .= $form->field($model, 'mail_fromEmail')->textInput(); ?>
                    <?php $tabMail      .= $form->field($model, 'mail_replyTo')->textInput(); ?>
                    <?php $tabMail      .= $form->field($model, 'mail_toName')->textInput(); ?>
                    <?php $tabMail      .= $form->field($model, 'mail_toEmail')->textInput(); ?>
                    <?php $tabPagination = $form->field($model, 'pagination_allowedSize')->widget(Select2::classname(), [
                        'data'       => $model->getAllowedPagination(),
                        'hideSearch' => true,
                        'options'    => [
                            'multiple' => true,
                            'value'    => $model->getAllowedPagination(),
                        ],
                        'pluginOptions'  => [
                            'allowClear' => true,
                            'tags'       => true,
                            ],
                    ]); ?>

                    <?php $tabPagination .= $form->field($model, 'pagination_defaultSize')->widget(Select2::classname(), [
                        'data'       => $model->getAllowedPagination(),
                        'hideSearch' => true,
                    ]); ?>

                    <?php $tabSystem  = $form->field($model, 'system_defaultLanguage')->widget(Select2::classname(), [
                        'data'       => $model->getAllowedLanguages(),
                        'hideSearch' => true,
                    ]); ?>
                </div>

                <?php echo Tabs::widget([
                    'tabContentOptions' => ['class' => 'mt-4'],
                    'items' => [
                        [
                            'label' => Yii::t('app', 'Site'),
                            'content' => $tabSite,
                            'active' => true,
                        ],
                        [
                            'label' => Yii::t('app', 'Company'),
                            'content' => $tabCompany,
                        ],
                        [
                            'label' => Yii::t('app', 'Social'),
                            'content' => $tabSocial,
                        ],
                        [
                            'label' => Yii::t('app', 'Mail'),
                            'content' => $tabMail,
                        ],
                        [
                            'label' => Yii::t('app', 'Pagination'),
                            'content' => $tabPagination,
                        ],
                        [
                            'label' => Yii::t('app', 'System'),
                            'content' => $tabSystem,
                        ],
                    ],
                ]); ?>
            </div>
        </div>
        <div class="card-footer">
            <div class="form-actions text-right">
                <?= Html::a(Yii::t('app', 'Back'), !empty(Yii::$app->request->referrer) ? Yii::$app->request->referrer : Url::to(['/site/index']), ['class' => 'btn btn-info float-left']) ?>
                <?= Html::submitButton(Yii::t('app', 'Save and continue'), ['class' => 'btn btn-secondary d-none d-sm-inline-block btn-save-and-continue']) ?>
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
