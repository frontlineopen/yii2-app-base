<?php
/**
 * @package     Frontline WebApp Base
 *
 * @subpackage  <Application settings Controller>
 *
 * @author      Sebastian Costiug <sebastian@frontline.ro>
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @category    controllers
 * @see         http://www.yiiframework.com/doc-2.0/guide-structure-controllers.html
 *
 * @since       2020.06.15
 *
 */

namespace app\modules\settings\controllers;

use app\components\BaseController;
use app\modules\settings\models\SettingsForm;
use app\modules\user\components\AccessRule;
use app\modules\user\models\User;
use yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\BaseJson;

/**
 * Class SettingsController
 */
class SettingsController extends BaseController
{
    /**
     * RBAC behaviours
     * @return mixed
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class'      => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'only'       => ['index'],
                'rules'      => [
                    [
                        'actions' => ['index'],
                        'allow'   => true,
                        'roles'   => [
                            User::ROLE_ADMIN,
                        ],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'index' => ['GET', 'POST'],
                ],
            ],
        ];
    }

    /**
     * Application settings.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new SettingsForm();
        $model->loadData();

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post())) {
                if (Yii::$app->request->isAjax) {
                    $result = [];

                    if ($model->validate()) {
                        try {
                            if ($model->save()) {
                                $result['status']  = 'succes';
                                $result['message'] = Yii::t('app', 'Settings succesfully updated');
                            } else {
                                $result['status']  = 'error';
                                $result['message'] = Yii::t('app', 'Settings did not save');
                            }
                        } catch (yii\db\CDbException $e) {
                            $result['status']  = 'error';
                            $result['message'] = Yii::t('app', $e->errorInfo);
                        } catch (yii\db\IntegrityException $e) {
                            $result['status']  = 'error';
                            $result['message'] = Yii::t('app', $e->errorInfo);
                        }
                    } else {
                        $result            = [];
                        $result['status']  = 'error';
                        $result['message'] = Yii::t('app', 'Please check input data!');
                        foreach ($model->getErrors() as $field => $error) {
                            $result['errors'][$field] = $error;
                        }
                    }

                    return BaseJson::encode($result);
                } else {
                    if ($model->validate()) {
                        try {
                            if ($model->save()) {
                                Yii::$app->session->setFlash('success', Yii::t('app', 'Settings succesfully updated'));

                                return $this->redirect(['/site/index']);
                            } else {
                                Yii::$app->session->setFlash('error', Yii::t('app', 'Settings did not save'));
                            }
                        } catch (yii\db\CDbException $e) {
                            Yii::$app->session->setFlash('error', Yii::t('app', $e->errorInfo));
                        } catch (yii\db\IntegrityException $e) {
                            Yii::$app->session->setFlash('error', Yii::t('app', $e->errorInfo));
                        }
                    } else {
                        Yii::$app->session->setFlash('error', Yii::t('app', 'Please check input data'));
                    }
                }
            } else {
                throw new \yii\web\HttpException(204, Yii::t('app', 'Invalid request.'));
            }
        }

        return $this->render('index', [
            'model' => $model,
        ]);
    }
}
