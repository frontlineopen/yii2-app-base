<?php
/**
 *
 * @package     Frontline WebApp Base
 *
 * @subpackage  <Application settings component>
 *
 * @author      Sebastian Costiug <sebastian@frontline.ro>
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @category    components
 * @see         https://www.yiiframework.com/wiki/760/yii-2-0-write-use-a-custom-component-in-yii2-0-advanced-template
 *
 * @since       2020.07.08
 *
 */

namespace app\modules\settings\components;

use yii;
use yii\base\BootstrapInterface;
use yii\base\Component;
use yii\base\Model;

/**
 * Class Settings
 */
class Settings extends Component implements BootstrapInterface
{
    /**
     * Models, that can be used to get/set settings values as object properties
     * @var array
     */
    public $modelsMap = [];

    /**
     * Array of Model objects, which allows to get/set settings params as properties
     * according to config settings_model_name
     * @var Model[]
     */
    public $models;

    /**
     * @var null|array|false the functions used to serialize and unserialize cached data. Defaults to null, meaning
     * using the default PHP `serialize()` and `unserialize()` functions.
     * If this property is set false, data will be directly sent to and retrieved from the underlying
     * cache component without any serialization or deserialization.
     */
    public $serializer;

    /**
     * Table name, where settings are stored.
     * Name is defined according to the extension migration
     * @var string
     */
    public $tableName = '{{%settings}}';

    /**
     * Default values
     * @var array
     */
    protected static $defaults = [
        'site_name'              => 'Frontline App',
        'site_copyright'         => 'All rights reserved',
        'mail_fromName'          => 'App Notifications',
        'mail_fromEmail'         => 'app@app.com',
        'mail_toName'            => 'Site Owner',
        'mail_toEmail'           => 'owner@app.com',
        'pagination_allowedSize' => '5,10,15,20,25,50,100',
        'pagination_defaultSize' => '25',
        'system_defaultLanguage' => 'en',
    ];

    /**
     * Bootstrap method to be called during application bootstrap stage.
     * @param mixed $app The application currently running
     * @return void
     */
    public function bootstrap($app)
    {
        $container = \Yii::$container;
        $container->setSingleton(Settings::class, $this);
    }

    /**
     * {@inheritdoc}
     * @return void
     */
    public function init()
    {
        foreach ($this->modelsMap as $name => $params) {
            $this->models[$name] = \Yii::createObject($params);
        }
    }

    /**
     * Checks, if there is a model with name $name. Returns model with loaded data
     * @param string $name The setting name to get
     * @return Model
     * @throws \yii\base\Exception If the name is not found
     */
    public function __get($name)
    {
        if (!isset($this->models[$name])) {
            throw new \yii\base\Exception("Settings name {$name} missing");
        }

        return $this->models[$name]->loadData();
    }

    /**
     * Returns serialized/unserialized data according to component settings
     * @param string $category The setting category
     * @param string $name     The seting name
     * @return mixed
     */
    public function get($category, $name)
    {
        $value = false;

        if (Yii::$app->cache->get('settingsLoaded')) {
            if (isset(Yii::$app->cache['settings'][$category][$name])) {
                $value = Yii::$app->cache['settings'][$category][$name];
            }
        } else {
            if ($this->exists($category, $name)) {
                $value = $this->getValue($category, $name);
                if ($this->serializer === null) {
                    $value = unserialize($value);
                } elseif ($this->serializer !== false) {
                    $value = call_user_func($this->serializer[1], $value);
                }
            }
        }

        if ($value === false) {
            if (isset(self::$defaults[$category . '_' . $name])) {
                $value = self::$defaults[$category . '_' . $name];
            }
        }

        return $value;
    }

    /**
     * Serialize data according components settings and set it
     * @param string $category The setting category
     * @param string $name     The seting name
     * @param mixed  $value    The seting value to save
     * @return boolean
     */
    public function set($category, $name, $value)
    {
        $this->setCache($category, $name, $value);

        if ($this->serializer === null) {
            $value = serialize($value);
        } elseif ($this->serializer !== false) {
            $value = call_user_func($this->serializer[0], $value);
        }

        return $this->setValue($category, $name, $value);
    }

    /**
     * Serialize data according components settings and set it
     * @param string $category The setting category
     * @param string $name     The seting name
     * @param mixed  $value    The seting value to save
     * @return void
     */
    public function setCache($category, $name, $value)
    {
        $data                   = Yii::$app->cache->get('settings');
        $data[$category][$name] = $value;
        Yii::$app->cache->set('settings', $data);
    }

    /**
     * Check if there is a value for $category-$name pair
     * @param string $category The setting category
     * @param string $name     The seting name
     * @return boolean
     */
    protected function exists($category, $name)
    {
        return false !== $this->getValue($category, $name);
    }

    /**
     * Insert new settings data to a db
     * @param string $category The setting category
     * @param string $name     The seting name
     * @param mixed  $value    The seting value to save
     * @return boolean
     */
    protected function insertValue($category, $name, $value)
    {
        try {
            $db = \Yii::$app->db;
            $db->createCommand()
                ->insert($this->tableName, [
                    'category' => $category,
                    'name'     => $name,
                    'value'    => [$value, \PDO::PARAM_LOB],
                ])->execute();

            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Update existed in a db settings data
     * @param string $category The setting category
     * @param string $name     The seting name
     * @param mixed  $value    The seting value to save
     * @return boolean
     */
    protected function updateValue($category, $name, $value)
    {
        try {
            $db = \Yii::$app->db;
            $db->createCommand()
                ->update($this->tableName, [
                    'value' => [$value, \PDO::PARAM_LOB],
                ], [
                    'category' => $category,
                    'name'     => $name,
                ])
                ->execute();

            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Get value from a db. if not exists, return false
     * @param string $category The setting category
     * @param string $name     The seting name
     * @return boolean
     */
    protected function getValue($category, $name)
    {
        try {
            $value = Yii::$app->db
                ->createCommand('SELECT `category`, `name`, `value` FROM' . $this->tableName . ' WHERE category = :cat AND name = :name')
                ->bindValue(':cat', $category)->bindValue(':name', $name)
                ->queryOne();

            $dbValue = $this->serializer ? call_user_func($this->serializer[1], $value['value']) : unserialize($value['value']);
            $this->setCache($category, $name, $dbValue);

            return isset($value['value']) ? $value['value'] : false;
        } catch (\yii\db\Exception $e) {
            return false;
        }
    }

    /**
     * Insert value to a db if it's not exist. Otherwise - update existed
     * @param string $category The setting category
     * @param string $name     The seting name
     * @param mixed  $value    The seting value to save
     * @return boolean
     */
    protected function setValue($category, $name, $value)
    {
        if ($this->exists($category, $name)) {
            return $this->updateValue($category, $name, $value);
        } else {
            return $this->insertValue($category, $name, $value);
        }
    }

    /** Gets all possible languages for the application
     * @return array
     */
    public function getAllowedLanguages()
    {
        if (isset(Yii::$app->params['allowedLanguages'])) {
            $result = Yii::$app->params['allowedLanguages'];
        } else {
            $result = ['en' => 'English'];
        }
        asort($result);

        return $result;
    }

    /** Gets all possible languages for the application
     * @return array
     */
    public function getAllowedPagination()
    {
        $result = [];
        $values = explode(',', ($this->get('pagination', 'allowedSize')) ? ($this->get('pagination', 'allowedSize')) : (self::$defaults['pagination_allowedSize']));
        foreach ($values as $value) {
            $result[$value] = $value;
        }
        asort($result);

        return $result;
    }

    /** Gets the default settings values
     * @return array
     */
    public static function getDefaults()
    {
        return self::$defaults;
    }
}
