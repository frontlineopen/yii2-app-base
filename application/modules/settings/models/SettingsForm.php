<?php
/**
 * @package     Frontline WebApp Base
 *
 * @subpackage  <Settings form model>
 *
 * @author      Sebastian Costiug <sebastian@frontline.ro>
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @category    models
 * @see         http://www.yiiframework.com/doc-2.0/guide-structure-models.html
 *
 * @since       2020.06.15
 *
 */

namespace app\modules\settings\models;

use app\modules\settings\components\Settings;
use yii;
use yii\base\Model;

/**
 * This is the model class for table "settings".
 */
class SettingsForm extends Model
{
    /** Application settings categories: */

    /** Application */
    public $site_name;
    public $site_headline;
    public $site_copyright;
    public $site_copyrightYearFrom;

    /** Company */
    public $company_name;
    public $company_address;
    public $company_addressCity;
    public $company_addressRegion;
    public $company_addressCountry;
    public $company_addressZip;
    public $company_phone;
    public $company_fax;
    public $company_email;
    public $company_registrationId;
    public $company_taxId;

    /** Social */
    public $social_linkedin;
    public $social_facebook;
    public $social_twitter;
    public $social_google;

    /** Pagination */
    public $pagination_defaultSize;
    public $pagination_allowedSize;

    /** Mailing */
    public $mail_fromName;
    public $mail_fromEmail;
    public $mail_replyTo;
    public $mail_toName;
    public $mail_toEmail;

    /** Languages */
    public $system_defaultLanguage;

    /**Application installation status */
    public $system_installed;

    /**
     * Set Default values
     * @return void
     */
    public function init()
    {
        foreach ($this->attributes as $key => $value) {
            if (isset(Settings::getDefaults()[$key])) {
                $this->$key = Settings::getDefaults()[$key];
            }
        }
        $this->site_copyrightYearFrom = date('Y');

        parent::init();
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function rules()
    {
        return [
            // Required
            [['site_name', 'site_copyright'], 'required'],
            [['mail_fromEmail', 'mail_fromName', 'mail_toName', 'mail_toEmail'], 'required'],
            [['pagination_defaultSize', 'system_defaultLanguage'], 'required'],
            // Sizes
            [['site_name', 'site_copyright', 'mail_fromName'], 'string', 'max' => 64],
            [['mail_fromEmail', 'mail_replyTo', 'mail_toEmail'], 'string', 'max' => 128],
            // EMails
            [['mail_fromEmail', 'mail_replyTo', 'mail_toEmail'], 'email'],
            // Filter - convert Email to lowercase
            [['mail_fromEmail', 'mail_replyTo', 'mail_toEmail'], 'filter', 'filter' => 'strtolower'],
            // Defaults
            ['system_defaultLanguage', 'default', 'value' => 'English'],
            ['pagination_defaultSize', 'default', 'value' => '25'],
            // All other fields are safe
            [['site_headline', 'site_copyrightYearFrom'], 'safe'],
            [['company_name', 'company_address', 'company_addressCity', 'company_addressRegion', 'company_addressCountry', 'company_addressZip', 'company_phone', 'company_fax', 'company_email', 'company_registrationId', 'company_taxId'], 'safe'],
            [['social_linkedin', 'social_facebook', 'social_twitter', 'social_google'], 'safe'],
            [['mail_replyTo', 'pagination_allowedSize', 'system_installed'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels()
    {
        return [
            //Application
            'site_name'              => Yii::t('app', 'Application name'),
            'site_headline'          => Yii::t('app', 'Headline'),
            'site_copyright'         => Yii::t('app', 'Copyright'),
            'site_copyrightYearFrom' => Yii::t('app', 'Since'),
            //Company
            'company_name'           => Yii::t('app', 'Company name'),
            'company_address'        => Yii::t('app', 'Address'),
            'company_addressCity'    => Yii::t('app', 'City'),
            'company_addressRegion'  => Yii::t('app', 'Region'),
            'company_addressCountry' => Yii::t('app', 'Country'),
            'company_addressZip'     => Yii::t('app', 'Zip Code'),
            'company_phone'          => Yii::t('app', 'Phone'),
            'company_fax'            => Yii::t('app', 'Fax'),
            'company_email'          => Yii::t('app', 'Email'),
            'company_registrationId' => Yii::t('app', 'Business Registration ID'),
            'company_taxId'          => Yii::t('app', 'Tax Identification ID'),
            //Social
            'social_linkedin'        => Yii::t('app', 'LinkedIn'),
            'social_facebook'        => Yii::t('app', 'Facebook'),
            'social_twitter'         => Yii::t('app', 'Twitter'),
            'social_google'          => Yii::t('app', 'Google+'),
            //Mails
            'mail_fromName'          => Yii::t('app', 'From Name'),
            'mail_fromEmail'         => Yii::t('app', 'From Email'),
            'mail_replyTo'           => Yii::t('app', 'Reply To Email'),
            'mail_toName'            => Yii::t('app', 'To Name'),
            'mail_toEmail'           => Yii::t('app', 'To Email'),
            //Pagination
            'pagination_defaultSize' => Yii::t('app', 'Pagination value'),
            'pagination_allowedSize' => Yii::t('app', 'Allowed pagination values'),
            //Language
            'system_defaultLanguage' => Yii::t('app', 'Default language'),
            'system_installed'       => Yii::t('app', 'Installed'),
        ];
    }

    /**
     * Method to load settings properties from storage to model
     * @param string $category The category of settings to load
     * If no category is chosen it loads all settings
     * @return $this
     */
    public function loadData($category = null)
    {
        foreach ($this->attributes as $key => $value) {
            $tmp        = explode('_', $key);
            $cat        = $tmp[0];
            $name       = $tmp[1];
            $this->$key = $category ? Yii::$app->settings->get($category, $name) : Yii::$app->settings->get($cat, $name);
            if ($this->$key == '') {
                if (isset(Settings::getDefaults()[$key])) {
                    $this->$key = Settings::getDefaults()[$key];
                }
            }
        }
        Yii::$app->cache->set('settingsLoaded', true);

        return $this;
    }

    /**
     * Method to save settings to configured storage
     * @return boolean
     */
    public function save()
    {
        $save = true;
        foreach ($this->getAttributes() as $name => $value) {
            $tmp = explode('_', $name);
            $cat = $tmp[0];
            $key = $tmp[1];
            if (is_array($value)) {
                asort($value);
                $value = implode(',', $value);
            }
            $save &= Yii::$app->settings->set($cat, $key, $value);
        }
        if (!Yii::$app->settings->get('system', 'installed')) {
            Yii::$app->settings->set('system', 'installed', true);
        }
        Yii::$app->cache->set('settingsLoaded', false);

        return $save;
    }

    /** Gets all possible languages for the application
     * @return array
     */
    public function getAllowedLanguages()
    {
        $result = Yii::$app->params['allowedLanguages'];
        asort($result);
        return $result;
    }

    /** Gets all possible languages for the application
     * @return array
     */
    public function getAllowedPagination()
    {
        $result = [];
        $values = explode(',', (Yii::$app->settings->get('pagination', 'allowedSize')) ? (Yii::$app->settings->get('pagination', 'allowedSize')) : ($this->pagination_allowedSize));
        foreach ($values as $value) {
            $result[$value] = $value;
        }
        asort($result);

        return $result;
    }

    /**
     * Returns the default pagination value that will be used as default for all users
     * @param integer $pagination The pagination value index in the pagination_allowedSize array
     * @return integer
     */
    public function getPagination($pagination)
    {
        $paginations = $this->getAllowedPagination();
        return !empty($paginations[$pagination]) ? $paginations[$pagination] : null;
    }
}
