<?php
/**
 *
 * @package     Frontline WebApp Base
 *
 * @subpackage  <Application settings database migration>
 *
 * @author      Sebastian Costiug <sebastian@frontline.ro>
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @category    controllers
 * @see         https://www.yiiframework.com/doc/guide/2.0/en/db-migrations
 *
 * @since       2020.07.07
 *
 */

use yii\db\Migration;

/**
 * Handles the creation of table `{{%settings}}`.
 */
class m200707_075002_create_settings_table extends Migration
{
/**
     * @inheritdoc
     * @return void
     */
    public function up()
    {
        // Tables
        $this->createTable('{{%settings}}', [
            'category' => $this->string()->notNull(),
            'name'     => $this->string()->notNull(),
            'value'    => $this->binary(),
        ]);

        $this->addPrimaryKey('settings_pk', 'settings', ['category', 'name']);
    }

    /**
     * @inheritdoc
     * @return void
     */
    public function down()
    {
        $this->dropTable('{{%settings}}');
    }
}
