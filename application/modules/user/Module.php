<?php
/**
 * @package     Frontline WebApp Base
 *
 * @subpackage  <User Module Class file>
 *
 * @author      Sebastian Costiug <sebastian@frontline.ro>
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @category    modules
 * @see         https://www.yiiframework.com/doc/guide/2.0/en/structure-modules
 *
 * @since       2020.06.15
 *
 */

namespace app\modules\user;

/**
 * User Module class
 */
class Module extends \yii\base\Module
{
    /**
     * @var boolean $selfReg
     * Determines whether or not self Registration is allowed
     */
    public $selfReg = true;

    /**
     * @var integer $selfReg
     * user password token TTL
     */
    public $userPasswordResetTokenExpire = 3600;

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
    }
}
