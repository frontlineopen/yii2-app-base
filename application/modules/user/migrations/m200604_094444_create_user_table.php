<?php
/**
 *
 * @package     Frontline WebApp Base
 *
 * @subpackage  <User Database Migration>
 *
 * @author      Sebastian Costiug <sebastian@frontline.ro>
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @category    migrations
 * @see         https://www.yiiframework.com/doc/guide/2.0/en/db-migrations
 *
 * @since       2020.06.15
 *
 */

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user}}`.
 */
class m200604_094444_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     * @return void
     */
    public function safeUp()
    {
        // Creates table: user
        $this->createTable('{{%user}}', [
            'id'                   => $this->primaryKey(10)->unsigned(),
            'first_name'           => $this->string(50)->notNull(),
            'last_name'            => $this->string(50),
            'email'                => $this->string()->notNull()->unique(),
            'email_notification'   => $this->boolean()->defaultValue(false),
            'phone'                => $this->string(),
            'phone_notification'   => $this->boolean()->defaultValue(false),
            'password_hash'        => $this->string(),
            'password_reset_token' => $this->string(64)->unique(),
            'birthday'             => $this->date(),
            'hide_birthyear'       => $this->boolean()->defaultValue(false),
            'role'                 => $this->integer()->defaultValue(10),
            'department'           => $this->string(),
            'category'             => $this->string()->defaultValue(''),
            'avatar'               => $this->string(),
            'page_size'            => $this->integer(),
            'theme'                => $this->string()->defaultValue('default'),
            'language'             => $this->string()->defaultValue('English'),
            'active'               => $this->boolean()->defaultValue(false),
            'auth_key'             => $this->string(64),
            'recovery_key'         => $this->string(64),
            'register_at'          => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'register_from'        => $this->string(),
            'last_login_at'        => $this->timestamp(),
            'last_login_from'      => $this->string(),
            'current_login_at'     => $this->timestamp(),
            'current_login_from'   => $this->string(),
        ]);

        // Column Names
        $this->addCommentOnColumn('{{%user}}', 'id', 'User ID');
        $this->addCommentOnColumn('{{%user}}', 'first_name', 'First name');
        $this->addCommentOnColumn('{{%user}}', 'last_name', 'Last name');
        $this->addCommentOnColumn('{{%user}}', 'email', 'Email');
        $this->addCommentOnColumn('{{%user}}', 'email_notification', 'Email notification');
        $this->addCommentOnColumn('{{%user}}', 'phone', 'Phone');
        $this->addCommentOnColumn('{{%user}}', 'phone_notification', 'Phone notification');
        $this->addCommentOnColumn('{{%user}}', 'password_hash', 'Password hash');
        $this->addCommentOnColumn('{{%user}}', 'password_reset_token', 'Password reset token');
        $this->addCommentOnColumn('{{%user}}', 'birthday', 'Birthday');
        $this->addCommentOnColumn('{{%user}}', 'role', 'Role');
        $this->addCommentOnColumn('{{%user}}', 'department', 'Department');
        $this->addCommentOnColumn('{{%user}}', 'category', 'Category');
        $this->addCommentOnColumn('{{%user}}', 'avatar', 'Avatar');
        $this->addCommentOnColumn('{{%user}}', 'page_size', 'Page size');
        $this->addCommentOnColumn('{{%user}}', 'hide_birthyear', 'Hide birthyear');
        $this->addCommentOnColumn('{{%user}}', 'theme', 'Theme');
        $this->addCommentOnColumn('{{%user}}', 'language', 'Language');
        $this->addCommentOnColumn('{{%user}}', 'auth_key', 'Authorization key');
        $this->addCommentOnColumn('{{%user}}', 'active', 'Active');
        $this->addCommentOnColumn('{{%user}}', 'recovery_key', 'Recovery key');
        $this->addCommentOnColumn('{{%user}}', 'register_at', 'Register date');
        $this->addCommentOnColumn('{{%user}}', 'register_from', 'Register IP address');
        $this->addCommentOnColumn('{{%user}}', 'last_login_at', 'Last login date');
        $this->addCommentOnColumn('{{%user}}', 'last_login_from', 'Last login IP address');
        $this->addCommentOnColumn('{{%user}}', 'current_login_at', 'Current login date');
        $this->addCommentOnColumn('{{%user}}', 'current_login_from', 'Current login IP address');
    }

    /**
     * {@inheritdoc}
     * @return void
     */
    public function safeDown()
    {
        $this->dropTable('{{%user}}');
    }
}
