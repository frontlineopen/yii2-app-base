<?php
/**
 *
 * @package     Frontline WebApp Base
 *
 * @subpackage  <Custom RBAC Access Rule>
 *
 * @author      Sebastian Costiug <sebastian@frontline.ro>
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @category    rbac
 * @see         https://www.yiiframework.com/doc/api/2.0/yii-filters-accessrule
 *
 * @since       2020.06.15
 *
 */

namespace app\modules\user\components;

use Yii;

/**
 * Custom RBAC AccessRule
 */
class AccessRule extends \yii\filters\AccessRule
{
    /**
     * @inheritdoc
     * @param object $user The user model to check permissions for
     * @return boolean
     */
    protected function matchRole($user)
    {
        if (empty($this->roles)) {
            return true;
        }

        foreach ($this->roles as $role) {
            if ($role === '?') {
                if ($user->getIsGuest()) {
                    return true;
                }
            } elseif ($role === '@') {
                if (!$user->getIsGuest()) {
                    return true;
                }
            // Check if the user is logged in, and the roles match
            } elseif ($role === $user->getRole()) {
                if (!$user->getIsGuest()) {
                    return true;
                }
            }
        }

        return false;
    }
}
