<?php
/**
 *
 * @package     Frontline WebApp Base
 *
 * @subpackage  <Web User class>
 *
 * @author      Sebastian Costiug <sebastian@frontline.ro>
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @category    classes
 * @see         http://www.yiiframework.com/doc-2.0/guide-structure-models.html
 *
 * @since       2020.09.07
 *
 */

namespace app\modules\user\components;

use Yii;
use yii\web\User as CoreWebUser;
use app\modules\user\models\User as DBUser;
use app\helpers\DbOps;

/**
 * Extension of the web\User Class
 */
class User extends CoreWebUser
{
    /**
     * Logged user data stored for curent session use
     * @var mixed
     */
    public $data;

    /**
     * {@inheritdoc}
     * @return mixed
     */
    public function init()
    {
        parent::init();

        $this->data['first_name'] = $this->identity['first_name'];
        $this->data['last_name']  = $this->identity['last_name'] ? $this->identity['last_name'] : null;
        $this->data['email']      = $this->identity['email'];
        $this->data['birthday']   = $this->identity['birthday'] ? $this->identity['birthday'] : null;
        $this->data['role']       = $this->identity['role'];
        $this->data['department'] = $this->identity['department'] ? $this->identity['department'] : null;
        $this->data['category']   = $this->identity['category'] ? $this->identity['category'] : null;
        $this->data['page_size']  = $this->identity['page_size'] ? $this->identity['page_size'] : Yii::$app->settings->get('system', 'pageSize');
        $this->data['language']   = $this->identity['language'] ? $this->identity['language'] : Yii::$app->settings->get('system', 'defaultLanguage');
        $this->data['avatarUrl']  = $this->identity['avatarUrl'];
    }

    /**
     * {@inheritdoc}
     * @param object  $identity    The user identity information
     * @param boolean $cookieBased Whether the login is cookie-based
     * @param integer $duration    Number of seconds that the user can remain in logged-in status. If 0, it means login till the user closes the browser or the session is manually destroyed.
     * @return boolean
     */
    protected function afterLogin($identity, $cookieBased, $duration)
    {
        parent::afterLogin($identity, $cookieBased, $duration);

        // Saves the timestamp and the location of the current User
        $user = DBUser::findIdentity($identity['id']);
        if ($user) {
            if ($user->current_login_at && $user->current_login_from) {
                $user->last_login_at   = $user->current_login_at;
                $user->last_login_from = $user->current_login_from;
            }

            $user->current_login_at   = date('Y-m-d H:i:s');
            $user->current_login_from = Yii::$app->getRequest()->getUserIP();
            return $user->save();
        } else {
            return false;
        }
    }

    /**
     * Checks Admin role of logged user
     * @return boolean
     */
    public function isAdmin()
    {
        if ($this->role === DBUser::ROLE_ADMIN) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get first name of the logged user
     * @return string
     */
    public function getFirstName()
    {
        return $this->data['firstName'];
    }

    /**
     * Set first name to the logged user
     * @param string  $value      Attribute value.
     * @param boolean $persistent Switches between permanent and session change.
     * @return mixed
     */
    public function setFirstName($value, $persistent = false)
    {
        self::setUserAtt('first_name', $value, $persistent);
    }

    /**
     * Get last name of the logged user
     * @return string
     */
    public function getLastName()
    {
        return $this->data['lastName'];
    }

    /**
     * Set last name to the logged user
     * @param string  $value      Attribute value.
     * @param boolean $persistent Switches between permanent and session change.
     * @return mixed
     */
    public function setLastName($value, $persistent = false)
    {
        self::setUserAtt('last_name', $value, $persistent);
    }

    /**
     * Get email of the logged user
     * @return string
     */
    public function getEmail()
    {
        return $this->data['email'];
    }

    /**
     * Set email to the logged user
     * @param string  $value      Attribute value.
     * @param boolean $persistent Switches between permanent and session change.
     * @return mixed
     */
    public function setEmail($value, $persistent = false)
    {
        self::setUserAtt('email', $value, $persistent);
    }

    /**
     * Get birthday of the logged user
     * @return string
     */
    public function getBirthday()
    {
        return $this->data['birthday'];
    }

    /**
     * Set birthday to the logged user
     * @param string  $value      Attribute value.
     * @param boolean $persistent Switches between permanent and session change.
     * @return mixed
     */
    public function setBirthday($value, $persistent = false)
    {
        self::setUserAtt('birthday', $value, $persistent);
    }

    /**
     * Get role of the logged user
     * @return string
     */
    public function getRole()
    {
        return $this->data['role'];
    }

    /**
     * Set role to the logged user
     * @param string  $value      Attribute value.
     * @param boolean $persistent Switches between permanent and session change.
     * @return mixed
     */
    public function setRole($value, $persistent = false)
    {
        self::setUserAtt('role', $value, $persistent);
    }

    /**
     * Get department of the logged user
     * @return string
     */
    public function getDepartment()
    {
        return $this->data['department'];
    }

    /**
     * Set department to the logged user
     * @param string  $value      Attribute value.
     * @param boolean $persistent Switches between permanent and session change.
     * @return mixed
     */
    public function setDepartment($value, $persistent = false)
    {
        self::setUserAtt('department', $value, $persistent);
    }

    /**
    * Get category of the logged user
    * @return string
    */
    public function getCategory()
    {
        return $this->data['category'];
    }

    /**
     * Set category to the logged user
     * @param string  $value      Attribute value.
     * @param boolean $persistent Switches between permanent and session change.
     * @return mixed
     */
    public function setCategory($value, $persistent = false)
    {
        self::setUserAtt('category', $value, $persistent);
    }

    /**
     * Get pagination value selected by the logged user
     * @return string
     */
    public function getPageSize()
    {
        return $this->data['page_size'];
    }

    /**
     * Set category to the logged user
     * @param string  $value      Attribute value.
     * @param boolean $persistent Switches between permanent and session change.
     * @return mixed
     */
    public function setPageSize($value, $persistent = false)
    {
        self::setUserAtt('page_size', $value, $persistent);
    }

    /**
     * Get language value selected by the logged user
     * @return string
     */
    public function getLanguage()
    {
        return $this->data['language'];
    }

    /**
     * Set category to the logged user
     * @param string  $value      Attribute value.
     * @param boolean $persistent Switches between permanent and session change.
     * @return mixed
     */
    public function setLanguage($value, $persistent = false)
    {
        self::setUserAtt('language', $value, $persistent);
    }

    /**
     * Updates User data
     * @param string  $entry      Attribute to be updated.
     * @param string  $value      Value of the passed attribute to be updated.
     * @param boolean $persistent Switches between permanent and session change.
     * @return mixed
     */
    protected function setUserAtt($entry, $value, $persistent)
    {
        if ($persistent) {
            DbOps::modifyTableEntry('{{user}}', $entry, $value, $this->identity['id']);
        } else {
            $this->data[$entry] = $value;
        }
    }

    /**
     * User avatar URL
     * @return string
     */
    public function getAvatarUrl()
    {
        return $this->data['avatarUrl'];
    }

    /**
    * Get user model
    * @return boolean
    */
    public function getModel()
    {
        if ($this->id) {
            return DBUser::findIdentity($this->id);
        } else {
            return NULL;
        }
    }
}
