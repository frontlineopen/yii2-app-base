<?php
/**
 * @package     Frontline WebApp Base
 *
 * @subpackage  <User Controller>
 *
 * @author      Sebastian Costiug <sebastian@frontline.ro>
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @category    controllers
 * @see         http://www.yiiframework.com/doc-2.0/guide-structure-controllers.html
 *
 * @since       2020.06.15
 *
 */

namespace app\modules\user\controllers;

use app\components\BaseController;
use app\helpers\VarHelper;
use app\modules\user\components\AccessRule;
use app\modules\user\models\User;
use app\modules\user\models\UserSearch;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\BaseJson;
use yii\web\UploadedFile;

/**
 * User controller.
 */
class UserController extends BaseController
{
    /**
     * RBAC behaviours
     * @return mixed
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class'      => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'only'       => ['index', 'logout', 'add', 'view', 'update', 'delete'],
                'rules'      => [
                    [
                        'actions' => ['profile', 'logout'],
                        'allow'   => true,
                        'roles'   => [
                            User::ROLE_USER,
                            User::ROLE_ADMIN,
                        ],
                    ],
                    [
                        'actions' => ['index', 'view', 'update', 'delete'],
                        'allow'   => true,
                        'roles'   => [
                            User::ROLE_ADMIN,
                        ],
                    ],
                    [
                        'actions' => ['add'],
                        'allow'   => true,
                        'roles'   => $this->request->get('register') ? ['?'] : [User::ROLE_ADMIN],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'login'   => ['GET', 'POST'],
                    'logout'  => ['GET'],
                    'add'     => ['GET', 'POST'],
                    'update'  => ['GET', 'POST'],
                    'profile' => ['GET', 'POST'],
                    'delete'  => ['POST'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     * @return mixed
     */
    public function beforeAction($action)
    {
        $this->actionBar = [];

        return parent::beforeAction($action);
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();

        $params = $this->request->queryParams;

        if (count($params) <= 1) {
            $params = Yii::$app->session['userparams'];
            if (isset(Yii::$app->session['userparams']['page'])) {
                $this->request->setQueryParams(['page' => Yii::$app->session['userparams']['page']]);
            }
        } else {
            Yii::$app->session['userparams'] = $params;
        }

        $dataProvider = $searchModel->search($params);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * View an existing User.
     * @param integer $id Id of the selected user.
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        if ($model) {
            Yii::$app->audit->logModelView($model);
            if ($this->request->isAjax) {
                return BaseJson::encode([
                    'title' => Yii::t('app', 'User details: {name}', ['name' => $model->name]),
                    'body'  => $this->renderAjax('view', ['model' => $model]),
                ]);
            } else {
                return $this->render('view', ['model' => $model]);
            }
        } else {
            Yii::$app->session->setFlash('error', Yii::t('app', 'User not found!'));
        }
    }

    /**
     * Add a new User.
     * @param string $register If present Add user function works as register
     * @return mixed
     */
    public function actionAdd($register = null)
    {
        if (!$register || ($register && Yii::$app->getModule('user')->selfReg)) {
            $model = new User(['scenario' => User::SCENARIO_ADD_USER]);

            if ($this->request->isPost) {
                if ($model->load($this->request->post())) {
                    if ($this->request->isAjax) {
                        $result = [];
                        $params = [];

                        if ($model->validate()) {
                            $model->generatePasswordResetToken();

                            try {
                                $model->save();
                                $params['resetLink'] = Yii::$app->urlManager->createAbsoluteUrl(['/user/user/reset-password', 'token' => $model->password_reset_token]);

                                if (VarHelper::sendEmail($model->first_name, $model->email, Yii::t('app', 'Let\'s choose a password'), 'setPassword', $params)) {
                                    if ($register) {
                                        $result['status']  = 'success';
                                        $result['message'] = Yii::t('app', 'Please check your inbox for an email to choose a password.');
                                    } else {
                                        $result['status']  = 'success';
                                        $result['message'] = Yii::t('app', 'User <b>{name}</b> has been added and will receive an email to choose a password.', ['name' => $model->name]);
                                    }
                                } else {
                                    $result['status']  = 'error';
                                    $result['message'] = Yii::t('app', 'There was an error sending email.');
                                }
                            } catch (CDbException $e) {
                                $result['status']  = 'error';
                                $result['message'] = Yii::t('app', $e->errorInfo);
                            } catch (yii\db\IntegrityException $e) {
                                $result['status']  = 'error';
                                $result['message'] = Yii::t('app', $e->errorInfo);
                            }
                        } else {
                            $result['status']  = 'error';
                            $result['message'] = Yii::t('app', 'Please check input!');
                            foreach ($model->getErrors() as $field => $error) {
                                $result['errors']['user-' . $field] = $error;
                            }
                        }

                        return BaseJson::encode($result);
                    } else {
                        if ($model->validate()) {
                            $model->generatePasswordResetToken();

                            try {
                                if ($model->save()) {
                                    $params['resetLink'] = Yii::$app->urlManager->createAbsoluteUrl(['/user/user/reset-password', 'token' => $model->password_reset_token]);

                                    if (VarHelper::sendEmail($model->first_name, $model->email, Yii::t('app', 'Let\'s choose a password'), 'setPassword', $params)) {
                                        if ($register) {
                                            Yii::$app->session->setFlash('success', Yii::t('app', 'Please check your inbox for an email to choose a password.'));
                                        } else {
                                            Yii::$app->session->setFlash('success', Yii::t('app', 'User <b>{name}</b> has been added and will receive an email to choose a password', ['name' => $model->name]));
                                        }
                                    } else {
                                        Yii::$app->session->setFlash('success', Yii::t('app', 'There was an error sending email.'));
                                    }
                                } else {
                                    Yii::$app->session->setFlash('error', Yii::t('app', 'There was an error saving the info.'));
                                }

                                return $register ? $this->redirect(['/site/index']) : $this->redirect(['/user/user/index']);
                            } catch (CDbException $e) {
                                Yii::$app->session->setFlash('error', Yii::t('app', $e->errorInfo));
                            } catch (yii\db\IntegrityException $e) {
                                Yii::$app->session->setFlash('error', Yii::t('app', $e->errorInfo));
                            }
                        } else {
                            Yii::$app->session->setFlash('error', Yii::t('app', 'Please check input!'));
                        }
                    }
                } else {
                    throw new \yii\web\HttpException(204, Yii::t('app', 'Invalid request!'));
                }
            }

            if ($this->request->isAjax) {
                return BaseJson::encode([
                    'title' => Yii::t('app', 'Add user'),
                    'body'  => $this->renderAjax('add', ['model' => $model]),
                ]);
            } else {
                return $register ? $this->render('register', ['model' => $model]) : $this->$method('add', ['model' => $model]);
            }
        } elseif ($register && !Yii::$app->getModule('user')->selfReg) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'User registration is disabled!'));

            return $this->redirect(['/site/index']);
        }
    }

    /**
     * Updates an existing User.
     * @param integer $id Id of the selected user.
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model) {
            $model->scenario = User::SCENARIO_EDIT_USER;
            if ($this->request->isPost) {
                if ($model->load($this->request->post())) {
                    if ($this->request->isAjax) {
                        $result = [];

                        if ($model->validate()) {
                            try {
                                if ($model->save()) {
                                    $result['status']  = 'success';
                                    $result['message'] = Yii::t('app', 'User <b>{name}</b> has been updated!', ['name' => $model->name]);
                                } else {
                                    $result['status']  = 'error';
                                    $result['message'] = Yii::t('app', 'User <b>{name}</b> has not been updated!', ['name' => $model->name]);
                                }
                            } catch (CDbException $e) {
                                $result['status']  = 'error';
                                $result['message'] = Yii::t('app', $e->errorInfo);
                            } catch (yii\db\IntegrityException $e) {
                                $result['status']  = 'error';
                                $result['message'] = Yii::t('app', $e->errorInfo);
                            }
                        } else {
                            $result['status']  = 'error';
                            $result['message'] = Yii::t('app', 'Please check input!');
                            foreach ($model->getErrors() as $field => $error) {
                                $result['errors']['user-' . $field] = $error;
                            }
                        }

                        return BaseJson::encode($result);
                    } else {
                        if ($model->validate()) {
                            try {
                                if ($model->save()) {
                                    Yii::$app->session->setFlash('success', Yii::t('app', 'User <b>{name}</b> has been updated!', ['name' => $model->name]));

                                    return $this->redirect(['index']);
                                } else {
                                    Yii::$app->session->setFlash('error', Yii::t('app', 'User <b>{name}</b> has not been updated!', ['name' => $model->name]));
                                }
                            } catch (CDbException $e) {
                                Yii::$app->session->setFlash('error', Yii::t('app', $e->errorInfo));
                            } catch (yii\db\IntegrityException $e) {
                                Yii::$app->session->setFlash('error', Yii::t('app', $e->errorInfo));
                            }
                        } else {
                            Yii::$app->session->setFlash('error', Yii::t('app', 'Please check input!'));
                        }
                    }
                } else {
                    throw new \yii\web\HttpException(204, Yii::t('app', 'Invalid request!'));
                }
            }

            if ($this->request->isAjax) {
                return BaseJson::encode([
                    'title' => Yii::t('app', 'Update user: {name}', ['name' => $model->name]),
                    'body'  => $this->renderAjax('update', ['model' => $model]),
                ]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        } else {
            Yii::$app->session->setFlash('error', Yii::t('app', 'User not found!'));
        }
    }

    /**
     * Updates current user Profile.
     * @return mixed
     */
    public function actionProfile()
    {
        $model = $this->findModel(Yii::$app->user->identity['id']);
        if ($model) {
            $model->scenario = User::SCENARIO_EDIT_USER;
            if ($this->request->isPost) {
                if ($model->load($this->request->post())) {
                    $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
                    if ($this->request->isAjax) {
                        $result = [];

                        if ($model->validate()) {
                            try {
                                if ($model->imageFile) {
                                    $model->uploadImage();
                                }
                                if ($model->birthday) {
                                    $model->birthday = (date('Y-m-d', strtotime($model->birthday)));
                                }
                                if ($model->save()) {
                                    Yii::$app->language = $model->language;
                                    $result['status']   = 'success';
                                    $result['message']  = Yii::t('app', 'Your profile has been updated.');
                                } else {
                                    $result['status']  = 'error';
                                    $result['message'] = Yii::t('app', 'There was an error updating your profile!');
                                }
                            } catch (CDbException $e) {
                                $result['status']  = 'error';
                                $result['message'] = 'user' . Yii::t('app', $e->errorInfo);
                            } catch (yii\db\IntegrityException $e) {
                                $result['status']  = 'error';
                                $result['message'] = Yii::t('app', $e->errorInfo);
                            }
                        } else {
                            $result            = [];
                            $result['status']  = 'error';
                            $result['message'] = Yii::t('app', 'Please check input data!');
                            foreach ($model->getErrors() as $field => $error) {
                                $result['errors'][$field] = $error;
                            }
                        }

                        return BaseJson::encode($result);
                    } else {
                        if ($model->validate()) {
                            try {
                                if ($model->imageFile) {
                                    $model->uploadImage();
                                }
                                if ($model->birthday) {
                                    $model->birthday = (date('Y-m-d', strtotime($model->birthday)));
                                }
                                if ($model->save()) {
                                    Yii::$app->language = $model->language;
                                    Yii::$app->session->setFlash('success', Yii::t('app', 'Your profile has been updated.'));

                                    return $this->redirect(['/site/index']);
                                } else {
                                    Yii::$app->session->setFlash('error', Yii::t('app', 'There was an error updating your profile!'));
                                }
                            } catch (CDbException $e) {
                                Yii::$app->session->setFlash('error', Yii::t('app', $e->errorInfo));
                            } catch (yii\db\IntegrityException $e) {
                                Yii::$app->session->setFlash('error', Yii::t('app', $e->errorInfo));
                            }
                        } else {
                            Yii::$app->session->setFlash('error', Yii::t('app', 'Please check input!'));
                        }
                    }
                }
            }

            return $this->render('profile', [
                'model' => $model,
            ]);
        } else {
            Yii::$app->session->setFlash('error', Yii::t('app', 'User not found!'));
        }
    }

    /**
     * Deletes an existing User.
     * @throws HttpException400 is request is invalid
     * @param integer $id Id of the selected user.
     * @return mixed
     */
    public function actionDelete($id)
    {
        if ($this->request->isPost) {
            if ($this->request->isAjax) {
                $result = [];
                if ($id == $this->user->id) {
                    $result['status']  = 'error';
                    $result['message'] = Yii::t('app', 'You can\'t delete your logged in account!');
                } elseif ($id == 1) {
                    $result['status']  = 'error';
                    $result['message'] = Yii::t('app', 'Administrator account can\'t be deleted!');
                } else {
                    $model = User::findIdentity($id);
                    if ($model) {
                        try {
                            $name = $model->name;
                            $model->delete();
                            $result['status']  = 'success';
                            $result['message'] = Yii::t('app', 'User <b>{name}</b> has been deleted', ['name' => $name]);
                        } catch (CDbException $e) {
                            $result['status']  = 'error';
                            $result['message'] = Yii::t('app', $e->errorInfo);
                        } catch (yii\db\IntegrityException $e) {
                            $result['status']  = 'error';
                            $result['message'] = Yii::t('app', $e->errorInfo);
                        }
                    } else {
                        $result['status']  = 'error';
                        $result['message'] = Yii::t('app', 'User <b>{name}</b> has not been found!', ['name' => $model->name]);
                    }
                }

                return BaseJson::encode($result);
            } else {
                if ($id == $this->user->id) {
                    Yii::$app->session->setFlash('error', Yii::t('app', 'You can\'t delete your logged in account!'));
                } elseif ($id == 1) {
                    Yii::$app->session->setFlash('error', Yii::t('app', 'Administrator account can\'t be deleted!'));
                } else {
                    $model = User::findIdentity($id);
                    if ($model) {
                        try {
                            $model->delete();
                            Yii::$app->session->setFlash('success', Yii::t('app', 'User <b>{name}</b> has been deleted!', ['name' => $model->name]));

                            return $this->redirect(['/user/user/index']);
                        } catch (CDbException $e) {
                            Yii::$app->session->setFlash('error', Yii::t('app', $e->errorInfo));
                        } catch (yii\db\IntegrityException $e) {
                            Yii::$app->session->setFlash('error', Yii::t('app', $e->errorInfo));
                        }
                    } else {
                        Yii::$app->session->setFlash('error', Yii::t('app', 'User not found!'));
                    }
                }
            }
        } else {
            throw new \yii\web\HttpException(400, 'Invalid request!');
        }
    }

    /**
     * Initiates Password reset logic
     * @return mixed
     */
    public function actionForgotPassword()
    {
        $model      = new User(['scenario' => User::SCENARIO_FORGOT_PASSWORD]);
        $successMsg = Yii::t('app', 'If an account with specified email address is found, you will receive an email with instructions to reset your password.');

        if ($model->load($this->request->post()) && $model->validate()) {
            $user = User::findByEmail($model->email);

            if ($user) {
                if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
                    $user->generatePasswordResetToken();
                }
                $params              = [];
                $params['resetLink'] = Yii::$app->urlManager->createAbsoluteUrl(['/user/user/reset-password', 'token' => $user->password_reset_token]);

                if ($user->save() && VarHelper::sendEmail($user->first_name, $user->email, Yii::t('app', 'Password reset request'), 'passwordReset', $params)) {
                    Yii::$app->session->setFlash('success', $successMsg);
                }
            } else {
                Yii::$app->session->setFlash('success', $successMsg);
            }

            return $this->redirect(['/site/index']);
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets User password
     * If valid, the browser will be redirected to the 'login' page.
     * @param string $token Token for pasword reset.
     * @return mixed
     */
    public function actionResetPassword($token)
    {
        $model = new User(['scenario' => User::SCENARIO_RESET_PASSWORD]);

        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
                if ($model->validate()) {
                    $user = User::findByPasswordResetToken($token);
                    if ($user) {
                        if ($model->newPassword === $model->repeatPassword) {
                            $user->setPassword($model->newPassword);
                            if ($user->active === User::STATUS_INACTIVE) {
                                $user->active        = User::STATUS_ACTIVE;
                                $user->register_from = $this->request->getUserIP();
                            }
                            try {
                                $user->save();
                                Yii::$app->session->setFlash('success', Yii::t('app', 'Your password has been updated.'));

                                return $this->redirect(['/user/user/login']);
                            } catch (CDbException $e) {
                                Yii::$app->session->setFlash('error', Yii::t('app', $e->errorInfo));
                            } catch (yii\db\IntegrityException $e) {
                                Yii::$app->session->setFlash('error', Yii::t('app', $e->errorInfo));
                            }
                        } else {
                            Yii::$app->session->setFlash('error', Yii::t('app', 'New password and new password confirmation inputs don\'t match.'));
                        }
                    }
                } else {
                    Yii::$app->session->setFlash('error', Yii::t('app', 'There was an error updating your password!'));
                }
            }
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Changes current User password.
     * @return mixed
     */
    public function actionChangePassword()
    {
        $model = new User(['scenario' => User::SCENARIO_CHANGE_PASS]);

        if ($model) {
            if ($model->load($this->request->post())) {
                if ($model->validate()) {
                    $user = User::findIdentity(Yii::$app->user->id);

                    if ($user) {
                        if ($model->password) {
                            if (Yii::$app->security->validatePassword($model->password, $user->password_hash)) {
                                if ($model->newPassword === $model->repeatPassword) {
                                    $user->setPassword($model->newPassword);
                                    $result = [];

                                    try {
                                        $user->save();
                                        $params['password'] = $model->newPassword;
                                        VarHelper::sendEmail($user->first_name, $user->email, Yii::t('app', 'Change password'), 'passwordChange', $params);
                                    } catch (CDbException $e) {
                                        Yii::$app->session->setFlash('error', Yii::t('app', $e->errorInfo));
                                    } catch (yii\db\IntegrityException $e) {
                                        Yii::$app->session->setFlash('error', Yii::t('app', $e->errorInfo));
                                    }

                                    return $this->goBack();
                                } else {
                                    Yii::$app->session->setFlash('error', Yii::t('app', 'New password and new password confirmation inputs don\'t match.'));
                                }
                            } else {
                                Yii::$app->session->setFlash('error', Yii::t('app', 'Current password is incorrect!'));
                            }
                        } else {
                            Yii::$app->session->setFlash('error', Yii::t('app', 'Current password is required!'));
                        }
                    } else {
                        Yii::$app->session->setFlash('error', Yii::t('app', 'The requested page does not exist!'));
                    }
                }
            }
        }

        return $this->render('changePassword', [
            'model' => $model,
        ]);
    }

    /**
     * Logs in a user.
     * @return mixed
     */
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new User(['scenario' => User::SCENARIO_LOGIN]);

        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
                if ($model->login()) {
                    // Clear settings
                    Yii::$app->cache->set('settingsLoaded', false);

                    // Audit: User login
                    Yii::$app->audit->logMessage('Logged in', $model);

                    Yii::$app->session->setFlash('success', Yii::t('app', 'Welcome back, <b>{name}</b>.', ['name' => Yii::$app->user->identity->first_name]));

                    if (Yii::$app->user->getReturnUrl()) {
                        return $this->redirect(Yii::$app->user->getReturnUrl());
                    } else {
                        return $this->goHome();
                    }
                } else {
                    Yii::$app->session->setFlash('error', Yii::t('app', 'Incorrect email or password!'));
                }
            }
        }

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logs out the currently logged user.
     * @return mixed
     */
    public function actionLogout()
    {
        // Clear settings
        Yii::$app->cache->set('settingsLoaded', false);

        // Audit: User logout
        Yii::$app->audit->logMessage('Logged out', Yii::$app->user->getModel());

        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Finds the User model based on its primary key value.
     * @param integer $id Id of the selected user.
     * @return mixed
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            return false;
        }
    }
}
