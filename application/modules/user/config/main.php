<?php
/**
 *
 * @package     Frontline WebApp Base
 *
 * @subpackage  <User Module config segment>
 *
 * @author      Sebastian Costiug <sebastian@frontline.ro>
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @category    config
 * @see         https://www.yiiframework.com/doc/guide/2.0/en/concept-configurations
 *
 * @since       2020.07.21
 *
 */

return [
    // Application modules
    'modules'    => [
        'user' => [
            'class' => 'app\modules\user\Module',
        ],
    ],

    // Components
    'components' => [
        // User URLs
        'urlManager' => [
            'rules' => [
                // Frontend actions
                'account/login'                      => '/user/user/login',
                'account/logout'                     => '/user/user/logout',
                'account/profile'                    => '/user/user/profile',
                'account/verify-email/<token:\w+>'   => '/user/user/verify-email',
                'account/forgot-password'            => '/user/user/forgot-password',
                'account/reset-password/<token:\w+>' => '/user/user/reset-password',
                'account/reset-password'             => '/user/user/reset-password',
                'account/change-password'            => '/user/user/change-password',
                [
                    'pattern'  => 'account/register',
                    'route'    => '/user/user/add',
                    'defaults' => ['register' => 1],
                ],

                // Backend
                'users/<page:\d+>'                   => '/user/user/index',
                'users'                              => '/user/user/index',
                'user/add'                           => '/user/user/add',
                'user/view/<id:\d+>'                 => '/user/user/view',
                'user/update/<id:\d+>'               => '/user/user/update',
                'user/delete/<id:\d+>'               => '/user/user/delete',
            ],
        ],

        // User configuration
        'user'       => [
            'identityClass'   => 'app\modules\user\models\User',
            'enableAutoLogin' => true,
            'class'           => 'app\modules\user\components\User',
            'loginUrl'        => ['/user/user/login'],
        ],

    ],
];
