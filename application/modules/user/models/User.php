<?php
/**
 * @package     Frontline WebApp Base
 *
 * @subpackage  <User Model>
 *
 * @author      Sebastian Costiug <sebastian@frontline.ro>
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @category    models
 * @see         http://www.yiiframework.com/doc-2.0/guide-structure-controllers.html
 *
 * @since       2020.06.15
 */

namespace app\modules\user\models;

use app\helpers\VarHelper;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\IdentityInterface;
use yii\web\UploadedFile;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property int $id User ID
 * @property string $first_name First name
 * @property string|null $last_name Last name
 * @property string $email Email
 * @property int|null $email_notification Email notification
 * @property string $phone Phone
 * @property int|null $phone_notification Phone notification
 * @property int|null $password_hash Password hash
 * @property int|null $password_reset_token Password reset token
 * @property string|null $birthday Birthday
 * @property int|null $role Role
 * @property string|null $department Department
 * @property string|null $category
 * @property string|null $avatar Avatar
 * @property int|null $page_size Page size
 * @property int|null $hide_birthyear Hide birthyear
 * @property string|null $theme Theme
 * @property string|null $language Language
 * @property string|null $auth_key Authorization key
 * @property int|null $active Active
 * @property string|null $recovery_key Recovery key
 * @property string $register_at Register date
 * @property string|null $register_from Register IP Address
 * @property string|null $last_login_at Last login date
 * @property string|null $last_login_from Last login IP Address
 * @property string|null $current_login_at Current login date
 * @property string|null $current_login_from Current login IP Address
 */
class User extends ActiveRecord implements IdentityInterface
{
    // DB Entry User fields
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE   = 1;
    const STATUS_DELETED  = 2;
    const ROLE_USER       = 10;
    const ROLE_CLIENT     = 20;
    const ROLE_ADMIN      = 90;

    // User Model Scenarios
    const SCENARIO_LOGIN           = 'login';
    const SCENARIO_REGISTER_ADMIN  = 'register';
    const SCENARIO_ADD_USER        = 'add';
    const SCENARIO_EDIT_USER       = 'update';
    const SCENARIO_ACTIVATE_USER   = 'activateUser';
    const SCENARIO_CHANGE_PASS     = 'changePass';
    const SCENARIO_FORGOT_PASSWORD = 'forgotPassword';
    const SCENARIO_RESET_PASSWORD  = 'resetPassword';

    /** @var boolean Determines remember me status */
    public $rememberMe = true;
    /** @var string Password chosen by member */
    public $password;
    /** @var string  New password chosen in a CHANGE_PASS Scenario */
    public $newPassword;
    /** @var string  Password spelling verification in a CHANGE_PASS Scenario */
    public $repeatPassword;
    /** @var UploadedFile Image used for profile avatar */
    public $imageFile;

    /**
     * User Model DB Fields that use
     * preset array options
     */

    /** @var array $_statuses Array of possible user statuses */
    private static $_statuses = [
        self::STATUS_INACTIVE => 'Inactive',
        self::STATUS_ACTIVE   => 'Active',
    ];
    /** @var array $_roles Array of possible user roles */
    private static $_roles = [
        self::ROLE_USER   => 'User',
        self::ROLE_CLIENT => 'Client',
        self::ROLE_ADMIN  => 'Administrator',
    ];
    /** @var array $_themes Array of possible application themes */
    private static $_themes = [
        'theme1' => 'Red',
        'theme2' => 'Blue',
        'theme3' => 'Black',
    ];

    /**
     * {@inheritdoc}
     * @return string
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     * @return array
     */
    public function rules()
    {
        return [
            [['first_name', 'email', 'password', 'repeatPassword'], 'required', 'on' => self::SCENARIO_REGISTER_ADMIN],
            [['email', 'password'], 'required', 'on' => self::SCENARIO_LOGIN],
            [['first_name', 'email'], 'required', 'on' => self::SCENARIO_ADD_USER],
            [['first_name', 'email', 'role'], 'required', 'on' => self::SCENARIO_EDIT_USER],
            [['password'], 'required', 'on' => self::SCENARIO_CHANGE_PASS],
            [['newPassword', 'repeatPassword'], 'required', 'on' => [self::SCENARIO_CHANGE_PASS, self::SCENARIO_RESET_PASSWORD]],
            [['newPassword'], 'safe', 'on' => self::SCENARIO_CHANGE_PASS],
            [['email'], 'required', 'on' => self::SCENARIO_FORGOT_PASSWORD],
            [['email_notification', 'phone_notification', 'role', 'active'], 'integer'],
            [['birthday', 'hide_birthyear', 'register_at', 'last_login_at', 'current_login_at', 'password', 'repeatPassword', 'rememberMe'], 'safe'],
            [['category', 'register_at', 'register_from', 'theme', 'language', 'role', 'active'], 'safe', 'on' => [self::SCENARIO_REGISTER_ADMIN, self::SCENARIO_ADD_USER]],
            [['imageFile'], 'safe', 'on' => self::SCENARIO_EDIT_USER],
            [['first_name', 'last_name'], 'string', 'max' => 50],
            [['email', 'phone', 'password_hash', 'department', 'category', 'avatar', 'theme', 'language', 'register_from', 'last_login_from', 'current_login_from'], 'string', 'max' => 255],
            [['auth_key', 'password_reset_token', 'recovery_key'], 'string', 'max' => 64],
            [['email'], 'unique', 'on' => [self::SCENARIO_REGISTER_ADMIN, self::SCENARIO_ADD_USER]],
            ['email', 'email'],
            [['active'], 'safe', 'on' => self::SCENARIO_ACTIVATE_USER],
            ['active', 'default', 'value' => self::STATUS_INACTIVE],
            ['active', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE]],
            ['role', 'default', 'value' => self::ROLE_USER],
            ['role', 'in', 'range' => [self::ROLE_ADMIN, self::ROLE_USER]],
            ['page_size', 'default', 'value' => '25'],
            ['language', 'default', 'value' => 'en'],
            ['theme', 'default', 'value' => 'theme1'],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
            ['rememberMe', 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id'                   => Yii::t('app', 'ID'),
            'first_name'           => Yii::t('app', 'First name'),
            'last_name'            => Yii::t('app', 'Last name'),
            'email'                => Yii::t('app', 'Email'),
            'email_notification'   => Yii::t('app', 'Email notification'),
            'phone'                => Yii::t('app', 'Phone'),
            'phone_notification'   => Yii::t('app', 'Phone notification'),
            'password'             => Yii::t('app', 'Current password'),
            'newPassword'          => Yii::t('app', 'New password'),
            'repeatPassword'       => Yii::t('app', 'Confirm new password'),
            'password_hash'        => Yii::t('app', 'Password hash'),
            'password_reset_token' => Yii::t('app', 'Password reset token'),
            'birthday'             => Yii::t('app', 'Birthday'),
            'role'                 => Yii::t('app', 'Role'),
            'department'           => Yii::t('app', 'Department'),
            'category'             => Yii::t('app', 'Category'),
            'avatar'               => Yii::t('app', 'Avatar'),
            'page_size'            => Yii::t('app', 'Page size'),
            'hide_birthyear'       => Yii::t('app', 'Hide birthyear'),
            'theme'                => Yii::t('app', 'Theme'),
            'language'             => Yii::t('app', 'Language'),
            'auth_key'             => Yii::t('app', 'Authorization key'),
            'active'               => Yii::t('app', 'Active'),
            'recovery_key'         => Yii::t('app', 'Recovery key'),
            'register_at'          => Yii::t('app', 'Register at'),
            'register_from'        => Yii::t('app', 'Register IP address'),
            'last_login_at'        => Yii::t('app', 'Last login at'),
            'last_login_from'      => Yii::t('app', 'Last login IP address'),
            'current_login_at'     => Yii::t('app', 'Current login at'),
            'current_login_from'   => Yii::t('app', 'Current login IP address'),
            'name'                 => Yii::t('app', 'Name'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return array
     */
    public function behaviors()
    {
        return [
            'AuditBehavior' => [
                'class'   => 'app\modules\log\components\AuditBehavior',
                'ignored' => [
                    'auth_key',
                    'avatar',
                    'current_login_at',
                    'current_login_from',
                    'id',
                    'last_login_at',
                    'last_login_from',
                    'page_size',
                    'password_hash',
                    'password_reset_token',
                    'recovery_key',
                    'register_from',
                    'theme',
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     * @param integer $id The user ID to find in the DB
     * @return static
     */
    public static function findIdentity($id)
    {
        try {
            return static::findOne(['id' => $id]);
        } catch (yii\db\Exception $e) {
            return null;
        } catch (yii\base\InvalidConfigException $e) {
            return null;
        }
    }

    /**
     * {@inheritdoc}
     * @param string $token The user token to find in the DB
     * @param string $type  Type
     * @return void
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by email
     * @param string $email Email to find
     * @return static|null
     */
    public static function findByEmail($email)
    {
        try {
            return static::findOne(['email' => $email, 'active' => self::STATUS_ACTIVE]);
        } catch (yii\db\Exception $e) {
            return null;
        } catch (yii\base\InvalidConfigException $e) {
            return null;
        }
    }

    /**
     * Finds user by password reset token
     * @param string $token Password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }
        try {
            return static::findOne([
                'password_reset_token' => $token,
                // 'active' => self::STATUS_ACTIVE,
            ]);
        } catch (yii\db\Exception $e) {
            return null;
        } catch (yii\base\InvalidConfigException $e) {
            return null;
        }
    }

    /**
     * Finds out if password reset token is valid
     * @param string $token Password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire    = Yii::$app->getModule('user')->userPasswordResetTokenExpire;

        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     * @return integer
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     * @return string
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     * @param string $authKey The user Authentication Key to find in the DB
     * @return boolean
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validate password
     * @param string $password Password to validate
     * @return boolean If password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->findByEmail($this->email)->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     * @param string $password String from which to generate Hash
     * @return void
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     * @return void
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates password reset token
     * @return void
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     * @return void
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * Logs in a user using the provided email and password.
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->findByEmail($this->email)) {
            if ($this->validatePassword($this->password)) {
                if ($this->validate()) {
                    return Yii::$app->user->login($this->findByEmail($this->email), $this->rememberMe ? 3600 * 24 * 30 : 0);
                }
            }
        }

        return false;
    }

    /**
     * Gets all possible statuses for Users
     * @return array
     */
    public static function statusList()
    {
        return self::$_statuses;
    }

    /**
     * Gets the status of current User
     * @param string $status String to get a status value for
     * @return integer
     */
    public static function getStatus($status)
    {
        return !empty(self::$_statuses[$status]) ? self::$_statuses[$status] : null;
    }

    /**
     * Gets the notification status of the selected notification type for the current User
     * @param string $notification String to get a notification status value
     * @return integer
     */
    public function getNotification($notification)
    {
        return $this->$notification ? 'Active' : 'Inactive';
    }

    /**
     * Gets all possible roles for Users
     * @return array
     */
    public static function roleList()
    {
        return self::$_roles;
    }

    /**
     * Gets the role of current User
     * @param string $role String to get a role value for
     * @return integer
     */
    public static function getRole($role)
    {
        return !empty(self::$_roles[$role]) ? self::$_roles[$role] : null;
    }

    /**
     * Gets the language chosen by the current User
     * @param string $language String to get a language value for
     * @return integer
     */
    public static function getLanguage($language)
    {
        $defaultLanguage = Yii::$app->params['allowedLanguages'][Yii::$app->settings->get('system', 'defaultLanguage')];
        return !empty(Yii::$app->params['allowedLanguages'][$language]) ? Yii::$app->params['allowedLanguages'][$language] : $defaultLanguage;
    }

    /**
     * Gets a list of previously selected categories or departments
     * @param string $column The list type to get (either departments or categories)
     * @return array
     */
    public static function getFieldsList($column)
    {
        $fieldsList = User::find()->select($column)->from('user')->distinct()->createCommand()->queryAll();

        $result = [];
        foreach ($fieldsList as $field) {
            $result[$field[$column]] = $field[$column];
        }

        return $result;
    }

    /**
     * Gets all possible themes for the application
     * @return array
     */
    public static function getThemeList()
    {
        return self::$_themes;
    }

    /**
     * Gets the theme chosen by the current User
     * @param string $theme String to get a theme value for
     * @return integer
     */
    public static function getTheme($theme)
    {
        return !empty(self::$_themes[$theme]) ? self::$_themes[$theme] : null;
    }

    /**
     * Gets the avatar link for the current User
     * @return integer
     */
    public function getAvatarUrl()
    {
        return self::getAvatarUrlByID($this->id);
    }

    /**
     * Gets the Birthday of the current User
     * @return integer
     */
    public function getUserBirthday()
    {
        if ($this->birthday) {
            if ($this->hide_birthyear) {
                return Yii::$app->formatter->asDate($this->birthday, 'MMMM dd');
            } else {
                return Yii::$app->formatter->asDate($this->birthday);
            }
        } else {
            return Yii::t('app', 'Birthday not set');
        }
    }

    /**
     * Gets the name of the current user
     * @return string
     */
    public function getName()
    {
        if ($this->hasAttribute('first_name') || $this->hasAttribute('last_name')) {
            $name = trim(($this->first_name ? $this->first_name : '') . ($this->last_name ? ' ' . $this->last_name : ''));
        } else {
            $name = '';
        }

        return $name;
    }

    /**
     * Gets the avatar link of the specified User ID
     * @param integer $id Model ID
     * @return string
     */
    public static function getAvatarUrlById($id)
    {
        $defaultAvatar = 'avatars' . DIRECTORY_SEPARATOR . 'default-profile.png';
        $defaultLocation = Url::home(true) . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR;
        $defaultLocationPath = Yii::getAlias('@uploads') . DIRECTORY_SEPARATOR;

        $model = static::findOne(['id' => $id]);
        if ($model) {
            if ($model->avatar) {
                $avatarFile = $defaultLocationPath . $model->avatar;

                if (file_exists($avatarFile)) {
                    return Url::to($defaultLocation . $model->avatar);
                } else {
                    return Url::to($defaultLocation . $defaultAvatar);
                }
            } else {
                return Url::to($defaultLocation . $defaultAvatar);
            }
        } else {
            return Url::to($defaultLocation . $defaultAvatar);
        }
    }

    /**
     * Checks Admin role of current user
     * @param string $email Email of the user to check admin status for
     * @return boolean
     */
    public static function isUserAdmin($email)
    {
        if (static::findOne(['email' => $email, 'role' => self::ROLE_ADMIN])) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Uploads a file and saves it to a specified folder
     * @return boolean
     */
    public function uploadImage()
    {
        if ($this->validate()) {
            $imageFile = VarHelper::uploadFile($this->imageFile, 'avatars');

            if ($imageFile) {
                $this->avatar    = 'avatars' . DIRECTORY_SEPARATOR . $imageFile;
                $this->imageFile = null;
            } else {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Selected image is invalid.'));
            }

            try {
                $this->save();
            } catch (CDbException $e) {
                Yii::$app->session->setFlash('error', Yii::t('app', $e->errorInfo));
            } catch (yii\db\IntegrityException $e) {
                Yii::$app->session->setFlash('error', Yii::t('app', $e->errorInfo));
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * Deletes a file
     * @return void
     */
    public function deleteImage()
    {
        VarHelper::deleteFile($this->avatar);
        $this->avatar = null;
        try {
            $this->save();
        } catch (CDbException $e) {
        }
    }
}
