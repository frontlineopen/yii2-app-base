<?php
/**
 *
 * @package     Frontline WebApp Base
 *
 * @subpackage  <User Search model>
 *
 * @author      Sebastian Costiug <sebastian@frontline.ro>
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @category    models
 * @see         http://www.yiiframework.com/doc-2.0/guide-structure-models.html
 *
 * @since       2020.06.15
 *
 */

namespace app\modules\user\models;

use app\modules\user\models\User;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * UserSearch represents the model behind the search form of `app\modules\user\models\User`.
 */
class UserSearch extends User
{
    /* Model name */
    public $name;

    /**
     * {@inheritdoc}
     * @return array
     */
    public function rules()
    {
        return [
            [['id', 'email_notification', 'phone_notification', 'role', 'active'], 'integer'],
            [['name', 'first_name', 'last_name', 'email', 'phone', 'password_hash', 'password_reset_token', 'birthday', 'department', 'category', 'avatar', 'theme', 'language', 'auth_key', 'recovery_key', 'register_at', 'register_from', 'last_login_at', 'last_login_from', 'current_login_at', 'current_login_from'], 'safe'],
        ];
    }
    /**
     * {@inheritdoc}
     * @return mixed
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params Search parameters
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => Yii::$app->user->getPageSize(),
            ],
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id',
                'name' => [
                    'asc'     => ['first_name' => SORT_ASC, 'last_name' => SORT_ASC],
                    'desc'    => ['first_name' => SORT_DESC, 'last_name' => SORT_DESC],
                    'label'   => Yii::t('app', 'Name'),
                    'default' => SORT_ASC,
                ],
                'email',
                'birthday',
                'role',
                'department',
                'category',
                'active',
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'                 => $this->id,
            'email_notification' => $this->email_notification,
            'phone_notification' => $this->phone_notification,
            'birthday'           => $this->birthday,
            'role'               => $this->role,
            'active'             => $this->active,
            'register_at'        => $this->register_at,
            'last_login_at'      => $this->last_login_at,
            'current_login_at'   => $this->current_login_at,
        ]);

        $query->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'department', $this->department])
            ->andFilterWhere(['like', 'category', $this->category])
            ->andFilterWhere(['like', 'theme', $this->theme])
            ->andFilterWhere(['like', 'language', $this->language])
            ->andFilterWhere(['like', 'register_from', $this->register_from])
            ->andFilterWhere(['like', 'last_login_from', $this->last_login_from])
            ->andFilterWhere(['like', 'current_login_from', $this->current_login_from]);

        $query->andWhere('first_name LIKE "%' . $this->name . '%" ' . 'OR last_name LIKE "%' . $this->name . '%" ' . 'OR CONCAT(first_name, " ", last_name) LIKE "%' . $this->name . '%"');

        return $dataProvider;
    }

    /**
     * Cleaner index URL with filter params
     */
    public function formName()
    {
        return '';
    }
}
