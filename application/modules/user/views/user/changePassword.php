<?php

use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Change password');
?>
<div class="site-change-password">
    <div class="m-auto col-xl-3 col-md-6">
        <div class="card-group">
            <div class="card">
                <?php $form = ActiveForm::begin(['id' => 'change-password-form']); ?>
                <div class="card-body">
                    <div class="col-xl-12 col-md-12 mx-auto">
                        <p class="text-muted"><?= Yii::t('app', 'Change your password')?></p>
                        <?= $form->field($model, 'password')->passwordInput() ?>
                        <?= $form->field($model, 'newPassword')->passwordInput() ?>
                        <?= $form->field($model, 'repeatPassword')->passwordInput() ?>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="form-actions text-right">
                        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
                        <?= Html::a(Yii::t('app', 'Back'), !empty(Yii::$app->request->referrer) ? Yii::$app->request->referrer : Url::to(['/site/index']), ['class' => 'btn btn-info float-left']) ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
