<?php
use app\modules\user\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

\yii\web\YiiAsset::register($this);

$this->title = Yii::t('app', 'View user: {name}', ['name' => $model->name]);
?>
<div class="user-view">

    <?= DetailView::widget([
        'model'      => $model,
        'options'    => [
            'class'             => 'table table-responsive-sm table-sm',
        ],
        'attributes' => [
            [
                'attribute'      => 'avatar',
                'captionOptions' => [
                    'class' => '',
                ],
                'contentOptions' => [
                    'class' => '',
                ],
                'format'         => 'raw',
                'label'          => Html::tag('h3', Html::encode($model->name), ['class' => 'mt-4']),
                'value'          => Html::img($model->getAvatarUrl(), ['class' => 'rounded mx-auto d-block float-right', 'style' => 'width: 80px;']),
            ],
            [
                'attribute'      => 'email',
                'format'         => 'email',
                'visible'        => !empty($model->email),
                'captionOptions' => [
                    'class' => '',
                ],
                'contentOptions' => [
                    'class' => '',
                ],
            ],
            [
                'attribute'      => 'email_notification',
                'format'         => 'raw',
                'value'          => '<i class="fas fa-power-off"></i>',
                'contentOptions' => [
                    'class' => $model->email_notification ? 'text-success' : 'text-secondary',
                ],
            ],
            [
                'attribute' => 'phone',
                'visible'   => !empty($model->phone),
            ],
            [
                'attribute'      => 'phone_notification',
                'format'         => 'raw',
                'value'          => '<i class="fas fa-power-off"></i>',
                'contentOptions' => [
                    'class' => $model->phone_notification ? 'text-success' : 'text-secondary',
                ],
            ],
            [
                'attribute' => 'birthday',
                'visible'   => !empty($model->birthday),
                'value'     => $model->getUserBirthday(),
            ],
            [
                'attribute' => 'role',
                'value'     => User::getRole($model->role),
            ],
            [
                'attribute' => 'department',
                'visible'   => !empty($model->department),
            ],
            [
                'attribute' => 'category',
                'visible'   => !empty($model->category),
            ],
            [
                'attribute' => 'language',
                'value'     => $model->getLanguage($model->language),
            ],
            [
                'attribute' => 'theme',
                'value'     => User::getTheme($model->theme),
            ],
            [
                'attribute'      => 'active',
                'label'          => 'Status',
                'format'         => 'raw',
                'value'          => '<i class="fas fa-power-off"></i>',
                'contentOptions' => [
                    'class' => $model->active ? 'text-success' : 'text-secondary',
                ],
            ],
            [
                'attribute' => 'register_at',
                'visible'   => !empty($model->register_at),
            ],
            [
                'attribute' => 'register_from',
                'visible'   => !empty($model->register_from),
            ],
            [
                'attribute' => 'last_login_at',
                'visible'   => !empty($model->last_login_at),
            ],
            [
                'attribute' => 'last_login_from',
                'visible'   => !empty($model->last_login_from),
            ],
            [
                'attribute' => 'current_login_at',
                'visible'   => !empty($model->current_login_at),
            ],
            [
                'attribute' => 'current_login_from',
                'visible'   => !empty($model->current_login_from),
            ],
        ],
    ]) ?>

    <div class="text-right">
        <?= Html::a(Yii::t('app', 'Close'), null, ['class' => 'btn btn-info float-left', 'data-dismiss' => 'modal']) ?>
        <?= Html::a(Yii::t('app', 'Update'), Url::to(['/user/user/update', 'id' => $model->id]), ['class' => 'btn btn-primary btn-update']) ?>
    </div>

</div>
