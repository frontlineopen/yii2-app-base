<?php

use kartik\form\ActiveForm;
use kartik\switchinput\SwitchInput;
use yii\helpers\Html;
use yii\helpers\Url;

$this->context->layout = '//main-full';
$this->title = Yii::t('app', 'Login');
?>
<div class="row justify-content-center pt-5">
    <div class="col-md-5">
        <div class="card-group">
            <div class="card">
                <div class="bg-primary text-center text-white p-5 rounded-top">
                    <h1><?= Html::encode($this->context->app->name) ?></h1>
                    <p class="text-muted"> <?= Yii::t('app', 'Login to your account') ?></p>
                </div>
                <div class="p-4">
                    <?php $form = ActiveForm::begin(); ?>
                        <?= $form->field($model, 'email', [
                                'addon' => [
                                    'prepend' => [
                                        'content' => '<i class="fas fa-user"></i>'
                                    ]
                                ]
                            ])->textInput([
                                'placeholder' => 'email@example.com',
                                'autofocus' => 'autofocus',
                            ]); ?>

                        <?= $form->field($model, 'password', [
                                'addon' => [
                                    'prepend' => [
                                        'content' => '<i class="fas fa-lock"></i>'
                                    ]
                                ]
                            ])->passwordInput([
                                'placeholder' => '*********',
                            ]); ?>

                        <?= $form->field($model, 'rememberMe')->widget(SwitchInput::classname(), [
                            'type' => SwitchInput::CHECKBOX,
                        ]) ?>

                        <div class="form-group">
                            <div class="text-right">
                                <?= Html::submitButton(Yii::t('app', 'Login'), ['class' => 'btn bg-primary text-white px-4']) ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6 text-left">
                                <?php
                                if (Yii::$app->controller->module->selfReg == true) {
                                    echo Html::a(Yii::t('app', 'Register an account'), Url::to(['/user/user/add', 'register' => 1]));
                                }?>
                            </div>

                            <div class="col-6 text-right">
                                <?= Html::a(Yii::t('app', 'Forgot password?'), ['/user/user/forgot-password']) ?>
                            </div>
                        </div>
                    <?php $form->end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
