<?php

use app\modules\user\models\User;
use kartik\grid\GridView;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Users');
?>

<div class="pjax-grid-container card m-auto">
    <div class="card-body">

        <?= GridView::widget([
            'id'           => 'grid-view',
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'columns'      => [
                [
                    'attribute' => 'name',
                ],
                [
                    'attribute'           => 'role',
                    'filter'              => User::roleList(),
                    'value'               => function ($model) {
                        return User::getRole($model->role);
                    },
                    'filterType'          => GridView::FILTER_SELECT2,
                    'filterWidgetOptions' => [
                        'options'       => ['prompt' => ''],
                        'pluginOptions' => ['allowClear' => true],
                    ],
                ],
                [
                    'attribute'      => 'email',
                    'format'         => 'email',
                    'contentOptions' => ['class' => 'd-lg-down-none'],
                    'headerOptions'  => ['class' => 'd-lg-down-none'],
                    'filterOptions'  => ['class' => 'd-lg-down-none'],
                ],
                [
                    'attribute'      => 'phone',
                    'contentOptions' => ['class' => 'd-md-down-none'],
                    'headerOptions'  => ['class' => 'd-md-down-none'],
                    'filterOptions'  => ['class' => 'd-md-down-none'],
                ],
                [
                    'attribute'           => 'department',
                    'contentOptions'      => ['class' => 'd-lg-down-none'],
                    'headerOptions'       => ['class' => 'd-lg-down-none'],
                    'filter'              => User::getFieldsList('department'),
                    'filterOptions'       => ['class' => 'd-lg-down-none'],
                    'filterType'          => GridView::FILTER_SELECT2,
                    'filterWidgetOptions' => [
                        'options'       => ['prompt' => ''],
                        'pluginOptions' => ['allowClear' => true],
                    ],
                ],
                [
                    'attribute'           => 'category',
                    'contentOptions'      => ['class' => 'd-sm-down-none'],
                    'headerOptions'       => ['class' => 'd-sm-down-none'],
                    'filter'              => User::getFieldsList('category'),
                    'filterOptions'       => ['class' => 'd-sm-down-none'],
                    'filterType'          => GridView::FILTER_SELECT2,
                    'filterWidgetOptions' => [
                        'options'       => ['prompt' => ''],
                        'pluginOptions' => ['allowClear' => true],
                    ],
                ],
                [
                    'attribute'           => 'active',
                    'label'               => Yii::t('app', 'Status'),
                    'filter'              => User::statusList(),
                    'value'               => function ($model) {
                        return User::getStatus($model->active);
                    },
                    'contentOptions'      => ['class' => 'd-sm-down-none'],
                    'headerOptions'       => ['class' => 'd-sm-down-none'],
                    'filterOptions'       => ['class' => 'd-sm-down-none'],
                    'filterType'          => GridView::FILTER_SELECT2,
                    'filterWidgetOptions' => [
                        'options'       => ['prompt' => ''],
                        'pluginOptions' => ['allowClear' => true],
                    ],
                ],
                [
                    'class'          => 'kartik\grid\ActionColumn',
                    'hidden'         => !Yii::$app->user->isAdmin(),
                    'visibleButtons' => [
                        'update' => Yii::$app->user->isAdmin(),
                        'delete' => Yii::$app->user->isAdmin(),
                    ],
                ],
            ],
            'options'      => [
                'data-addurl' => Url::to(['/' . Yii::$app->controller->module->id . '/' . Yii::$app->controller->id . '/add']),
            ],
        ]) ?>

    </div>
</div>
