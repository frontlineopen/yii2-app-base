<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Password reset request');
$this->context->layout = '//main-full';
?>
<div class="row justify-content-center pt-5">
    <div class="col-md-5">
        <div class="card-group">
            <div class="card">
                <div class="text-center bg-primary text-white p-5 rounded-top">
                    <h1><?= Html::encode(Yii::$app->name) ?></h1>
                    <p class="text-muted"> <?= Yii::t('app', 'Password reset request') ?></p>
                </div>
                <div class="p-4">
                    <p class="text-muted"><?= Yii::t('app', 'Fill out your account email address to get a password reset link.')?></p>
                    <div class="mb-1">
                        <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>
                        <?= $form->field($model, 'email', [
                            'addon' => [
                                'prepend' => [
                                    'content' => '<i class="fas fa-user"></i>'
                                    ]
                                ]
                            ])->textInput(['placeholder' => 'email@example.com', 'autofocus' => 'autofocus']); ?>
                        <div class="form-group">
                            <div class="text-right">
                                <?= Html::submitButton(Yii::t('app', 'Send'), ['class' => 'btn bg-primary text-white px-4']) ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6 text-left">
                                <?= Html::a(Yii::t('app', 'Back to Login'), ['/user/user/login']) ?>
                            </div>
                            <div class="col-6 text-right">
                            <?php if (Yii::$app->controller->module->selfReg == true) {
                                echo Html::a(Yii::t('app', 'Register an account'), Url::to(['/user/user/add', 'register' => 1]));
                            } ?>
                            </div>
                        </div>
                        <?php $form->end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
