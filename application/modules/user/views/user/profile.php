<?php
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use kartik\file\FileInput;
use yii\bootstrap4\Modal;
use kartik\switchinput\SwitchInput;
use app\modules\user\models\User;
use app\modules\settings\models\SettingsForm;

$this->title = Yii::t('app', 'My Profile');
?>
<div class="user-profile">
    <div class="m-auto col-xl-8 ">
        <div class="card">
            <?php $form = ActiveForm::begin(); ?>
            <div class="card-body">
                <div class="row col-xl-12 col-md-12 col-sm-12">
                    <div class="col-xl-5 col-md-5 offset-md-2 order-xl-12 order-lg-12 order-md-12">
                        <?= $form->field($model, 'imageFile')->widget(FileInput::classname(), [
                            'name'    => 'avatar',
                            'options' => [
                                'accept' => 'image/*',
                                'id'     => 'avatar',
                                'class'  => '',
                            ],
                            'pluginOptions' => [
                                'maxFileCount'           => 1,
                                'maxFileSize'            => 1500,
                                'overwriteInitial'       => true,
                                'browseOnZoneClick'      => true,
                                'showPreview'            => true,
                                'showClose'              => false,
                                'showBrowse'             => false,
                                'showUpload'             => false,
                                'showCancel'             => false,
                                'showCaption'            => false,
                                'removeLabel'            => 'Cancel',
                                'removeIcon'             => '<i class="fas fa-window-close"></i>',
                                'removeTitle'            => 'Cancel',
                                'removeClass'            => 'btn btn-warning btn-block',
                                'mainClass'              => 'border-0 d-flex justify-content-center',
                                // 'frameClass'             => 'mx-auto',
                                'previewClass'           => 'border-0 d-flex justify-content-center',
                                // 'captionClass'           => 'mx-auto',
                                'defaultPreviewContent'  => Html::img($model->getAvatarUrl(), ['class' => 'rounded-circle mx-auto', 'style' => 'width: 150px;']),
                                'initialPreviewFileType' => 'image',
                                'maxImageWidth'          => 250,
                                'maxImageHeight'         => 250,
                                'resizePreference'       => 'height',
                                'resizeImage'            => true,
                                ],
                        ])->label(false) ?>
                    </div>
                    <div class="col-xl-5 col-md-5 order-xl-1 order-lg-1 order-md-1 ">
                        <div class="row">
                            <div class="col-md-12">
                                <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row col-xl-12 col-md-12 col-sm-12">
                    <div class="col-xl-6">
                        <div class="row">
                            <div class="col-xl-9 col-md-9 col-sm-9">
                                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-xl-3 col-md-3 col-sm-3">
                                <?= $form->field($model, 'email_notification')->widget(SwitchInput::classname(), [
                                    'type' => SwitchInput::CHECKBOX,
                                    ])->label('Notification') ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xl-9 col-md-9 col-sm-9">
                                <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-xl-3 col-md-3 col-sm-3">
                                <?= $form->field($model, 'phone_notification')->widget(SwitchInput::classname(), [
                                    'type' => SwitchInput::CHECKBOX,
                                    ])->label('Notification') ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xl-9 col-md-9 col-sm-9">
                                <?= $form->field($model, 'birthday')->widget(DatePicker::classname(), [
                                    'options' => ['placeholder' => Yii::t('app', 'Enter birth date ...')],
                                    'pluginOptions' => [
                                        'autoclose' => true,
                                        'format' => 'dd-mm-yyyy'
                                    ]
                                ]); ?>
                            </div>
                            <div class="col-xl-3 col-md-3 col-sm-3">
                                <?= $form->field($model, 'hide_birthyear')->widget(SwitchInput::classname(), [
                                    'type' => SwitchInput::CHECKBOX,
                                    ])->label('Hide year') ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-5 offset-xl-1">
                        <div class="row">
                            <div class="col-xl-6 col-md-6 col-sm-6">
                                <?= $form->field($model, 'page_size')->widget(Select2::classname(), [
                                    'data'       => Yii::$app->settings->getAllowedPagination(),
                                    'hideSearch' => true,
                                ]); ?>
                            </div>
                            <div class="col-xl-6 col-md-6 col-sm-6">
                                <?=  $form->field($model, 'theme')->widget(Select2::classname(), [
                                    'data'       => User::getThemeList(),
                                    'hideSearch' => true,
                                ]); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xl-6 col-md-6 col-sm-6">
                                <?=  $form->field($model, 'language')->widget(Select2::classname(), [
                                    'data'       => Yii::$app->settings->getAllowedLanguages(),
                                    'hideSearch' => true,
                                ]); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="form-actions text-right">
                    <?= Html::a(Yii::t('app', 'Back'), !empty(Yii::$app->request->referrer) ? Yii::$app->request->referrer : null, ['class' => 'btn btn-info float-left']) ?>
                    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
