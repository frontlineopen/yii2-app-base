<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\switchinput\SwitchInput;

$this->title = Yii::t('app', 'Register');
$this->context->layout = '//main-full';
?>
<div class="row justify-content-center pt-5">
    <div class="col-md-5">
        <div class="card-group">
            <div class="card">
                <div class="text-center bg-primary text-white p-5 rounded-top">
                    <h1><?= Html::encode(Yii::$app->name) ?></h1>
                    <p class="text-muted"> <?= Yii::t('app', 'Register a new account') ?></p>
                </div>
                <div class="p-4">
                    <p class="text-muted"><?=  Yii::t('app', 'Please fill out the fields below')?></p>
                    <div class="mb-1">
                        <?php $form = ActiveForm::begin(['id' => 'form-register']); ?>
                        <?= $form->field($model, 'first_name', [
                            'addon' => [
                                'prepend' => [
                                    'content' => '<i class="fas fa-user"></i>'
                                    ]
                                ]
                            ])->textInput(['autofocus' => 'autofocus', 'placeholder' => 'Jack']); ?>

                        <?= $form->field($model, 'last_name', [
                                'addon' => [
                                    'prepend' => [
                                        'content' => '<i class="fas fa-user"></i>'
                                    ]
                                ]
                            ])->textInput(['placeholder' => 'Smith']); ?>

                        <?= $form->field($model, 'email', [
                                'addon' => [
                                    'prepend' => [
                                        'content' => '<i class="fas fa-envelope"></i>'
                                    ]
                                ]
                            ])->textInput(['placeholder' => 'email@example.com']); ?>

                        <?= $form->field($model, 'email_notification')->widget(SwitchInput::classname(), [
                                'type' => SwitchInput::CHECKBOX,
                        ]) ?>

                        <?= $form->field($model, 'phone', [
                            'addon' => [
                                'prepend' => [
                                    'content' => '<i class="fas fa-phone"></i>'
                                    ]
                                ]
                            ])->textInput(['placeholder' => '1234-567-890']); ?>

                        <?= $form->field($model, 'phone_notification')->widget(SwitchInput::classname(), [
                                'type' => SwitchInput::CHECKBOX,
                        ]) ?>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-6 text-left">
                                    <?= Html::a(Yii::t('app', 'Back to Login'), ['/user/user/login']) ?>
                                </div>
                                <div class="col-6 text-right">
                                    <?= Html::submitButton(Yii::t('app', 'Register'), ['class' => 'btn bg-primary text-white', 'name' => 'register-button']) ?>
                                </div>
                            </div>
                        </div>
                        <?php $form->end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
