<?php

use app\assets\AppAsset;
use app\modules\user\models\User;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use kartik\switchinput\SwitchInput;
use yii\helpers\Html;
use yii\web\JsExpression;

AppAsset::register($this);
?>

<div class="m-auto">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6 col-12">
            <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>
            <div class="form-group d-flex">
                <?= $form->field($model, 'email', ['options' => [ 'class' => 'w-100']])->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'email_notification', ['options' => ['class' => 'col-3']])->widget(SwitchInput::classname(), [
                    'type'          => SwitchInput::CHECKBOX,
                    'containerOptions' => [
                        'class' => false,
                    ],
                    'pluginOptions' => [
                        'onText'      => '<i class="fas fa-power-off"></i>',
                        'offText'     => '<i class="fas fa-power-off"></i>',
                        'onColor'     => 'success',
                        'handleWidth' => 10,
                    ]
                ])->label(Yii::t('app', 'Notification')) ?>
            </div>
            <div class="form-group d-flex">
                <?= $form->field($model, 'phone', ['options' => [ 'class' => 'w-100']])->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'phone_notification', ['options' => ['class' => 'col-3']])->widget(SwitchInput::classname(), [
                    'type'          => SwitchInput::CHECKBOX,
                    'containerOptions' => [
                        'class' => false,
                    ],
                    'pluginOptions' => [
                        'onText'      => '<i class="fas fa-power-off"></i>',
                        'offText'     => '<i class="fas fa-power-off"></i>',
                        'onColor'     => 'success',
                        'handleWidth' => 10,
                    ]
                ])->label(Yii::t('app', 'Notification')) ?>
            </div>
        </div>
        <div class="col-md-6 col-12">
            <?= $form->field($model, 'role')->widget(Select2::classname(), [
                'data'       => User::roleList(),
                'hideSearch' => false,
                'options'    => [
                    'multiple'    => false,
                    'placeholder' => Yii::t('app', 'Select a role ...'),
                    'value'       => $model->role,
                ],
                'pluginOptions' => [
                    'tags'           => true,
                    'dropdownParent' => new JsExpression('$("#activity-modal")'),
                ],
            ]) ?>
            <?= $form->field($model, 'department')->widget(Select2::classname(), [
                'data'       => $model->getFieldsList('department'),
                'hideSearch' => false,
                'options'    => [
                    'multiple'    => false,
                    'placeholder' => Yii::t('app', 'Select a department ...'),
                    'value'       => $model->department,
                ],
                'pluginOptions' => [
                    'tags'           => true,
                    'dropdownParent' => new JsExpression('$("#activity-modal")'),
                ],
            ]) ?>
            <?= $form->field($model, 'category')->widget(Select2::classname(), [
                'data'       => $model->getFieldsList('category'),
                'hideSearch' => false,
                'options'    => [
                    'placeholder' => Yii::t('app', 'Select a category ...'),
                    'value'       => $model->category,
                ],
                'pluginOptions' => [
                    'tags'           => true,
                    'dropdownParent' => new JsExpression('$("#activity-modal")'),
                ],
            ]) ?>
            <?= $form->field($model, 'active')->widget(SwitchInput::classname(), [
                'type'          => SwitchInput::CHECKBOX,
                'containerOptions' => [
                    'class' => false,
                ],
                'pluginOptions' => [
                    'onText'      => '<i class="fas fa-power-off"></i>',
                    'offText'     => '<i class="fas fa-power-off"></i>',
                    'onColor'     => 'success',
                    'handleWidth' => 10,
                ],
            ]) ?>
        </div>
    </div>
    <div class="form-actions text-right">
        <?= Html::a(Yii::t('app', 'Close'), null, ['class' => 'btn btn-info float-left', 'data-dismiss' => 'modal']) ?>
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary btn-save-and-continue']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
