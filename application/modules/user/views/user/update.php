<?php
$this->title = Yii::t('app', 'Update user: {name}', ['name' => $model->name]);
?>

<div class="user-update">

    <?=$this->render('_form', [
        'model' => $model,
    ])?>

</div>
