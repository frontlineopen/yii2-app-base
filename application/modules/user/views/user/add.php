<?php
$this->title = Yii::t('app', 'Add user');
?>

<div class="user-add">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
