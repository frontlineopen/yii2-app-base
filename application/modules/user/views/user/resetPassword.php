<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;

$this->context->layout = '//main-full';
$this->title = Yii::t('app', 'Reset password');
?>
<div class="site-reset-password">
    <div class="justify-content-center pt-5">
    <div class="m-auto col-xl-5 col-md-5">
        <div class="card-group">
            <div class="card">
                <div class="bg-primary text-center text-white p-5 rounded-top">
                    <h1><?= Html::encode($this->context->app->name) ?></h1>
                    <p class="text-muted"> <?= Yii::t('app', 'Reset password') ?></p>
                </div>
                <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>
                <div class="card-body">
                    <div class="col-xl-10 col-md-10 mx-auto">
                        <p class="text-muted"><?= Yii::t('app', 'Please fill out the following fields:')?></p>
                        <?= $form->field($model, 'newPassword')->passwordInput() ?>
                        <?= $form->field($model, 'repeatPassword')->passwordInput() ?>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="form-group text-right">
                        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
</div>
