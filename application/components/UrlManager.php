<?php
/**
 *
 * @package     Frontline WebApp Base
 *
 * @subpackage  <URL Manager Extension>
 *
 * @author      Sebastian Costiug <sebastian@frontline.ro>
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @category    BaseObject
 * @see         https://www.yiiframework.com/doc/api/2.0/yii-web-urlmanager
 *
 * @since       2020.07.31
 *
 */

namespace app\components;

use Yii;
use yii\web\UrlManager as BaseUrlManager;

/**
 * Language selector component
 */
class UrlManager extends BaseUrlManager
{
    /**
     * {@inheritdoc}
     * @return mixed
     * @param array $params Parameters for url creation
     */
    public function createUrl($params)
    {
        $language;
        if (!Yii::$app->user->isGuest) {
            $language = Yii::$app->user->getLanguage();
        } elseif (isset(Yii::$app->request->cookies['language'])) {
            $language = Yii::$app->request->cookies['language']->value;
        } else {
            $language = Yii::$app->request->getPreferredLanguage(Yii::$app->params['allowedLanguages']);
        }
        $params['language'] = $language;

        return parent::createUrl($params);
    }
}
