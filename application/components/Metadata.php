<?php
/**
 *
 * @package     Frontline WebApp Base
 *
 * @subpackage  <Metadata Gatherer>
 *
 * @author      Sebastian Costiug <sebastian@frontline.ro>
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @category    components
 * @see         https://www.yiiframework.com/wiki/760/yii-2-0-write-use-a-custom-component-in-yii2-0-advanced-template
 *
 * @since       2020.07.03
 *
 */

namespace app\components;

use Yii;
use yii\base\Component;

/**
 * Metadata class
 */
class Metadata extends Component
{
    /**
     * Parse all folders to find installed modules
     * @return array where the key is the module name and the value is the module absolute path
     */
    public static function getModules()
    {
        $modules    = [];
        $allModules = Yii::$app->modules;
        $appRoot    = Yii::getAlias('@app');
        foreach ($allModules as $module) {
            if (is_array($module)) {
                $moduleBasePath       = (str_replace('\Module', '', $module['class']));
                $moduleName           = (str_replace('app\modules\\', '', $moduleBasePath));
                $moduleBasePath       = (str_replace('app\modules\\', $appRoot . '/modules/', $moduleBasePath));
                $modules[$moduleName] = $moduleBasePath;
            } elseif (is_object($module) && $module->id !== 'debug' && $module->id !== 'gii') {
                $moduleBasePath       = $module->getBasePath();
                $moduleName           = $module->id;
                $moduleBasePath       = (str_replace('app\modules\\', $appRoot . '/modules/', $moduleBasePath));
                $modules[$moduleName] = $moduleBasePath;
            }
            $modules['site'] = $appRoot;
        }

        return $modules;
    }

    /**
     * Parses all controller folders found in each module or in the application root
     * @return array where the key is the module name and the value is an array of found controllers in that module
     */
    public static function getControllers()
    {
        $allControllers = [];
        foreach (self::getModules() as $module) {
            $controllerList = [];
            if (strpos($module, 'application') !== false) {
                if (is_dir($module . DIRECTORY_SEPARATOR . 'controllers') && $handle = opendir($module . DIRECTORY_SEPARATOR . 'controllers')) {
                    while (false !== ($file = readdir($handle))) {
                        if ($file != '.' && $file != '..' && substr($file, strrpos($file, '.') - 10) == 'Controller.php') {
                            $controllerList[] = $file;
                        }
                    }
                    closedir($handle);
                }
            }

            asort($controllerList);
            $allControllers[$module] = $controllerList;
        }

        return $allControllers;
    }

    /**
     * Parses all Controller files found in each module or in application root
     * @return array of arrays where the keys are modules and the values are arrays of controllers
     * Each controller name represents a key for an array of actions available
     */
    public static function getActions()
    {
        $actionList = [];

        foreach (self::getControllers() as $controllerPath => $controller) {
            for ($i = 0; $i < count($controller); $i++) {
                $handle     = fopen($controllerPath . DIRECTORY_SEPARATOR . 'controllers/' . $controller[$i], 'r');
                $modulePath = preg_split('/module(.*?)\(/', $controllerPath, null, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY)[0];
                $appRoot    = Yii::getAlias('@app');
                if (strpos($modulePath, 'modules')) {
                    $moduleName = str_replace($appRoot . '/modules', '', $modulePath);
                } else {
                    $moduleName = str_replace($appRoot, '', $modulePath);
                }
                if ($handle) {
                    while (($codeLine = fgets($handle)) !== false) {
                        if (preg_match('/public function action(.*?)\(/', $codeLine, $matchesArray)) {
                            if (strlen($matchesArray[1]) > 2) {
                                $actionStings = preg_split('#([A-Z][^A-Z]*)#', $matchesArray[1], null, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
                                if (count($actionStings) === 1) {
                                    $action = '';
                                    $action = strtolower($actionStings[0]);

                                    $actionList[lcfirst(substr($controller[$i], 0, -14))][$action] = $moduleName . '/' . lcfirst(substr($controller[$i], 0, -14)) . '/' . $action;
                                } else {
                                    $action = '';
                                    for ($j = 0; $j < count($actionStings); $j++) {
                                        $action .= '-' . strtolower($actionStings[$j]);
                                        if ($j > 0) {
                                            $action = substr($action, 1);
                                        }
                                    }
                                    $actionList[lcfirst(substr($controller[$i], 0, -14))][$action] = $moduleName . '/' . lcfirst(substr($controller[$i], 0, -14)) . '/' . $action;
                                }
                            }
                        }
                    }
                }
                fclose($handle);
            }
        }

        return $actionList;
    }
}
