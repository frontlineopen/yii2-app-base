<?php
/**
 *
 * @package     Frontline WebApp Base
 *
 * @subpackage  <Base Controller>
 *
 * @author      Sorin Pohontu <sorin@frontline.ro> & Sebastian Costiug <sebastian@frontline.ro>
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @category    controllers
 * @see         https://www.yiiframework.com/doc/guide/2.0/en/structure-controllers
 *
 * @since       2020.06.15
 *
 */

namespace app\components;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Cookie;

/**
 * Extension of the base web controller
 */
class BaseController extends \yii\web\Controller
{
    /**
     * Yii app
     * @var array
     */
    public $app;

    /**
     * Shorthand for Yii app user
     * @var array
     */
    public $user;

    /**
     * Current HTML Lang
     * @var string
     */
    public $htmlLang;

    /**
     * Current page MetaDescription
     * @var string
     */
    public $metaDescription;

    /**
     * Current page MetaKeywords
     * @var string
     */
    public $metaKeywords;

    /**
     * Current Body class
     * @var string
     */
    public $bodyClass = 'c-app';

    /**
     * Default layout
     * @var string
     */
    public $layout = '//main';

    /**
     * Static information available in all controllers
     * @var array static info
     */
    public static $data = [
        'poweredBy'    => 'Frontline softworks',
        'poweredByUrl' => 'http://www.frontline.ro',
    ];

    /**
     * Sidebar navigation items
     * @var array context menu items
     */
    public $sideBar = [];

    /**
     * Top navigation items
     * @var array context menu items
     */
    public $actionBar = [];

    /**
     * {@inheritdoc}
     * Orders menu items in the user selected order
     * Sets selected icons to menu items
     * Unsets 'order' and 'icon' keys along with their values from the menu array
     * @return void
     */
    public function init()
    {
        parent::init();

        // Init app
        $this->app       = Yii::$app;
        $this->app->name = $this->app->settings->get('site', 'name');
        $this->user      = $this->app->user;
        $this->htmlLang  = substr($this->app->language, 0, 2);

        // Get all actions
        $actions = Yii::$app->metadata->getActions();

        // Set application language
        if (!Yii::$app->user->isGuest) {
            Yii::$app->language = Yii::$app->user->getLanguage();
        } elseif (Yii::$app->user->isGuest && isset(Yii::$app->request->cookies['language'])) {
            Yii::$app->language = Yii::$app->request->cookies['language']->value;
        } else {
            Yii::$app->language = Yii::$app->request->getPreferredLanguage(Yii::$app->params['allowedLanguages']);
        }

        // Send application language cookie valid for 30 days
        $languageCookie = new Cookie([
            'name'   => 'language',
            'value'  => Yii::$app->language,
            'expire' => time() + 60 * 60 * 24 * 30,
        ]);
        Yii::$app->response->cookies->add($languageCookie);

        $this->sideBar = [
            [
                'label'   => Yii::t('app', 'Dashboard'),
                'url'     => Url::to(['/dashboard/index']),
                'visible' => !Yii::$app->user->isGuest,
                'icon'    => 'fas fa-grip-horizontal',
                'order'   => 1,
            ],
            [
                'label'   => Yii::t('app', 'Modules'),
                'url'     => null,
                'visible' => !Yii::$app->user->isGuest,
                'order'   => 2,
            ],
            [
                'label'   => Yii::t('app', 'Users'),
                'url'     => Url::to(['/user/user/index']),
                'visible' => Yii::$app->user->isAdmin(),
                'icon'    => 'fas fa-users',
                'order'   => 3,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     * @param Action $action The action to be executed.
     * @return mixed
     */
    public function beforeAction($action)
    {
        // Process sideBar items
        if ($this->userOrder($this->sideBar)) {
            usort($this->sideBar, function ($a, $b) {
                return $a['order'] - $b['order'];
            });
        }
        foreach ($this->sideBar as $key => $value) {
            if ($value['url'] !== null) {
                if (isset($value['icon'])) {
                    $iconOptions    = ['class' => $value['icon'] . ' c-sidebar-nav-icon'];
                    $value['label'] = Html::tag('i', '', $iconOptions) . ' ' . $value['label'];
                    unset($value['icon']);
                }
                if (isset($value['order'])) {
                    unset($value['order']);
                }

                // Options for CoreUI sidebar
                $value['options']     = ['class' => 'c-sidebar-nav-item'];
                $value['linkOptions'] = ['class' => 'c-sidebar-nav-link'];
            } else {
                if (isset($value['order'])) {
                    unset($value['order']);
                }

                // Options for CoreUI sidebar
                $value['options'] = ['class' => 'c-sidebar-nav-title'];
            }

            $this->sideBar[$key] = $value;
        }

        // Process actionBar items
        if ($this->userOrder($this->actionBar)) {
            usort($this->actionBar, function ($a, $b) {
                return $a['order'] - $b['order'];
            });
        }
        foreach ($this->actionBar as $key => $value) {
            if (isset($value['icon'])) {
                $iconOptions    = ['class' => $value['icon'] . ' c-sidebar-nav-icon'];
                $value['label'] = Html::tag('i', '', $iconOptions) . ' ' . '<span class="d-sm-down-none">' . $value['label'] . '</span>';
                unset($value['icon']);
            }
            if (isset($value['order'])) {
                unset($value['order']);
            }
            $this->actionBar[$key] = $value;
        }

        return parent::beforeAction($action);
    }

    /**
     * Checks if there is an user defined order to the menu items
     * @param mixed $bar Specify which bar to check for sorting order
     * @return boolean
     */
    public function userOrder($bar)
    {
        foreach ($bar as $item) {
            if (isset($item['order'])) {
                return true;
            }
        }

        return false;
    }

    /**
     * Return static information available in all controllers
     * @param string $param Static info to be retrieved
     * @return string
     */
    public static function getData($param)
    {
        return self::$data[$param];
    }
}
