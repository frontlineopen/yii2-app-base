<?php
$this->title = Yii::t('app', $subject);
$this->params['poweredBy'] = $poweredBy;
$this->params['poweredByUrl'] = $poweredByUrl;
$this->params['preHeadEmail'] = Yii::t('app', 'Thanks for trying out {companyName}. We’ve pulled together some information and resources to help you get started.', ['companyName' => $companyName]);
?>
<table class="email-content" width="100%" cellpadding="0" cellspacing="0" role="presentation" style="width: 570px; -premailer-width: 570px; -premailer-cellpadding: 0; -premailer-cellspacing: 0; margin: 0;">
    <tr>
        <td class="email-masthead" style="word-break: break-word; font-family: &quot;Nunito Sans&quot;, Helvetica, Arial, sans-serif; background-color: #007bff; font-size: 16px; text-align: center; padding: 25px 25px;border-width:0px 50px;"
            align="center">
            <a href="<?= $siteUrl ?>" class="f-fallback email-masthead_name" style="color: black; font-size: 16px; font-weight: bold; text-decoration: none; text-shadow: 0 1px 0 black;">
                <?= $this->title ?>
            </a>
        </td>
    </tr>
<tr>
    <td class="email-body" width="570" cellpadding="0" cellspacing="0"
        style="word-break: break-word; margin: 0; padding: 0; font-family: &quot;Nunito Sans&quot;, Helvetica, Arial, sans-serif; font-size: 16px; width: 100%; -premailer-width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0;">
        <table class="email-body_inner" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation"
            style="width: 570px; -premailer-width: 570px; -premailer-cellpadding: 0; -premailer-cellspacing: 0; background-color: #FFFFFF; margin: 0 auto; padding: 0;" bgcolor="#FFFFFF">
            <tr>
                <td class="content-cell" style="word-break: break-word; font-family: &quot;Nunito Sans&quot;, Helvetica, Arial, sans-serif; font-size: 16px; padding: 45px;">
                    <div class="f-fallback">
                        <h1 style="margin-top: 0; color: #333333; font-size: 22px; font-weight: bold; text-align: left;" align="left"><?= Yii::t('app', 'Hello {name}', ['name' => $name]) ?>,</h1>
                        <p style="font-size: 16px; line-height: 1.625; color: #51545E; margin: .4em 0 1.1875em;"><?= Yii::t('app', 'Welcome in your account! ') ?></p>
                        <table class="body-action" align="center" width="100%" cellpadding="0" cellspacing="0" role="presentation"
                            style="width: 100%; -premailer-width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; text-align: center; margin: 30px auto; padding: 0;">
                            <tr>
                                <td align="center" style="word-break: break-word; font-family: &quot;Nunito Sans&quot;, Helvetica, Arial, sans-serif; font-size: 16px;">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" role="presentation">
                                        <tr>
                                            <td align="center" style="word-break: break-word; font-family: &quot;Nunito Sans&quot;, Helvetica, Arial, sans-serif; font-size: 16px;">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <p style="font-size: 16px; line-height: 1.625; color: #51545E; margin: .4em 0 1.1875em;"><?= Yii::t('app', 'You can login on your account using the following credentials: ') ?> </p>
                        <p style="font-size: 18px; font-weight: 700; line-height: 1.625; color: #51545E; margin: .4em 0 1.1875em;"><?= Yii::t('app', 'Email: {email}',['email' =>$email]) ?></p>
                        <p style="font-size: 18px; font-weight: 700; line-height: 1.625; color: #51545E; margin: .4em 0 1.1875em;"><?= Yii::t('app', 'Password: {password}', ['password' => $password]) ?></p>
                        <p style="font-size: 16px; line-height: 1.625; color: #51545E; margin: .4em 0 1.1875em;"><?= Yii::t('app', 'Regards, ') ?></p>
                        <p style="font-size: 16px; line-height: 1.625; color: #51545E; margin: .4em 0 1.1875em;"><?= $appName ?></p>
                    </div>
                </td>
            </tr>
        </table>
    </td>
</tr>
</table>
