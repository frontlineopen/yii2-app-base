<?php
$this->title = Yii::t('app', $subject);
$this->params['poweredBy'] = $poweredBy;
$this->params['poweredByUrl'] = $poweredByUrl;
$this->params['companyName'] = $companyName;
$this->params['preHeadEmail'] = Yii::t('app', 'We received a request to reset your password with this email address.');
?>
<table class="email-content" width="100%" cellpadding="0" cellspacing="0" role="presentation" style="width: 570px; -premailer-width: 570px; -premailer-cellpadding: 0; -premailer-cellspacing: 0; margin: 0;">
    <tr>
        <td class="email-masthead" style="word-break: break-word; font-family: &quot;Nunito Sans&quot;, Helvetica, Arial, sans-serif; background-color: #007bff; font-size: 16px; text-align: center; padding: 25px 25px;border-width:0px 50px;"
            align="center">
            <a href="<?= $siteUrl ?>" class="f-fallback email-masthead_name" style="color: black; font-size: 16px; font-weight: bold; text-decoration: none; text-shadow: 0 1px 0 black;">
                <?= $this->title ?>
            </a>
        </td>
    </tr>
<tr>
    <td class="email-body" width="570" cellpadding="0" cellspacing="0"
        style="word-break: break-word; margin: 0; padding: 0; font-family: &quot;Nunito Sans&quot;, Helvetica, Arial, sans-serif; font-size: 16px; width: 100%; -premailer-width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0;">
        <table class="email-body_inner" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation"
            style="width: 570px; -premailer-width: 570px; -premailer-cellpadding: 0; -premailer-cellspacing: 0; background-color: #FFFFFF; margin: 0 auto; padding: 0;" bgcolor="#FFFFFF">
            <tr>
                <td class="content-cell" style="word-break: break-word; font-family: &quot;Nunito Sans&quot;, Helvetica, Arial, sans-serif; font-size: 16px; padding: 45px;">
                    <div class="f-fallback">
                        <h1 style="margin-top: 0; color: #333333; font-size: 22px; font-weight: bold; text-align: left;" align="left"><?= Yii::t('app', 'Hello {name}', ['name' => $name]) ?>,</h1>
                        <p style="font-size: 16px; line-height: 1.625; color: #51545E; margin: .4em 0 1.1875em;"><?= Yii::t('app', 'You are receiving this email because we received a password request for your account.') ?></p>
                        <table class="body-action" align="center" width="100%" cellpadding="0" cellspacing="0" role="presentation"
                            style="width: 100%; -premailer-width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; text-align: center; margin: 30px auto; padding: 0;">
                            <tr>
                                <td align="center" style="word-break: break-word; font-family: &quot;Nunito Sans&quot;, Helvetica, Arial, sans-serif; font-size: 16px;">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" role="presentation">
                                        <tr>
                                            <td align="center" style="word-break: break-word; font-family: &quot;Nunito Sans&quot;, Helvetica, Arial, sans-serif; font-size: 16px;">
                                                <a href="<?=  $resetLink ?>" class="f-fallback button button--green" target="_blank"
                                                    style="color: #FFF; border-color: #22bc66; border-style: solid; border-width: 10px 18px; background-color: #22BC66; display: inline-block; text-decoration: none; border-radius: 3px; box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16); -webkit-text-size-adjust: none; box-sizing: border-box;">
                                                    <?= Yii::t('app', 'Click here to Reset Password') ?></a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <p style="font-size: 16px; line-height: 1.625; color: #51545E; margin: .4em 0 1.1875em;"><?= Yii::t('app', 'If you did not request this, you can ignore this email. Your password won\'t change until you create a new password. ') ?></p>
                        <p style="font-size: 16px; line-height: 1.625; color: #51545E; margin: .4em 0 1.1875em;"><?= Yii::t('app', 'Regards, ') ?>
                            <br /><?=  $appName ?></p>
                        <table class="body-sub" role="presentation" style="margin-top: 25px; padding-top: 25px; border-top-width: 1px; border-top-color: #EAEAEC; border-top-style: solid;">
                            <tr>
                                <td style="word-break: break-word; font-family: &quot;Nunito Sans&quot;, Helvetica, Arial, sans-serif; font-size: 16px;">
                                    <p class="f-fallback sub" style="font-size: 13px; line-height: 1.625; color: #51545E; margin: .4em 0 1.1875em;">
                                        <?= Yii::t('app', 'If you’re having trouble with the button above, copy and paste the URL below into your web browser.') ?></p>
                                    <p class="f-fallback sub" style="font-size: 13px; line-height: 1.625; color: #51545E; margin: .4em 0 1.1875em;"><a><?= $resetLink ?></a></p>
                                </td>
                            </tr>
                        </table>
                </td>
            </tr>
        </table>
        </div>
    </td>
</tr>
</table>
