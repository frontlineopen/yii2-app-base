<?php
/**
 *
 * @package     Frontline WebApp Base
 *
 * @subpackage  <Shell Commands Controller>
 *
 * @author      Sebastian Costiug <sebastian@frontline.ro>
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @category    controllers
 * @see         http://www.yiiframework.com/doc-2.0/guide-structure-controllers.html
 *
 * @since       2020.06.15
 *
 */

namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use app\helpers\FileOps;

/**
 * Shell Controller class
 */
class ShellController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * Purpose: test the functionality of the ShellController
     * @param string $message Message to be echoed
     * @return mixed
     */
    public function actionIndex($message = 'ShellController Working')
    {
        echo $message . "\n";
        return ExitCode::OK;
    }

    /**
     * Shell Command for all migration paths in /modules folder
     * @return void
     */
    public function actionMigrateUp()
    {
        \Yii::$app->controllerMap['migrate']['migrationPath'] = FileOps::migrationPaths();
        \Yii::$app->runAction('migrate/up', ['interactive'   => true,]);
    }
}
