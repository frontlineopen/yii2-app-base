<?php
/**
 * @package     yii2-app-base
 * @subpackage  Clean Vendors
 * @author      Laurentiu Vizitiu <laurentiu@frontline.ro>
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @category    commands
 * @since       2020.06.29
 */

namespace app\commands;

use yii\console\Controller;
use yii\console\Exception;
use yii\helpers\FileHelper;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Clean Vendors.
 * Run from console: `php application/console clean-vendors`
 */
class CleanVendorsController extends Controller
{
    /**
     * @var boolean Simulate but do not delete files
     */
    public $dryRun    = false;

    /**
     * @var boolean be silent, do not show status messages in output
     */
    public $silent    = false;

    /**
     * @var array $rules
     */
    public $rules     = [];

    /**
     * {@inheritdoc}
     */
    public function options($actionID)
    {
        return ([
            'help',
            'dryRun',
            'silent',
        ]);
    }

    /**
     * Remove unneeded files from composer's packages
     * @return boolean
     */
    public function actionIndex()
    {
        return ($this->cleanPackages() === false) ? 1 : 0;
    }

    /**
    * Get rules
    * @return array
    */
    public static function getRules()
    {
        // Default patterns for common files
        $docs      = 'AUTHORS.txt README* CHANGELOG* CHANGELOG* FAQ* CONTRIBUTING* HISTORY* UPGRADING* UPGRADE* package* doc docs readme*';
        $examples  = 'demo example examples';
        $git       = '.git .github .gitattributes .gitignore';
        $sys       = '.DS_Store .editorconfig .eslintrc .php_cs.dist';
        $tests     = '.travis.yml .scrutinizer.yml phpunit.xml* phpunit.php test tests Tests travis psalm.xml psalm.baseline.xml';
        $languages = 'messages/af messages/ar messages/az messages/be messages/bg messages/bs messages/ca messages/cs messages/da messages/de messages/el messages/es messages/et messages/fa messages/fi messages/fr messages/he messages/hi messages/hr messages/hu messages/hy messages/id messages/it messages/ja messages/ka messages/kk messages/ko messages/kz messages/lt messages/lv messages/ms messages/nb-NO messages/nl messages/pl messages/pt messages/pt-BR messages/ru messages/sk messages/sl messages/sr messages/sr-Latn messages/sv messages/tg messages/th messages/tr messages/uk messages/uz messages/vi messages/zh-CN messages/zh-TW';
        $languagesSrc = 'src/messages/ar src/messages/az src/messages/bg src/messages/bs src/messages/ca src/messages/cs src/messages/da src/messages/de src/messages/el src/messages/eo src/messages/es src/messages/et src/messages/eu src/messages/fa src/messages/fi src/messages/fo src/messages/fr src/messages/gl src/messages/gu src/messages/he src/messages/hi src/messages/hr src/messages/hu src/messages/hy src/messages/id src/messages/is src/messages/it src/messages/ja src/messages/ka src/messages/kk src/messages/kn src/messages/ko src/messages/ky src/messages/lt src/messages/lv src/messages/mi src/messages/mk src/messages/mn src/messages/mr src/messages/ms src/messages/mt src/messages/nb src/messages/nl src/messages/nn src/messages/pl src/messages/pt src/messages/pt-BR src/messages/qu src/messages/ru src/messages/sa src/messages/se src/messages/sk src/messages/sl src/messages/sq src/messages/sr src/messages/sr-Latn src/messages/sv src/messages/sw src/messages/ta src/messages/te src/messages/th src/messages/tj src/messages/tl src/messages/tn src/messages/tr src/messages/ts src/messages/tt src/messages/uk src/messages/ur src/messages/vi src/messages/zh-CN src/messages/zh-TW src/messages/zu';

        return [
            'default'                   => [$docs, $examples, $git, $sys, $tests, ],

            // Bower
            'bower/jquery'              => [$docs, $examples, $git, $sys, $tests, 'external src'],

            // NPM
            'npm/bootstrap'             => [$docs, $examples, $git, $sys, $tests, 'js* scss*'],

            // Yiisoft
            'yiisoft/yii2'              => [$docs, $examples, $git, $sys, $languages],

            // Select2
            'select2/select2'           => [$docs, $examples, $git, $sys, $tests, 'src .jshint*'],

            // Kartik
            'kartik-v/yii2-dialog'      => [$docs, $examples, $git, $sys, $tests, $languagesSrc],
            'kartik-v/yii2-grid'        => [$docs, $examples, $git, $sys, $tests, $languagesSrc],
            'kartik-v/yii2-krajee-base' => [$docs, $examples, $git, $sys, $tests, $languagesSrc],
            'kartik-v/yii2-widget-datepicker' => [$docs, $examples, $git, $sys, $tests, $languagesSrc],
            'kartik-v/yii2-widget-fileinput'  => [$docs, $examples, $git, $sys, $tests, $languagesSrc],
            'kartik-v/yii2-widget-select2'    => [$docs, $examples, $git, $sys, $tests, $languagesSrc],
        ];
    }

    /**
     * cleanPackages
     * @return boolean
     */
    public function cleanPackages()
    {
        $this->showMessage('Vendor path: ' . Yii::getAlias('@vendor') . DIRECTORY_SEPARATOR . ' ... ');

        $composerInstalledFile = Yii::getAlias('@vendor') . DIRECTORY_SEPARATOR . 'composer' . DIRECTORY_SEPARATOR . 'installed.json';
        if (!is_file($composerInstalledFile)) {
            $this->showMessage('Composer installed file [' . $composerInstalledFile . ' ] does not exist');
            return false;
        }
        $composerInstalled = file_get_contents($composerInstalledFile);
        $packages = json_decode($composerInstalled, true);

        $this->rules = ArrayHelper::merge(
            $this->rules,
            $this->getRules()
        );

        $nbrCleaned = 0;
        $nbrChecked = 0;
        $nbrDefault = 0;

        foreach ($packages as $package) {
            // Invalid package name
            if (empty($package['name'])) {
                continue;
            }

            $this->showMessage('Processing : ' . $package['name'] . ' ...');

            // No rule for package, use default rules
            if (!isset($this->rules[$package['name']])) {
                $this->rules[$package['name']] = $this->rules['default'];
                $nbrDefault++;
            } else {
                $nbrChecked++;
            }

            $cleaned = $this->cleanPackage($package['name']);
            if ($cleaned !== false) {
                $nbrCleaned += $cleaned;
            }

            $nbrChecked++;
        }

        $nbrCustom = ($nbrChecked - $nbrDefault);
        $this->showMessage('Scanned ' . $nbrChecked . ' packages' . ($nbrChecked == 1 ? '' : 's') . '; used ' . $nbrCustom . ' custom rule' . ($nbrCustom == 1 ? '' : 's') . '; used default rule ' . $nbrDefault . ' times');
        $this->showMessage('Done. Cleaned ' . $nbrCleaned . ' files/dirs');
    }

    /**
     * Rename a package
     * @param  string $package Name package
     * @return string renamed package
     */
    protected function renamePackage($package)
    {
        if (preg_match('/bower-asset/i', $package)) {
            $package = str_replace('bower-asset', 'bower', $package);
        } elseif (preg_match('/npm-asset/i', $package)) {
            $package = str_replace('npm-asset', 'npm', $package);
        }

        return $package;
    }

    /**
     * Clean package, based on its own rules or default.
     * @param string $package Package name
     * @return boolean
     */
    protected function cleanPackage($package)
    {
        // `bower` and `npm`: `name` (bower-asset/jquery) is different than package folder `bower/jquery)
        $packageFolder = $this->renamePackage($package);
        $dir = Yii::getAlias('@vendor') . DIRECTORY_SEPARATOR . $packageFolder;

        if (!is_dir($dir)) {
            return false;
        }

        if ($this->dryRun) {
            $msgTry = 'would have ';
        } else {
            $msgTry = '';
        }

        $nbrCleaned = 0;
        foreach ((array) $this->rules[$package] as $part) {
            // Split patterns for single globs (should be max 260 chars)
            $patterns = explode(' ', trim($part));

            foreach ($patterns as $pattern) {
                try {
                    $files = glob($dir . DIRECTORY_SEPARATOR . $pattern);
                    $nbrFiles = count($files);
                    $this->showMessage('checking ' . $nbrFiles . ' result' . ($nbrFiles == 1 ? '' : 's') . ' in ' . $dir . DIRECTORY_SEPARATOR . $pattern, false);

                    foreach (glob($dir . DIRECTORY_SEPARATOR . $pattern) as $file) {
                        if (is_dir($file)) {
                            $this->showMessage($msgTry . 'removed dir : ' . $file);

                            if (!$this->dryRun) {
                                FileHelper::removeDirectory($file);
                            }
                        } else {
                            $this->showMessage($msgTry . 'removed file: ' . $file);

                            if (!$this->dryRun) {
                                unlink($file);
                            }
                        }
                        $nbrCleaned++;
                    }
                } catch (\Exception $e) {
                    $this->showMessage('Could not parse [' . $dir . DIRECTORY_SEPARATOR . $pattern . ']: ' . $e->getMessage());
                }
            }
        }
        return ($nbrCleaned > 0);
    }

    /**
     * showMessage
     * @param  string  $msg  Message
     * @param  boolean $show Set visible message
     * @return void
     */
    private function showMessage($msg, $show = true)
    {
        if (!$this->silent && $show) {
            echo $msg . "\n";
        }
    }
}
