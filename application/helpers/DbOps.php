<?php
/**
 *
 * @package     Frontline WebApp Base
 *
 * @subpackage  <Database Operation helpers>
 *
 * @author      Sebastian Costiug <sebastian@frontline.ro>
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @category    helpers
 *
 * @since       2020.06.15
 *
 */

namespace app\helpers;

use Yii;

/**
 * DataBase operation class
 */
class DbOps
{
    /**
     * Modifies SQL DataBase table entries
     * @param string $table  The table in which to write the change
     * @param string $column The column in which to write the change
     * @param string $value  The value to be written
     * @param string $id     The row ID in which to write the change
     * @return mixed
     */
    public function modifyTableEntry($table, $column, $value, $id)
    {
        return Yii::$app->db
            ->createCommand("UPDATE $table SET $column=$value WHERE id=$id")
            ->execute();
    }

    /**
     * Web option to use migration on all paths in /modules folder
     * @return mixed
     */
    public static function migrateUp()
    {
        $oldApp = \Yii::$app;
        $config = require dirname(__DIR__) . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'console.php';
        new \yii\console\Application($config);
        \ob_start();
        \Yii::$app->controllerMap['migrate']['migrationPath'] = FileOps::migrationPaths();
        \Yii::$app->runAction('migrate/up', ['interactive' => false]);
        \Yii::$app              = $oldApp;
        $migrationOutputContent = ob_get_clean();
        $migrationOutputFile    = Yii::getAlias('@runtime') . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'migration.log';
        file_put_contents($migrationOutputFile, $migrationOutputContent, FILE_APPEND);

        return true;
    }
}
