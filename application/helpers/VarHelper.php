<?php
/**
 *
 * @package     Frontline WebApp Base
 *
 * @subpackage  <Various Operation helpers>
 *
 * @author      Sebastian Costiug <sebastian@frontline.ro>
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @category    helpers
 *
 * @since       2020.06.15
 *
 */

namespace app\helpers;

use Yii;
use yii\helpers\FileHelper;
use yii\helpers\Url;

/**
 * Variable Operations Class
 */
class VarHelper
{
    /**
     * In case the app has a user module the function returns the ID of the logged user
     * If the user is not logged it returns 1.
     * If the app doesn't have a user module it returns 1.
     * @return integer
     */
    public static function getUserId()
    {
        $userId = 1;
        if (Yii::$app->has('user')) {
            $userId = Yii::$app->get('user')->id;
            if ($userId === null) {
                return 1;
            }
        }
        return $userId;
    }

    /**
     * Sends confirmation email to user.
     * @param mixed  $name    Recipient's name.
     * @param mixed  $email   Recipient's email address.
     * @param string $subject Email subject.
     * @param string $view    View file to render in the email body.
     * @param array  $params  Appendable array of parameters that will be received in view as individual variables.
     * @return boolean Whether the email was sent.
     */
    public static function sendEmail($name, $email, $subject, $view, $params)
    {
        //Add data to params
        $params['subject']      = $subject;
        $params['name']         = $name;
        $params['email']        = $email;
        $params['siteUrl']      = Yii::$app->urlManager->createAbsoluteUrl(['/site/index']);
        $params['appName']      = Yii::$app->settings->get('site', 'name');
        $params['companyName']  = Yii::$app->settings->get('company', 'name');
        $params['poweredBy']    = 'Frontline softworks';
        $params['poweredByUrl'] = 'http://www.frontline.ro';

        return Yii::$app
            ->mailer
            ->compose(['html' => $view], $params)
            ->setReturnPath(Yii::$app->settings->get('mail', 'replyTo'))
            ->setFrom([Yii::$app->settings->get('mail', 'fromEmail') => Yii::$app->settings->get('site', 'name') . ' notifications'])
            ->setTo($email)
            ->setSubject($subject . ' @ ' . $params['appName'])
            ->send();
    }

    /**
     * Get Current domain
     * @return  string
     */
    public static function getCurrentDomain()
    {
        $host = Url::base(true);
        $url  = @parse_url($host);
        if (empty($url['host'])) {
            return false;
        }

        $parts = explode('.', $url['host']);
        $slice = (strlen(reset(@array_slice($parts, -2, 1))) == 2) && (count($parts) > 2) ? 3 : 2;
        return implode('.', array_slice($parts, (0 - $slice), $slice));
    }

    /**
     * Get Current Subdomain, if exists
     * @return  string
     */
    public static function getCurrentSubDomain()
    {
        $host = Url::base(true);
        $url  = @parse_url($host);
        if (empty($url['host'])) {
            return false;
        }

        $parts = explode('.', $url['host']);
        if ($parts[0] != 'www') {
            return $parts[0];
        } else {
            return false;
        }
    }

    /**
     * Sends confirmation email to user.
     * @param integer $length Generated string length.
     * @return boolean Whether the email was sent.
     */
    public static function generateRandomString($length)
    {
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $string     = [];
        $charLength = strlen($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $n        = rand(0, $charLength);
            $string[] = $characters[$n];
        }
        return implode($string);
    }

    /**
     * Uploads a file and saves it to a specified folder
     * @param string $file   The file name to be uploaded
     * @param string $folder Optional parameter to specify a folder where the file should be uploaded
     * If $folder parameter is missing the file will be uploaded in the @uploads folder
     * @return string
     */
    public static function uploadFile($file, $folder = null)
    {
        $path = Yii::getAlias('@uploads') . DIRECTORY_SEPARATOR . $folder;
        if (!file_exists($path) && !is_dir($path)) {
            FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
        }
        do {
            $fileName = VarHelper::generateRandomString(64) . '.' . $file->extension;
        } while (file_exists($path . DIRECTORY_SEPARATOR . $folder . DIRECTORY_SEPARATOR . $fileName));

        if ($file->saveAs($path . DIRECTORY_SEPARATOR . $fileName)) {
            return $fileName;
        }
        return null;
    }

    /**
     * Deletes a file from a specified folder
     * @param string $file   The file name to be deleted
     * @param string $folder Optional parameter to specify a folder where the file that should be deleted is stored
     * If $folder parameter is missing the file will be assumed to be in the @uploads folder
     * @return void
     */
    public static function deleteFile($file, $folder = null)
    {
        $path = Yii::getAlias('@uploads') . DIRECTORY_SEPARATOR . $folder;
        FileHelper::unlink($path . DIRECTORY_SEPARATOR . $file);
    }
}
