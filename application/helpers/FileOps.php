<?php
/**
 *
 * @package     Frontline WebApp Base
 *
 * @subpackage  <File Operation helpers>
 *
 * @author      Sebastian Costiug <sebastian@frontline.ro>
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @category    helpers
 *
 * @since       2020.06.15
 *
 */

namespace app\helpers;

use Yii;
use yii\helpers\FileHelper;

/**
 * File Operations Class
 */
class FileOps
{
    /**
     * Parse /modules folders, find installed modules
     * return an array of existing /migrations paths
     * @return array
     */
    public static function migrationPaths()
    {
        $appRoot          = Yii::getAlias('@app');
        $modulesTree      = FileHelper::findDirectories("$appRoot/modules", ['recursive' => true]);
        $moduleMigrations = [
            '@app/migrations',
        ];
        foreach ($modulesTree as $moduleFolder) {
            if (strpos($moduleFolder, 'migrations')) {
                $modulePath = str_replace($appRoot, '@app', $moduleFolder);
                $moduleMigrations[] = $modulePath;
            }
        }
        return $moduleMigrations;
    }
}
