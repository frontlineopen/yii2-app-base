<?php

use yii\helpers\Url;

$this->title = Yii::t('app', 'Error');
$this->context->layout = '//main-full';
?>
<div class="card mt-4">
    <div class="bg-primary text-center text-white p-5 rounded-top">
        <h1><?= $this->context->app->name ?></h1>
        <p class="text-muted"> <?= Yii::t('app', 'That\'s an error') ?></p>
    </div>
    <div class="jumbotron mb-0">
            <h2 class="display-4"><?= Yii::t('app', 'Error') . ' ' . Yii::$app->response->statusCode ?></h2>
            <p class="lead"><?= Yii::t('app', 'Something is wrong here') ?></p>
            <hr class="my-4">
            <pre><?= $message ?></pre>
            <div class="card-footer">
                <a class="btn btn-primary btn-lg" href="<?= Url::to(['site/index'])?>" role="button"><?= Yii::t('app', 'Back to site index') ?></a>
            </div>
    </div>
</div>
