<?php

use app\modules\user\models\User;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Dashboard');

?>
<?php if (!Yii::$app->user->isGuest) { ?>
<div class="row">

    <div class="col-xs-6 col-lg-4">
        <div class="card text-white shadow bg-gradient-warning">
            <div class="card-body card-body pb-0 d-flex justify-content-between align-items-start">
                <div>
                    <div class="text-value-lg"><?php echo $dataProvider['totalUsers'] ?></div>
                    <div>Total users</div>
                </div>
                <?php if (Yii::$app->user->isAdmin()) { ?>
                <div class="btn-group">
                    <button class="btn btn-transparent p-0" type="button" data-toggle="" aria-haspopup="true" aria-expanded="false">
                        <a class="btn btn-warning shadow-sm text-white" href="<?= Url::to(['/user/user/index', 'active' => '', 'sort' => 'id']) ?>"><i class="c-icon mfe-2 fas fa-users"></i><?= Yii::t('app', 'All users') ?></a>
                    </button>
                </div>
                <?php }?>
            </div>
            <div class="c-chart-wrapper mt-3 mx-3" style="height:70px;">
                <div class="chartjs-size-monitor">
                    <div class="row chartjs-size-monitor-expand mx-2">
                        <div class="h1 text-muted"><i class="fas fa-users"></i></div>
                        <div class="h2 text-muted"><i class="fas fa-user-friends"></i></div>
                        <div class="h3 text-muted"><i class="fas fa-users"></i></div>
                    </div>
                </div>
                <canvas class="chart chartjs-render-monitor" id="card-chart4" height="70" style="display: block; width: 254px; height: 70px;" width="254"></canvas>
            </div>
        </div>
    </div>

    <div class="col-xs-6 col-lg-4">
        <div class="card text-white shadow bg-gradient-danger">
            <div class="card-body card-body pb-0 d-flex justify-content-between align-items-start">
                <div>
                    <div class="text-value-lg"><?php echo $dataProvider['inactive'] ?></div>
                    <div>Inactive users</div>
                </div>
                <?php if (Yii::$app->user->isAdmin()) { ?>
                <div class="btn-group">
                    <button class="btn btn-transparent p-0" type="button" data-toggle="" aria-haspopup="true" aria-expanded="false">
                        <a class="btn btn-danger shadow-sm text-white" href="<?= Url::to(['/user/user/index', 'active' => 0, 'sort' => 'id']) ?>"><i class="c-icon mfe-2 fas fa-users"></i><?= Yii::t('app', 'Inactive users') ?></a>
                    </button>
                </div>
                <?php } ?>
            </div>
            <div class="c-chart-wrapper mt-3 mx-3" style="height:70px;">
                <div class="chartjs-size-monitor">
                    <div class="chartjs-size-monitor-expand">
                        <div class="">
                            <?php if (isset($dataProvider['lastInactive'])) { ?>
                                <?php foreach ($dataProvider['lastInactive'] as $entry) { ?>
                                    <div class="text-muted"><?php echo $entry->name ?></div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <canvas class="chart chartjs-render-monitor" id="card-chart4" height="70" style="display: block; width: 254px; height: 70px;" width="254"></canvas>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-lg-4">
        <div class="card text-white shadow bg-gradient-info">
            <div class="card-body card-body pb-0 d-flex justify-content-between align-items-start">
                <div>
                    <div class="text-value-lg">LATEST</div>
                    <div>Registered users</div>
                </div>
                <?php if (Yii::$app->user->isAdmin()) { ?>
                <div class="btn-group">
                    <button class="btn btn-transparent p-0" type="button" data-toggle="" aria-haspopup="true" aria-expanded="false">
                        <a class="btn btn-info shadow-sm text-white" href="<?= Url::to(['/user/user/index', 'active' => '', 'sort' => '-id']) ?>"><i class="c-icon mfe-2 fas fa-users"></i><?= Yii::t('app', 'Latest users') ?></a>
                    </button>
                </div>
                <?php } ?>
            </div>
            <div class="c-chart-wrapper mt-3 mx-3" style="height:70px;">
                <div class="chartjs-size-monitor">
                    <div class="">
                        <?php if (isset($dataProvider['lastRegistered'])) { ?>
                            <?php foreach ($dataProvider['lastRegistered'] as $entry) { ?>
                                <div class="text-muted"><?php echo $entry->name ?></div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
                <canvas class="chart chartjs-render-monitor" id="card-chart4" height="70" style="display: block; width: 254px; height: 70px;" width="254"></canvas>
            </div>
        </div>
    </div>
</div>

    <?php if (Yii::$app->user->isAdmin()) { ?>
    <div class="card">
        <table class="table table-responsive-sm table-hover table-outline shadow mb-0">
            <thead class="thead-light">
                <tr>
                    <th class="text-center"></th>
                    <th class="text-left">User</th>
                    <th class="text-center">Message</th>
                    <th class="text-right">Timestamp</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($dataProvider['audit'] as $entry) { ?>
                <tr>
                    <td class="text-center">
                        <div class="c-avatar"><img class="c-avatar-img" src="<?= User::getAvatarUrlByID($entry->created_by) ?>" alt="<?= $entry->created_by_name ?>"></span></div>
                    </td>
                    <td class="text-left">
                        <div><?= $entry->created_by_name ?></div>
                        <div class="small text-muted"><?= $entry->action ?></div>
                    </td>
                    <td class="text-left">
                        <div class="clearfix">
                            <div class="float-left"><strong><?= $entry->message ?></strong></div>
                        </div>
                    </td>
                    <td class="text-right">
                        <div class="small text-muted"></div><strong><?= $entry->created ?></strong>
                    </td>
                </tr>
                <?php } ?>
                <tfoot class="thead-light">
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th class="text-right c-header-nav-item"><a class="btn btn-dark text-light" href=<?= Url::to(['/log/audit/index']) ?>><i class="c-icon mfe-2 fas fa-fingerprint"></i><?= Yii::t('app', 'Audit log') ?></a></th>
                </tr>
            </tfoot>

            </tbody>
        </table>
    </div>
    <?php } ?>

<?php } else { ?>
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="clearfix">
                <h1 class="float-left display-3 mr-3">403</h1>
                <h4 class="pt-3"><?=Yii::t('app', 'Forbidden')?></h4>
                <p class="text-muted"><?= Yii::t('app', 'You need to login for this resources.') ?></p>
            </div>
        </div>
<?php } ?>
