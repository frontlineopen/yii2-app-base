<?php
/**
 *
 * @package     Views
 * @subpackage  Layouts / Blocks
 * @author      Sorin Pohontu <sorin@frontline.ro>
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 * @since       2020.07.21
 *
 */

use kartik\select2\Select2;
use kartik\switchinput\SwitchInput;
use yii\bootstrap4\Modal;
use yii\bootstrap4\Nav;
use yii\helpers\Html;
use yii\helpers\Url;

kartik\select2\Select2Asset::register($this);
kartik\switchinput\SwitchInputAsset::register($this);

?>
<?php $this->beginContent('@app/views/layouts/main-frame.php'); ?>
<?php Modal::begin([
    'id'            => 'activity-modal',
    'title'         => '&nbsp;',
    'size'          => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static'],
    'options'       => [
        'tabindex' => false,
    ],
]);?>
<?php Modal::end(); ?>
<div id="sidebar" class="c-sidebar c-sidebar-dark c-sidebar-fixed c-sidebar-lg-show">
    <div class="c-sidebar-brand d-md-down-none">
        <?= $this->context->app->name ?>
    </div>
    <?= Nav::widget([
        'encodeLabels' => false,
        'options'      => [
            'class' => [
                'widget' => 'c-sidebar-nav',
            ],
        ],
        'items'        => $this->context->sideBar,
    ]) ?>
    <a class="c-sidebar-minimizer c-sidebar-nav-link nav-link" href="<?= Url::to(['/user/user/logout'])?>"> <?= Yii::t('app', 'Logout') ?></a>
</div>
<div class="c-wrapper">
    <header class="c-header c-header-light c-header-fixed c-header-with-subheader">
        <button class="c-header-toggler c-class-toggler mfs-3 d-md-down-none" type="button" data-target="#sidebar" data-class="c-sidebar-lg-show" responsive="true">
            <i class="fas fa-bars"></i>
        </button>
        <button class="c-header-toggler c-class-toggler d-lg-none mfe-auto" type="button" data-target="#sidebar" data-class="c-sidebar-show">
            <i class="fas fa-bars"></i>
        </button>

        <div class="c-header-nav ml-4"><?= Html::tag('h1', Yii::t('app', $this->title)) ?></div>

        <ul class="c-header-nav ml-auto mr-4">
            <li class="c-header-nav-item dropdown">
                <a class="c-header-nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <div class="c-avatar">
                        <?= Html::img(Yii::$app->user->getAvatarUrl(), ['class' => 'c-avatar-img', 'alt' => Yii::t('app', 'User menu')]) ?>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-right pt-0">
                    <div class="dropdown-header bg-light py-2">
                        <strong><?= Yii::t('app', 'User menu') ?></strong>
                    </div>
                    <a class="dropdown-item" href="<?= Url::to(['/user/user/profile']) ?>"><i class="c-icon mfe-2 fas fa-user"></i> <?=Yii::t('app', 'My profile')?></a>
                    <a class="dropdown-item" href="<?= Url::to(['/user/user/change-password']) ?>"><i class="c-icon mfe-2 fas fa-key"></i> <?= Yii::t('app', 'Change password') ?></a>
                    <?php if (Yii::$app->user->isAdmin()) {?>
                    <div class="dropdown-header bg-light py-2">
                        <strong><?= Yii::t('app', 'Administrator') ?></strong>
                    </div>
                    <a class="dropdown-item" href="<?= Url::to(['/settings/settings/index']) ?>"><i class="c-icon mfe-2 fas fa-tools"></i> <?= Yii::t('app', 'Settings') ?></a>
                    <a class="dropdown-item" href="<?= Url::to(['/log/audit/index']) ?>"><i class="c-icon mfe-2 fas fa-fingerprint fa-fw"></i> <?= Yii::t('app', 'Audit') ?></a>
                    <a class="dropdown-item" href="<?= Url::to(['/log/log/view-application-log']) ?>"><i class="c-icon mfe-2 fas fa-scroll fa-fw"></i> <?= Yii::t('app', 'Logs') ?></a>
                    <?php }?>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="<?= Url::to(['/user/user/logout'])?> "><i class="c-icon mfe-2 fas fa-sign-out-alt"></i> <?= Yii::t('app', 'Logout') ?></a>
                </div>
            </li>
        </ul>
    </header>
    <div class="c-body">
        <main class="c-main">
            <div class="container-fluid h-100">
                <div class="ui-view h-100">
                    <div class="h-100">
                        <?= $content ?>
                    </div>
                </div>
            </div>
        </main>
    </div>
    <footer id="footer" class="c-footer">
        <div>
            <a href="<?= Url::to(['/site/index']) ?>"><?= $this->context->app->name ?></a>
        </div>
        <div class="mfs-auto">
            Powered by <a href="https://www.frontline.ro">Frontline softworks</a>
        </div>
    </footer>
</div>
<?php $this->endContent()?>
