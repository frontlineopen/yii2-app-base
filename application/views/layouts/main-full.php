<?php
/**
 *
 * @package     Views
 * @subpackage  Layouts / Blocks
 * @author      Sorin Pohontu <sorin@frontline.ro>
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 * @since       2020.07.21
 *
 */

$this->beginContent('@app/views/layouts/main-frame.php'); ?>
<div class="container">
    <?= $content ?>
</div>
<?php $this->endContent() ?>
