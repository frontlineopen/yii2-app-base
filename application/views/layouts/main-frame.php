<?php
/**
 *
 * @package     Views
 * @subpackage  Layouts / Blocks
 * @author      Sorin Pohontu <sorin@frontline.ro>
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 * @since       2020.07.21
 *
 */

use app\assets\AppAsset;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\dialog\Dialog;
use kartik\base\AnimateAsset;

AppAsset::register($this);
kartik\growl\GrowlAsset::register($this);
kartik\dialog\DialogAsset::register($this);
kartik\base\AnimateAsset::register($this);

$this->registerLinkTag(['rel' => 'canonical', 'href' => Url::canonical()]);
$this->beginPage();
?><!DOCTYPE html>
<html lang="<?= $this->context->htmlLang ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode(($this->context->app->name) ? $this->title . ' | ' . $this->context->app->name : $this->title) ?></title>
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/media/img/favicon/icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/media/img/favicon/icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/media/img/favicon/icon-72-precomposed.png">
    <link rel="shortcut icon" href="/media/img/favicon/favicon.ico">
    <meta name="theme-color" content="#85C1E9">
    <meta name="msapplication-TileColor" content="#85C1E9">
    <meta name="msapplication-TileImage" content="/media/img/favicon/icon-144-precomposed.png">
    <?php $this->head() ?>
</head>
<body class="<?= $this->context->bodyClass ?> <?= $this->context->id . '-' . $this->context->action->id ?>">
    <?php $this->beginBody() ?>
    <?= Yii::$app->controller->renderPartial('//layouts/blocks/flashes') ?>
    <?= $content ?>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
