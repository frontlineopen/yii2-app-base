<?php
/**
 *
 * @package     Views
 * @subpackage  Layouts / Blocks
 * @author      Sorin Pohontu <sorin@frontline.ro>
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 * @since       2020.07.21
 *
 */

use kartik\growl\Growl;

foreach (Yii::$app->session->getAllFlashes() as $type => $message) {
    switch ($type) {
        case 'error':
            $types = Growl::TYPE_DANGER;
            $icon = 'fas fa-radiation-alt';
            break;

        case 'success':
            $types = Growl::TYPE_SUCCESS;
            $icon = 'fas fa-check-circle';
            break;

        case 'info':
            $types = Growl::TYPE_INFO;
            $icon = 'fas fa-info';
            break;

        default:
            $types = Growl::TYPE_WARNING;
            $icon = 'fas fa-exclamation-triangle';
            break;
    }

    echo Growl::widget([
        'type'          => $types,
        'icon'          => $icon,
        'body'          => $message,
        'pluginOptions' => [
            'showProgressbar' => false,
            'placement'       => [
                'from'  => 'top',
                'align' => 'right',
            ],
        ]
    ]);
}
