<?php
/**
 *
 * @package     Frontline Yii2 App-base
 *
 * @subpackage  <Application Assets>
 *
 * @author      Sorin Pohontu <sorin@frontline.ro>
 * @copyright   2017-2020 Frontline softworks <https://www.frontline.ro>
 * @license     https://opensource.org/licenses/BSD-3-Clause
 *
 * @category    assets
 * @see         http://www.yiiframework.com/doc-2.0/guide-structure-assets.html
 *
 * @since       2020.06.03
 *
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Application Assets class
 */
class AppAsset extends AssetBundle
{
    /** @var string Application base path */
    public $basePath = '@webroot';

    /** @var string Application base URL */
    public $baseUrl = '@web';

    /** @var array Application base CSS */
    public $css = [
        '//fonts.googleapis.com/css?family=Montserrat:300,400,400i,700',
        '//cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css',
        '/media/css/coreui.css',
        '/media/css/app-base.css',
    ];

    /** @var array Application base JS */
    public $js = [
        '/media/js/coreui.bundle.min.js',
        '/media/js/app-base.js',
    ];

    /** @var array Application depends */
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
    ];
}
